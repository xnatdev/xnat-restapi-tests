XNAT REST API Functional Test Suite
===================================

The XNAT REST API functional test suite exercises the REST API of a specified server to verify the proper function of
the available REST calls supported by XNAT.

You can find the latest version of the test suite and its documentation at:

* https://bitbucket.org/xnatdev/xnat-restapi-tests

INSTALLATION
------------

The test suite is maintained in (Git)[https://git-scm.com]. If you have Git installed, you can
clone the repository to your local machine:

    git clone git@bitbucket.org:xnatdev/xnat-restapi-tests.git

If you have an existing installation that may need to be updated, you can pull the latest changes from the repo as well:

    git publ git@bitbucket.org:xnatdev/xnat-restapi-tests.git

USAGE
-----

You can run the XNAT REST API functional test suite in a number of ways:

* Run from Eclipse using **Run As > JUnit Test**

* Run from IntelliJ IDEA by right-clicking the module and clicking **Run 'All Tests'**

* Run from the command line using Maven directly:

        mvn clean test

The default test configuration uses the file **local.properties**, located in the folder **src/test/resources/config**,
for the necessary authentication values. Copy **local.properties.sample** to **local.properties* (still in the
**src/test/resources/config** folder) and customize as needed.

Note that the test suite will _not_ create the user specified by the **xnat.user** property. You must make sure this
user is already created before running the tests, with the user's password corresponding to the value set for the
**xnat.password** property.

To use a different properties file in the same folder:

* Configure a profile in the **pom.xml** file (profiles already exist that you can use as a model). You can
then invoke **maven** with the **-P_profile_** option:

        mvn -Pprofile clean test

* Specify the filename with the the **xnat.config** system property

        mvn -Dxnat.config=profile clean test
