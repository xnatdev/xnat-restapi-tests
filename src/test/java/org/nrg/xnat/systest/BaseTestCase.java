/*
 * BaseTestCase.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */
package org.nrg.xnat.systest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.nrg.framework.net.AuthenticatedClientHttpRequestFactory;
import org.nrg.xapi.model.users.User;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.bean.base.BaseElement.UnknownFieldException;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.nrg.xnat.systest.util.FileLocation;
import org.nrg.xnat.systest.util.IOUtils;
import org.nrg.xnat.systest.util.XNATProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.*;

public class BaseTestCase {
    private static final String URI2 = "url";
    private static final Logger _log = LoggerFactory.getLogger(BaseTestCase.class);

    public static final String MEMBER               = "member";
    public static final String COLLABORATOR         = "collaborator";
    public static final String XNAT_MR_SCAN_DATA    = "xnat:mrScanData";
    public static final String XNAT_MR_SESSION_DATA = "xnat:mrSessionData";
    public final static String ASSESSOR_TYPE        = "xnat:qcAssessmentData";

    public final static String SOME_FILE = "images/1.dcm";

    protected static final String NONE   = "none";
    protected static final String DELETE = "DELETE";
    public static final    String PUT    = "PUT";
    protected static final String POST   = "POST";
    public static final    String GET    = "GET";

    public static long f = 0;

    private static User _user;

    @BeforeClass
    public static void initialize() {
        if (_user == null) {
            verifyOrCreateUser(properties.getAdminUser(), properties.getAdminEmail());
            verifyOrCreateUser(properties.getUser(), properties.getEmail());
        }
    }

    protected static XNATProperties getProperties() {
        return properties;
    }

    public static String getHost() {
        return getProperties().getBaseURL();
    }

    public interface Credentials {
        String toString();
    }

    public static class StandardCredentials implements Credentials {
        final String u;
        final String p;

        StandardCredentials(String username, String password) {
            u = username;
            p = password;
        }

        public String toString() {
            return u + ":" + p;
        }
    }

    protected static Credentials getUserCredentials() {
        return new StandardCredentials(getProperties().getUser(), getProperties().getPassword());
    }

    protected static Credentials getAdminCredentials() {
        return new StandardCredentials(getProperties().getAdminUser(), getProperties().getAdminPassword());
    }

    protected static Credentials getInvalidCredentials() {
        return new StandardCredentials(getProperties().getUser(), "badpassword");
    }

    public static XNATProperties properties = new XNATProperties();

    public void writeToStream(URLConnection urlConn, String source) throws IOException {
        IOUtils.writeToStream(urlConn, BaseTestCase.GetSourceFile(this.getClass(), source));
    }

    public void writeToFile(URLConnection url, String destination) throws IOException {
        IOUtils.writeToFile(this.getClass(), url, destination);
    }

    public File GetResultsFile(String location) {
        return BaseTestCase.GetResultsFile(this.getClass(), location);
    }

    public File GetSourceFile(String location) throws FileNotFoundException {
        return GetSourceFile(this.getClass(), location);
    }

    public static File GetSourceFile(Class clazz, String location) throws FileNotFoundException {
        File outFile = new File(FileLocation.fileLocation.getDataLocation() + File.separator + "src" + File.separator + clazz.getName() + File.separator + location);
        if (!outFile.exists()) {
            outFile = new File(FileLocation.fileLocation.getDataLocation() + File.separator + location);
        }
        if (location != null && !location.equals(NONE) && !outFile.exists()) {
            throw new FileNotFoundException();
        }
        return outFile;
    }

    public String RetrieveContents(String location) throws IOException {
        return IOUtils.RetrieveContents(BaseTestCase.GetResultsFile(this.getClass(), location));
    }

    public static String RetrieveContents(Class clazz, String location) throws IOException {
        return IOUtils.RetrieveContents(BaseTestCase.GetResultsFile(clazz, location));
    }

    public void DeleteFile(String location) throws IOException {
        IOUtils.DeleteFile(BaseTestCase.GetResultsFile(this.getClass(), location));
    }

    public static Comparison CompareObjectsFromFile(File f1, File f2, @SuppressWarnings("rawtypes") Map<Class, List<String>> ignoreMap) throws IOException, SAXException {
        FileInputStream fis = new FileInputStream(f1);
        XDATXMLReader reader = new XDATXMLReader();
        BaseElement base1 = reader.parse(fis);

        FileInputStream fis2 = new FileInputStream(f2);
        XDATXMLReader reader2 = new XDATXMLReader();
        BaseElement base2 = reader2.parse(fis2);

        return CompareObjects(base1, base2, new Comparison(), ignoreMap);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static Comparison CompareObjects(BaseElement base1, BaseElement base2, Comparison msgs, Map<Class, List<String>> ignoreMap) {
        if (!base1.getSchemaElementName().equals(base2.getSchemaElementName())) {
            msgs.critical.add("Different XSI Types: " + base1.getSchemaElementName() + " - " + base2.getSchemaElementName());
        } else {
            List<String> ignoreFields = new ArrayList<String>();
            for (Map.Entry<Class, List<String>> entry : ignoreMap.entrySet()) {
                Class c = entry.getKey();

                if (c.isInstance(base1)) {
                    ignoreFields.addAll(entry.getValue());
                    break;
                }
            }
            if (ignoreMap.containsKey(base1.getSchemaElementName())) {
                ignoreFields = ignoreMap.get(base1.getSchemaElementName());
            }
            for (int i = 0; i < base1.getAllFields().size(); i++) {
                try {
                    String path = (String) base1.getAllFields().get(i);
                    if (ignoreFields == null || !ignoreFields.contains(path)) {
                        String ft = base1.getFieldType(path);
                        if (ft.equals(BaseElement.field_data) || ft.equals(BaseElement.field_LONG_DATA)) {
                            if (path.equals("ID") || path.equals("image_session_ID") || path.equals("subject_ID") || path.equals("imageSession_ID")) {
                                Object v1 = base1.getDataFieldValue(path);
                                Object v2 = base2.getDataFieldValue(path);
                                if (!isEmpty(v1) && !isEmpty(v2)) {
                                    if (!v1.equals(v2)) {
                                        msgs.critical.add(base1.getSchemaElementName() + "/" + path + " Mismatch: " + v1 + " " + v2);
                                    }
                                } else if (!isEmpty(v1) || !isEmpty(v1)) {
                                    msgs.warnings.add(base1.getSchemaElementName() + " Null ID: " + v1 + " " + v2);
                                }
                            } else {
                                Object v1 = base1.getDataFieldValue(path);
                                Object v2 = base2.getDataFieldValue(path);
                                if (v1 != null && v2 != null) {
                                    if (v1 instanceof String) {
                                        v1 = ((String) v1).replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", "");
                                    }
                                    if (v2 instanceof String) {
                                        v2 = ((String) v2).replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t", "");
                                    }
                                    if (!v1.equals(v2)) {
                                        msgs.critical.add(base1.getSchemaElementName() + "/" + path + " Mismatch: " + v1 + " " + v2);
                                    }
                                } else if (v1 != null || v2 != null) {
                                    msgs.critical.add(base1.getSchemaElementName() + "/" + path + " Mismatch: " + v1 + " " + v2);
                                }
                            }
                        } else {
                            //reference
                            Object o1 = base1.getReferenceField(path);
                            Object o2 = base2.getReferenceField(path);

                            if (o1 == null && o2 == null) {

                            } else if (o1 == null || o2 == null) {
                                msgs.critical.add(path + " Mismatch: " + o1 + " " + o2);
                            } else {
                                if (o1 instanceof ArrayList && o2 instanceof ArrayList) {
                                    ArrayList<BaseElement> children1 = (ArrayList<BaseElement>) o1;
                                    ArrayList<BaseElement> children2 = (ArrayList<BaseElement>) o2;
                                    if (children1.size() != children2.size()) {
                                        msgs.critical.add(base1.getSchemaElementName() + "/" + path + " Count mis-match: " + children1.size() + " " + children2.size());
                                    } else {
                                        if (children1.size() > 0) {
                                            String field = null;
                                            try {
                                                children1.get(0).getFieldType("ID");
                                                field = "ID";
                                            } catch (UnknownFieldException e) {
                                                try {
                                                    children1.get(0).getFieldType("project");
                                                    field = "project";
                                                } catch (UnknownFieldException e2) {
                                                    try {
                                                        children1.get(0).getFieldType("timestamp");
                                                        field = "timestamp";
                                                    } catch (UnknownFieldException e3) {

                                                        try {
                                                            children1.get(0).getFieldType("URI");
                                                            field = "URI";
                                                        } catch (UnknownFieldException e4) {

                                                            try {
                                                                children1.get(0).getFieldType("path");
                                                                field = "path";
                                                            } catch (UnknownFieldException e5) {

                                                                try {
                                                                    children1.get(0).getFieldType("name");
                                                                    field = "name";
                                                                } catch (UnknownFieldException e6) {
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (field != null) {
                                                Collections.sort(children1, new BaseElementComparator(field));
                                                Collections.sort(children2, new BaseElementComparator(field));
                                            }

                                            for (int j = 0; j < children1.size(); j++) {
                                                CompareObjects(children1.get(j), children2.get(j), msgs, ignoreMap);
                                            }
                                        }
                                    }
                                } else if (o1 instanceof ArrayList || o2 instanceof ArrayList) {
                                    msgs.critical.add(base1.getSchemaElementName() + "/" + path + " Type mis-match: " + o1 + " " + o2);
                                } else {
                                    BaseElement child1 = (BaseElement) o1;
                                    BaseElement child2 = (BaseElement) o2;

                                    CompareObjects(child1, child2, msgs, ignoreMap);
                                }
                            }
                        }
                    }
                } catch (UnknownFieldException e) {
                    _log.error("A specified field was not found", e);
                }

            }
        }

        return msgs;
    }

    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o.toString().equals("")) {
            return true;
        }
        return false;
    }

    public static class Comparison {
        public ArrayList<String> critical = new ArrayList<String>();
        public ArrayList<String> warnings = new ArrayList<String>();
    }

    private static String buildResultsFile(String uri, String method) {
        String download = method + "_";
        if (uri.indexOf("files") > -1) {
            download += "files";
        } else if (uri.indexOf("assess") > -1) {
            download += "assessor_";
        } else if (uri.indexOf("scans") > -1) {
            download += "scans_";
        } else if (uri.indexOf("recons") > -1) {
            download += "recons_";
        } else if (uri.indexOf("experiments") > -1) {
            download += "expt_";
        } else if (uri.indexOf("subjects") > -1) {
            download += "subjects_";
        } else if (uri.indexOf("projects") > -1) {
            download += "projects_";
        }
        download += String.valueOf(f++);
        return download;
    }

    public ResponseWrapper get(String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "GET", GetSourceFile(upload));
    }

    public ResponseWrapper put(String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "PUT", GetSourceFile(upload));
    }

    public ResponseWrapper post(String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "POST", GetSourceFile(upload));
    }

    public ResponseWrapper delete(String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "DELETE", GetSourceFile(upload));
    }

    public ResponseWrapper get(String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "GET", upload);
    }

    public ResponseWrapper put(String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "PUT", upload);
    }

    public ResponseWrapper post(String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "POST", upload);
    }

    public ResponseWrapper delete(String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "DELETE", upload);
    }

    public ResponseWrapper get(String uri, Credentials user, Map<String, Object> upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "GET", upload);
    }

    public ResponseWrapper put(String uri, Credentials user, Map<String, Object> upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "PUT", upload);
    }

    public ResponseWrapper post(String uri, Credentials user, Map<String, Object> upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "POST", upload);
    }

    public ResponseWrapper delete(String uri, Credentials user, Map<String, Object> upload) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "DELETE", upload);
    }

    public ResponseWrapper get(String uri, Credentials user) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "GET", (File) null);
    }

    public ResponseWrapper put(String uri, Credentials user) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "PUT", (File) null);
    }

    public ResponseWrapper post(String uri, Credentials user) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "POST", (File) null);
    }

    public ResponseWrapper delete(String uri, Credentials user) throws RESTException, IOException {
        return process(this.getClass(), uri, user, "DELETE", (File) null);
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper get(Class clazz, String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(clazz, uri, user, GET, GetSourceFile(clazz, upload));
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper get(Class clazz, String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(clazz, uri, user, GET, upload);
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper post(Class clazz, String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(clazz, uri, user, POST, GetSourceFile(clazz, upload));
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper post(Class clazz, String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(clazz, uri, user, POST, upload);
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper put(Class clazz, String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(clazz, uri, user, PUT, GetSourceFile(clazz, upload));
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper put(Class clazz, String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(clazz, uri, user, PUT, upload);
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper delete(Class clazz, String uri, Credentials user, String upload) throws RESTException, IOException {
        return process(clazz, uri, user, DELETE, GetSourceFile(clazz, upload));
    }

    /**
     * @param clazz
     * @param uri
     * @param user
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    public static ResponseWrapper delete(Class clazz, String uri, Credentials user, File upload) throws RESTException, IOException {
        return process(clazz, uri, user, DELETE, upload);
    }

    protected static ResponseWrapper process(Class clazz, String uri, Credentials user, String method, File upload) throws RESTException, IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        if (upload != null && upload.exists()) {
            if (upload.getName().endsWith(".zip")) {
                map.put("Content-Type", "application/zip");
            } else if (upload.getName().endsWith(".txt")) {
                map.put("Content-Type", "text/plain");
            } else if (upload.getName().endsWith(".xml")) {
                map.put("Content-Type", "text/xml");
            } else if (upload.getName().endsWith(".anon")) {
                map.put("Content-Type", "text/plain");
            }
            map.put("body", upload);
        }
        return process(clazz, uri, user, method, map);
    }

    /**
     * @param uri
     * @param user
     * @param method
     * @param upload
     *
     * @return
     *
     * @throws IOException
     */
    protected static ResponseWrapper process(Class clazz, String uri, Credentials user, String method, Map<String, Object> upload) throws RESTException, IOException {
        long startTime = Calendar.getInstance().getTimeInMillis();
        HttpURLConnection urlConn = IOUtils.newConnection(uri, user.toString());

        int code = -1;
        String msg = null;
        String jsessionid = null;
        File download = BaseTestCase.GetResultsFile(clazz, buildResultsFile(uri, method));
        try {
            urlConn.setRequestMethod(method);
            urlConn.setRequestProperty("Cache-Control", "no-cache");
            urlConn.setRequestProperty("Connection", "Keep-Alive");

            _log.warn(method + " " + uri + "...");

            if (upload.containsKey("Content-Type")) {
                urlConn.setRequestProperty("Content-Type", upload.get("Content-Type").toString());
            }
            if (upload.containsKey("body") && upload.get("body") instanceof File) {
                IOUtils.writeToStream(urlConn, (File) upload.get("body"));
            } else if (upload.containsKey("body")) {
                IOUtils.writeToStream(urlConn, upload.get("body").toString());
            }
            IOUtils.writeToFile(urlConn, download);

            code = urlConn.getResponseCode();
            msg = urlConn.getResponseMessage();
            jsessionid = parseJSESSIONID(urlConn);

            //TODO: PULL OUT THE JSESSION ID
            _log.info("complete.  " + (Calendar.getInstance().getTimeInMillis() - startTime) + "ms");

            urlConn = null;
        } catch (IOException e) {
            _log.error("FAILED.  " + (Calendar.getInstance().getTimeInMillis() - startTime) + "ms");
            if (urlConn != null) {
                try {
                    throw new RESTException(new ResponseWrapper(download, urlConn.getResponseCode(), urlConn.getResponseMessage(), null), e);
                } catch (IOException e1) {
                    throw e;
                }
            }
        }

        return new ResponseWrapper(download, code, msg, jsessionid);
    }

    /**
     * Gets the JSESSION ID from the cookies attached to the given connection.
     *
     * @param connection The HTTP connection from which to extract the JSESSION ID.
     *
     * @return The JSESSION ID as a string if found, null otherwise.
     */
    public static String parseJSESSIONID(final HttpURLConnection connection) {
        if (connection == null) {
            return null;
        }

        List<String> cookies = connection.getHeaderFields().get("Set-Cookie");

        if (cookies == null || cookies.size() == 0) {
            return null;
        }

        for (String cookie : cookies) {
            if (StringUtils.startsWith(cookie, "JSESSIONID")) {
                int start = cookie.indexOf("=");
                int end = cookie.indexOf(";");
                return cookie.substring(start + 1, end);
            }
        }

        return null;
    }

    public static class RESTException extends Throwable {
        final ResponseWrapper response;

        public RESTException(ResponseWrapper response, Exception e) {
            super(e);
            this.response = response;
        }

        public ResponseWrapper getResponse() {
            return this.response;
        }

        public int getStatusCode() {
            return this.response.getStatusCode();
        }
    }

    /**
     * @author tolsen01
     */
    public static class ResponseWrapper {
        private final File   f;
        private final int    code;
        public final  String msg;
        public final  String JSESSIONID;

        public ResponseWrapper(final File f, final int code, final String msg, final String jsessionid) {
            this.f = f;
            this.code = code;
            this.msg = msg;
            this.JSESSIONID = jsessionid;
        }

        public int getStatusCode() {
            return code;
        }

        /**
         * @return
         *
         * @throws IOException
         */
        public String getResponseTxt() throws IOException {
            return IOUtils.RetrieveContents(f);
        }

        public String txt() throws IOException {
            return getResponseTxt();
        }

        public File getResponseFile() {
            return f;
        }

        public File file() {
            return getResponseFile();
        }

        /**
         * @return
         *
         * @throws IOException
         */
        public String getResponseTxt1stLine() throws IOException {
            return IOUtils.RetrieveFirstLine(f);
        }

        public JSONObject getResponseAsJSON() throws JSONException, IOException {
            return parseJSON(this);
        }

        public JSONObject json() throws JSONException, IOException {
            return parseJSON(this);
        }

        public List<JSONObject> jsonList() throws JSONException, IOException {
            return getResponseAsJSONList();
        }

        public List<JSONObject> getResponseAsJSONList() throws JSONException, IOException {
            return parseJSONTable(this);
        }

        public BaseElement getResponseAsBean() throws IOException, SAXException {
            FileInputStream fis = new FileInputStream(this.f);
            XDATXMLReader reader = new XDATXMLReader();
            return reader.parse(fis);
        }

        public boolean compareBeanXML(File original, Map<Class, List<String>> fieldsToIgnore) throws ComparisonException, IOException, SAXException {
            Comparison c = CompareObjectsFromFile(this.file(), original, fieldsToIgnore);
            if (c.critical.size() == 0) {
                for (String s : c.warnings) {
                    _log.warn(s);
                }
            } else {
                for (String s : c.critical) {
                    throw new ComparisonException(s);
                }
            }

            return true;
        }

        public boolean compareBinaryFile(File original) throws FileNotFoundException, IOException {
            return compareBinaryFiles(this.file(), original);
        }

        public boolean compareTextFile(File original) throws FileNotFoundException, IOException {
            return this.getResponseTxt().equals(IOUtils.RetrieveContents(original));
        }
    }

    public static class ComparisonException extends Throwable {
        public ComparisonException(String s) {
            super(s);
        }
    }

    /**
     * @param f
     *
     * @return
     *
     * @throws IOException
     * @throws JSONException
     * @throws IOException
     */
    public static JSONObject parseJSON(final File f) throws JSONException, IOException {
        if (f == null || !f.exists()) {
            throw new FileNotFoundException();
        }

        return new JSONObject(IOUtils.RetrieveContents(f));
    }

    /**
     * @param rw
     *
     * @return
     *
     * @throws IOException
     */
    public static JSONObject parseJSON(final ResponseWrapper rw) throws JSONException, IOException {
        return parseJSON(rw.f);
    }

    /**
     * @param f
     *
     * @return
     *
     * @throws IOException
     */
    public static List<JSONObject> parseJSONTable(final File f) throws JSONException, IOException {
        JSONObject o = parseJSON(f);

        JSONObject set = o.getJSONObject("ResultSet");

        JSONArray results = set.getJSONArray("Result");

        List<JSONObject> response = new ArrayList<JSONObject>();

        for (int i = 0; i < results.length(); i++) {
            response.add(results.getJSONObject(i));
        }

        return response;
    }

    /**
     * @param rw
     *
     * @return
     *
     * @throws IOException
     */
    public static List<JSONObject> parseJSONTable(final ResponseWrapper rw) throws JSONException, IOException {
        return parseJSONTable(rw.f);
    }

    /**
     * @param expected
     * @param objects
     *
     * @return
     *
     * @throws IOException
     */
    public boolean containsJSONObject(final Map<String, Object> expected, final List<JSONObject> objects) throws JSONException, IOException {
        if (getJSONObject(expected, objects) == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param expected
     * @param objects
     *
     * @return
     *
     * @throws IOException
     */
    public JSONObject getJSONObject(final Map<String, Object> expected, final List<JSONObject> objects) throws JSONException, IOException {
        for (JSONObject o : objects) {
            int matched = 0;
            for (Map.Entry<String, Object> entry : expected.entrySet()) {
                if (o.has(entry.getKey())) {
                    if (o.get(entry.getKey()).equals(entry.getValue())) {
                        matched++;
                    }
                }
            }

            if (matched == expected.size()) {
                return o;
            }
        }

        return null;
    }

    public static class BaseElementComparator implements Comparator<BaseElement> {
        String field = null;

        public BaseElementComparator(String f) {
            field = f;
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        public int compare(BaseElement o1, BaseElement o2) {
            Comparable value1 = null;
            try {
                value1 = (Comparable) o1.getDataFieldValue(field);
            } catch (UnknownFieldException e) {
            }

            Comparable value2 = null;
            try {
                value2 = (Comparable) o2.getDataFieldValue(field);
            } catch (UnknownFieldException e) {
            }

            if (value1 == null) {
                if (value2 == null) {
                    return 0;
                } else {
                    return -1;
                }
            }
            if (value2 == null) {
                return 1;
            }

            return value1.compareTo(value2);
        }
    }

    /**
     * @param f1
     * @param f2
     *
     * @return
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static boolean compareBinaryFiles(final File f1, final File f2) throws FileNotFoundException, IOException {
        FileInputStream fis1 = null;
        FileInputStream fis2 = null;
        try {
            fis1 = new FileInputStream(f1);
            fis2 = new FileInputStream(f2);

            final String s1 = DigestUtils.sha512Hex(fis1);
            final String s2 = DigestUtils.sha512Hex(fis2);

            return s1.equals(s2);
        } finally {
            try {
                fis1.close();
            } catch (IOException e) {
                _log.error("An exception occurred reading a file", e);
            }
            try {
                if (fis2 != null) {
                    fis2.close();
                }
            } catch (IOException e) {
                _log.error("An exception occurred reading a file", e);
            }
        }

    }

    public static boolean _compareDicomPixelData(final File f1, final File f2) throws FileNotFoundException, IOException {
        final DicomInputStream dis1 = new DicomInputStream(f1);
        final DicomInputStream dis2 = new DicomInputStream(f2);
        DicomObject d1 = dis1.readDicomObject();
        DicomObject d2 = dis2.readDicomObject();
        String p1 = d1.getString(Tag.PixelData);
        String p2 = d2.getString(Tag.PixelData);
        return p1.equals(p2);
    }

    /**
     * @param p: project
     *
     * @return
     */
    public static String getProjectURL(final String p) {
        return "/REST/projects/" + p;
    }

    /**
     * @param p: project
     * @param s
     *
     * @return
     */
    public static String getSubjectURL(final String p, final String s) {
        return getProjectURL(p) + "/subjects/" + s;
    }

    /**
     * @param p: project
     * @param s
     * @param e
     *
     * @return
     */
    public static String getExptURL(final String p, final String s, final String e) {
        return getSubjectURL(p, s) + "/experiments/" + e;
    }

    /**
     * @param p:   project
     * @param s
     * @param e
     * @param scan
     *
     * @return
     */
    public static String getScanURL(final String p, final String s, final String e, final String scan) {
        return getExptURL(p, s, e) + "/scans/" + scan;
    }

    /**
     * @param p:    project
     * @param s
     * @param e
     * @param recon
     *
     * @return
     */
    public static String getReconURL(final String p, final String s, final String e, final String recon) {
        return getExptURL(p, s, e) + "/reconstructions/" + recon;
    }

    /**
     * @param p:     project
     * @param s
     * @param e
     * @param assess
     *
     * @return
     */
    public static String getAssessURL(final String p, final String s, final String e, final String assess) {
        return getExptURL(p, s, e) + "/assessors/" + assess;
    }

    private static String addParams(final String seperator, final String params) {
        if (StringUtils.isEmpty(params)) {
            return "";
        } else {
            return seperator + params;
        }
    }

    /**
     * @param p:     project
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper createProject(final String p, final String params, final Credentials user) throws IOException, RESTException {
        return put(getProjectURL(p) + addParams("?", params), user);
    }

    /**
     * @param p:     project
     * @param s
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper createSubject(final String p, final String s, final String params, final Credentials user) throws IOException, RESTException {
        return put(getSubjectURL(p, s) + addParams("?", params), user);
    }

    /**
     * @param p:      project
     * @param s
     * @param e
     * @param xsiType
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper createExpt(final String p, final String s, final String e, final String xsiType, final String params, final Credentials user) throws IOException, RESTException {
        return put(getExptURL(p, s, e) + "?xsiType=" + xsiType + addParams("&", params), user);
    }

    /**
     * @param p:      project
     * @param s
     * @param e
     * @param scan
     * @param xsiType
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper createScan(final String p, final String s, final String e, final String scan, final String xsiType, final String params, final Credentials user) throws IOException, RESTException {
        return put(getScanURL(p, s, e, scan) + "?xsiType=" + xsiType + addParams("&", params), user);
    }

    /**
     * @param p:      project
     * @param s
     * @param e
     * @param assess
     * @param xsiType
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper createAssess(final String p, final String s, final String e, final String assess, final String xsiType, final String params, final Credentials user) throws IOException, RESTException {
        return put(getAssessURL(p, s, e, assess) + "?xsiType=" + xsiType + addParams("&", params), user);
    }

    /**
     * @param p:          project
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadProjResource(final String p, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getProjectURL(p) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:          project
     * @param s
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadSubjResource(final String p, final String s, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getSubjectURL(p, s) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:          project
     * @param s
     * @param e
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadExptResource(final String p, final String s, final String e, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getExptURL(p, s, e) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:          project
     * @param s
     * @param e
     * @param scan
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadScanResource(final String p, final String s, final String e, final String scan, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getScanURL(p, s, e, scan) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:          project
     * @param s
     * @param e
     * @param recon
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadReconResource(final String p, final String s, final String e, final String recon, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getReconURL(p, s, e, recon) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:          project
     * @param s
     * @param e
     * @param assess
     * @param resource
     * @param fileName
     * @param newFileName
     * @param params
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper uploadAssessResource(final String p, final String s, final String e, final String assess, final String resource, final String fileName, final String newFileName, final String params, final Credentials user) throws IOException, RESTException {
        return put(getAssessURL(p, s, e, assess) + "/resources/" + resource + "/files/" + newFileName + "?inbody=true" + addParams("&", params), user, fileName);
    }

    /**
     * @param p:             project
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateProjResource(final String p, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getProjectURL(p) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    /**
     * @param p:             project
     * @param s
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateSubjResource(final String p, final String s, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getSubjectURL(p, s) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    /**
     * @param p:             project
     * @param s
     * @param e
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateExptResource(final String p, final String s, final String e, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getExptURL(p, s, e) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    /**
     * @param p:             project
     * @param s
     * @param e
     * @param scan
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateScanResource(final String p, final String s, final String e, final String scan, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getScanURL(p, s, e, scan) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    /**
     * @param p:             project
     * @param s
     * @param e
     * @param recon
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateReconResource(final String p, final String s, final String e, final String recon, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getReconURL(p, s, e, recon) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    /**
     * @param p:             project
     * @param s
     * @param e
     * @param assess
     * @param resource
     * @param localFileName
     * @param remoteFileName
     * @param user
     *
     * @throws IOException
     */
    public void validateAssessResource(final String p, final String s, final String e, final String assess, final String resource, final String localFileName, final String remoteFileName, final Credentials user) throws RESTException, IOException {
        validateUpload(getAssessURL(p, s, e, assess) + "/resources/" + resource + "/files/" + remoteFileName, localFileName, user);
    }

    public void validateUpload(final String url, final String fileName, final Credentials user) throws RESTException, IOException {
        ResponseWrapper response = get(url, user);

        if (!response.compareBinaryFile(GetSourceFile(this.getClass(), fileName))) {
            throw new IOException("Files do not match.");
        }
    }

    /**
     * @param p:        project
     * @param s
     * @param new_proj
     * @param new_label
     * @param user
     *
     * @return
     *
     * @throws IOException
     */
    public ResponseWrapper shareSubject(final String p, final String s, final String new_proj, final String new_label, final Credentials user) throws RESTException, IOException {
        return put(getSubjectURL(p, s) + "/projects/" + new_proj + "?label=" + new_label, user);
    }

    /**
     * @param p:        project
     * @param s
     * @param e
     * @param new_proj
     * @param new_label
     * @param user
     *
     * @return
     *
     * @throws IOException
     */
    public ResponseWrapper shareExpt(final String p, final String s, final String e, final String new_proj, final String new_label, final Credentials user) throws RESTException, IOException {
        return put(getExptURL(p, s, e) + "/projects/" + new_proj + "?label=" + new_label, user);
    }

    /**
     * @param p:        project
     * @param s
     * @param e
     * @param assess
     * @param new_proj
     * @param new_label
     * @param user
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper shareAssess(final String p, final String s, final String e, final String assess, final String new_proj, final String new_label, final Credentials user) throws IOException, RESTException {
        return put(getAssessURL(p, s, e, assess) + "/projects/" + new_proj + "?label=" + new_label, user);
    }

    /**
     * @param p:    project
     * @param role: member, collaborator, owner
     * @param u:    username
     * @param user: user account used to process modification
     *
     * @return
     *
     * @throws IOException
     * @throws RESTException
     */
    public ResponseWrapper addUserToProject(final String p, final String role, final String u, final Credentials user) throws IOException, RESTException {
        return put("/REST/projects/" + p + "/users/" + role + "/" + u, user);
    }

    public static boolean CompareFiles(File f1, File f2) throws IOException {
        return FileUtils.contentEquals(f1, f2);
    }

    public static Comparison CompareObjectsFromFile(File f1, File f2) throws IOException, SAXException {
        @SuppressWarnings("rawtypes")
        Map<Class, List<String>> ignore = new Hashtable<Class, List<String>>();

        return CompareObjectsFromFile(f1, f2, ignore);
    }

    public static File GetResultsFile(Class clazz, String location) {
        File outFile = new File(FileLocation.fileLocation.getResultLocation() + File.separator + "results" + File.separator + clazz.getName() + File.separator + location);
        outFile.getParentFile().mkdirs();
        return outFile;
    }

    /**
     * @param serviceName
     *
     * @return
     */
    public static String getServiceURL(final String serviceName) {
        return "/REST/services/" + serviceName;
    }

    static List<String> all_ids = new ArrayList<String>();

    /**
     * The header variable should vaguely identify the test class that created it.  This way if we see a bunch of _projects left on the test server, we can track down where they came from
     *
     * @param header : Prepended to generated ID (should identify test class vaguely... and shouldn't be long <=4 chars)
     *
     * @return
     */
    public static String createProjectId(String header) {
        String id = header + RandomStringUtils.randomAlphanumeric(15);
        while (all_ids.contains(id)) {
            id = header + RandomStringUtils.randomAlphanumeric(15);
        }

        all_ids.add(id);

        return id;
    }

    /**
     * Helper method to delete the archived experiments and subjects for a project.
     *
     * @param project
     *
     * @throws RESTException
     */
    public static void clearProjectArchive(Class clazz, String project) {
        try {
            //delete experiments
            final List<JSONObject> experiments = parseJSONTable(get(clazz, "/REST/projects/" + project + "/experiments?format=json&columns=ID,subject_ID", getAdminCredentials(), NONE));

            for (JSONObject item : experiments) {
                final String id = item.getString("ID");
                if (StringUtils.isEmpty(id)) {
                    throw new Exception("Failed to retrieve ID from experiment");
                }
                final String subject = item.getString("subject_ID");
                if (StringUtils.isEmpty(subject)) {
                    throw new Exception("Failed to retrieve subject_ID from experiment");
                }
                delete(clazz, "/REST/projects/" + project + "/subjects/" + subject + "/experiments/" + id + "?removeFiles=true", getAdminCredentials(), NONE);
            }

            //delete subjects
            final List<JSONObject> subjects = parseJSONTable(get(clazz, "/REST/projects/" + project + "/subjects?format=json&columns=ID", getAdminCredentials(), NONE));

            for (JSONObject item : subjects) {
                final String id = item.getString("ID");
                if (StringUtils.isEmpty(id)) {
                    throw new Exception("Failed to retrieve ID from subject");
                }
                delete(clazz, "/REST/projects/" + project + "/subjects/" + id, getAdminCredentials(), NONE);
            }
        } catch (Throwable e) {
            _log.error("An error occurred", e);
        }
    }

    /**
     * Helper method to delete the contents of the project prearchive.  It queries for the contents of the prearchive and then deletes each session
     *
     * @param project
     */
    public static void clearProjectPrearchive(Class clazz, String project) {
        try {
            final List<JSONObject> objects = parseJSONTable(get(clazz, "/REST/prearchive/projects/" + project + "?format=json", getAdminCredentials(), NONE));

            for (JSONObject item : objects) {

                final String uri = item.getString(URI2);

                delete(clazz, "/REST" + uri, getAdminCredentials(), "none");
            }
        } catch (Throwable e) {

        }
    }

    public final static List<String> UIDs = Arrays.asList("1.3.12.2.1107.5.2.32.35248.30000007072415501831200000004", "1.2.124.113932.1.170.223.162.178.20050107.134232.11943651", "1.2.124.113932.1.170.223.162.178.20050107.134232.11943649");

    /**
     * Helper method to delete the contents of the project prearchive.  It queries for the contents of the prearchive and then deletes each session
     */
    public void clearUnassigned() {
        try {
            final List<JSONObject> objects = parseJSONTable(get("/REST/prearchive?format=json", getAdminCredentials(), NONE));

            for (JSONObject item : objects) {
                if (UIDs.contains(item.getString("tag")) && StringUtils.equals("Unassigned", item.getString("project"))) {
                    final String uri = item.getString(URI2);

                    delete(getClass(), "/REST" + uri, getAdminCredentials(), "none");
                }
            }
        } catch (Throwable e) {
        }
    }

    private static synchronized void verifyOrCreateUser(final String username, final String email) {
        final XNATProperties properties = new XNATProperties();
        final AuthenticatedClientHttpRequestFactory factory = new AuthenticatedClientHttpRequestFactory(properties.getAdminUser(),
                                                                                                        properties.getAdminPassword());
        final String userUrl = properties.getBaseURL() + "/xapi/users/{userId}";
        final Map<String, String> variables = new HashMap<>();
        variables.put("userId", username);

        final RestTemplate template = new RestTemplate(factory);

        try {
            final ResponseEntity<User> response = template.getForEntity(userUrl, User.class, variables);
            _user = response.getBody();
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                if (!org.apache.commons.lang3.StringUtils.equalsIgnoreCase(email, _user.getEmail())) {
                    _user.setEmail(email);
                    _user.setVerified(true);
                    _user.setEnabled(true);
                    template.put(userUrl, _user, variables);
                } else {
                    if (!_user.isEnabled()) {
                        template.put(userUrl + "/enabled/true", null, variables);
                    }
                    if (!_user.isVerified()) {
                        template.put(userUrl + "/verified/true", null, variables);
                    }
                }
            } else {
                _log.error("I don't know what to make of status code {}:\n{}", response.getStatusCode(), _user == null ? "Nothing returned." : _user.toString());
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                _log.error("Looks like I didn't find it.");
                _user = createUser(template, properties, variables);
            } else {
                _log.error("I don't know what to make of status code {}:\n{}", e.getStatusCode(), e.getMessage());
            }
        }
    }

    private static User createUser(final RestTemplate template, final XNATProperties properties, final Map<String, String> variables) {
        final User user = new User();
        final String username = properties.getUser();
        user.setUsername(username);
        user.setPassword(properties.getPassword());
        user.setEmail(properties.getEmail());
        user.setEnabled(true);
        user.setVerified(true);
        user.setFirstName(org.apache.commons.lang3.StringUtils.capitalize(username));
        user.setLastName(org.apache.commons.lang3.StringUtils.capitalize(username));
        try {
            final ResponseEntity<User> response = template.postForEntity(properties.getBaseURL() + "/xapi/users", user, User.class, variables);
            _log.error("Successfully created user {}:\n{}", username, response.getBody().toString());
            return response.getBody();
        } catch (RestClientException e) {
            _log.error("An error occurred creating the user {}", username, e);
        }
        return null;
    }
}
