/*
 * Base64.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.util;

public class Base64 {
	public static String encode(String data) {
		return (new org.apache.commons.codec.binary.Base64()).encodeToString(
				data.getBytes()).trim();
	}
}
