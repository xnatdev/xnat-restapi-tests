/*
 * IOUtils.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.util;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.FileUtils;
import org.nrg.xnat.systest.BaseTestCase;
import org.nrg.xnat.systest.BaseTestCase.Comparison;

public class IOUtils {

	@SuppressWarnings("deprecation")
	public static String RetrieveContents(File f)throws IOException
	{
	    return FileUtils.readFileToString(f);
	}

	public static void DeleteFile(File f)
	{
		if(!f.exists())return;
	    if (f.isDirectory())
	    {
	        String[] files = f.list();
	        for (int i=0;i<files.length;i++){
	            File child = new File(f.getAbsolutePath() + File.separator + files[i]);
	            DeleteFile(child);
	        }
	        if (!f.delete()){
	            System.out.println("Failed to delete: " + f.getAbsolutePath());
	        }
	    }else{
	        if (!f.delete()){
	            System.out.println("Failed to delete: " + f.getAbsolutePath());
	        }
	    }
	}

	public static void writeToStream(URLConnection urlConn,File inFile) throws IOException{
		
		urlConn.setDoOutput(true);
		
		OutputStream out=urlConn.getOutputStream();
		
		InputStream bis = null;
		BufferedOutputStream bos = null;
	
		try {
			bis = new FileInputStream(inFile);
	
			bos = new BufferedOutputStream(out);
			

			org.apache.commons.io.IOUtils.copy(bis, out);
			
			bos.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally{
			try {
				bis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				bos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				out.close();
			} catch (Exception e) {
			}
		}
	}

	public static void writeToStream(URLConnection urlConn,String source) throws IOException{
		
		urlConn.setDoOutput(true);
		
		OutputStream out=urlConn.getOutputStream();
		
	
		try {
			org.apache.commons.io.IOUtils.write(source, out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally{
			out.close();
		}
	}

	public static void writeToFile(Class clazz,URLConnection url,String destination) throws IOException{
		IOUtils.writeToFile(url,BaseTestCase.GetResultsFile(clazz,destination));
	}

	public static void writeToFile(URLConnection url,File outFile) throws IOException{
		if (outFile.exists()) {
			outFile.delete();
		}
		InputStream bis = null;
		BufferedOutputStream bos = null;
	
		try {
			FileOutputStream out = new FileOutputStream(outFile);
	
			bis = url.getInputStream();
			bos = new BufferedOutputStream(out);
	
			byte[] buff = new byte[256];
			int bytesRead;
			int loaded = 0;
	
			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
				bos.flush();
				loaded = loaded + bytesRead;
			}
	
			bis.close();
			bos.flush();
			bos.close();
			out.close();
		} catch (FileNotFoundException e) {
			throw e;
		} 
	}

	@SuppressWarnings("deprecation")
	public static String RetrieveFirstLine(File f)throws IOException
	{
	    FileInputStream in = new FileInputStream(f);
	    DataInputStream dis = new DataInputStream(in);
	    StringBuffer sb = new StringBuffer();
	    while (dis.available() !=0)
	    {
	                            // Print file line to screen
	        sb.append(dis.readLine());
	        break;
	    }
	
	    dis.close();
	
	    return sb.toString();
	
	}

	public static HttpURLConnection newConnection(String script,String userString) throws MalformedURLException, IOException{
		HttpURLConnection url = (HttpURLConnection) new URL(BaseTestCase.getHost() + script).openConnection();
		if(userString.contains(":"))
			url.addRequestProperty("Authorization", "Basic " + Base64.encode(userString));
		else
			url.addRequestProperty("Cookie", "JSESSIONID="+userString);
		return url;
	}

	public static void DeleteFile(Class clazz,String location) throws IOException{
		DeleteFile(BaseTestCase.GetResultsFile(clazz,location));
	}

}
