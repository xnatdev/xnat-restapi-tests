/*
 * XNATProperties.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class XNATProperties {
	private static final Logger LOGGER = Logger.getLogger(XNATProperties.class);
	private static final String DEFAULT_XNAT_CONFIG = "local.properties";
	private static final String XNAT_CONFIG = "xnat.config";
	private static final String XNAT_CONFIG_FOLDER = "/config/";

	private static Properties p;

	public String getBaseURL() {
		return getProperties().getProperty("xnat.baseurl");
	}
	public String getUser() {
		return getProperties().getProperty("xnat.user");
	}
	public String getEmail() {
		return getProperties().getProperty("xnat.email", "xnatselenium@gmail.com");
	}
	public String getPassword() {
		return getProperties().getProperty("xnat.password");
	}
	public String getAdminUser() {
		return getProperties().getProperty("xnat.admin.user");
	}
	public String getAdminPassword() {
		return getProperties().getProperty("xnat.admin.password");
	}
	public String getAdminEmail() {
		return getProperties().getProperty("xnat.admin.email", "xnatselenium@gmail.com");
	}

	/*
	 * On the first invocation of getProperties, the method will search for a
	 * property file on the classpath at /config/<xnat.config>, where
	 * xnat.config is a system property. The property file is then cached for
	 * subsequent use.
	 */
	private synchronized Properties getProperties() {
		if (p == null) {
			String config = System.getProperty(XNAT_CONFIG);
			// test ${xnat.config} in case no profile is specified and maven does not filter a value to surfire
			if (config == null || "${xnat.config}".equals(config)) {
				config = DEFAULT_XNAT_CONFIG;
				LOGGER.debug(XNAT_CONFIG + " variable not specified, using default, " + DEFAULT_XNAT_CONFIG);
			}

			try {
				final Properties props = new Properties();
				final String configPath = XNAT_CONFIG_FOLDER + config; 
				InputStream configStream = getClass().getResourceAsStream(configPath);
				if (configStream == null){
					throw new RuntimeException("Config file, " + configPath + ", not found");
				}
				props.load(configStream);
				p = props;
				LOGGER.debug("Loaded properties from classpath location, " + configPath);
				return props;
			} catch (IOException e) {
				throw new RuntimeException("Error obtaining xnat_test config file: " + config, e);
			}
		} else {
			return p;
		}
	}
}
