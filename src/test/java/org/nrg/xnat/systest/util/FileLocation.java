/*
 * FileLocation.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang.StringUtils;

public class FileLocation {
	private String currentDirectory;
	public static FileLocation fileLocation = new FileLocation();

	private FileLocation() {
		final File dir = new File(".");
		try {
			currentDirectory = dir.getCanonicalPath();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String getDataLocation() {
		return makePath(new String[] { currentDirectory, "src", "test",
				"resources", "data" });
	}

	public String getResultLocation() {
		return makePath(new String[] { currentDirectory, "target" });
	}

	private String makePath(Object[] parts) {
		return StringUtils.join(parts, File.separator);
	}
}
