/*
 * TestSubjectResources.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

import com.google.common.collect.Lists;

public class TestSubjectResources extends BaseTestCase {
    public static List<String> projects = Lists.newArrayList();

    private static String registerProject() {
        final String projectId = createProjectId("SUBJ");
        projects.add(projectId);
        return projectId;
    }

    @AfterClass
    public static void tearDownAfterClass() throws IOException {
        for (String p : projects) {
            try {
                delete(TestSubjAssessmentResources.class, "/REST/projects/" + p + "?removeFiles=true", getUserCredentials(), NONE);
            } catch (Throwable e) {
                //ignore
            }
        }
    }

    @Before
    public void setUp() throws IOException {
    }

    @After
    public void tearDown() throws IOException {
    }

    @Test
    public void testQueryCRUD() throws IOException, SAXException, ComparisonException, RESTException {
        String PROJECT1 = registerProject();
        createProject(PROJECT1, null, getUserCredentials());

        String params = "";
        params += "&group=control";
        params += "&src=12";
        params += "&pi_firstname=Tim";
        params += "&pi_lastname=Olsen";
        params += "&dob=1990-09-08";
        params += "&gender=male";
        params += "&handedness=left";

        //create via query string parameters
        put("/REST/projects/" + PROJECT1 + "/subjects/1?format=xml&req_format=qs" + params, getUserCredentials());

        //retrieve
        ResponseWrapper response = get("/REST/projects/" + PROJECT1 + "/subjects/1?format=xml", getUserCredentials());

        //should match expected xml
        Map<Class, List<String>> ignore = new Hashtable<>();

        ignore.put(XnatSubjectdataBean.class, new ArrayList<String>());
        ignore.get(XnatSubjectdataBean.class).add("project");

        response.compareBeanXML(GetSourceFile("qs_subject_v1.xml"), ignore);
    }

    @Test
    public void testXMLCRUD() throws IOException, SAXException, ComparisonException, RESTException {
        String PROJECT2 = registerProject();

        createProject(PROJECT2, null, getUserCredentials());

        //retrieve list of subjects (should work even though there aren't any)
        get("/REST/projects/" + PROJECT2 + "/subjects?format=html", getUserCredentials());

        //create subject 1 via POST to list url
        post("/REST/projects/" + PROJECT2 + "/subjects?format=xml", getUserCredentials(), "test_subject_v1.xml");

        //retrieve subject 1 xml
        ResponseWrapper response = get("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

        //compare downloaded xml to uploaded one (only projects should differ)
        Map<Class, List<String>> ignore = new Hashtable<>();

        ignore.put(XnatSubjectdataBean.class, new ArrayList<String>());
        ignore.get(XnatSubjectdataBean.class).add("project");

        response.compareBeanXML(GetSourceFile("test_subject_v1.xml"), ignore);

        //retrieve list of subjects (not sure why)
        get("/REST/projects/" + PROJECT2 + "/subjects?format=html", getUserCredentials());

        //re-save without a few parameters.  allowDataDeletion=false
        put("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials(), "test_subject_v2.xml");

        //retrieve subject 1 xml
        response = get("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

        try {
            //compare downloaded to last uploaded (should differ by one field)
            response.compareBeanXML(GetSourceFile("test_subject_v2.xml"), ignore);
            fail("shouldn't match");
        } catch (ComparisonException e1) {
            if (!e1.getMessage().equals("demographicData/education Mismatch: 12 null")) {
                throw e1;
            }
        }

        //compare to original (should match
        response.compareBeanXML(GetSourceFile("test_subject_v1.xml"), ignore);

        ///////////
        //re-save without a few parameters.  allowDataDeletion=true
        put("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml&allowDataDeletion=true", getUserCredentials(), "test_subject_v2.xml");

        //retrieve subject 1 xml
        response = get("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

        try {
            //compare downloaded to original (should differ by one field)
            response.compareBeanXML(GetSourceFile("test_subject_v1.xml"), ignore);
            fail("shouldn't match");
        } catch (ComparisonException e1) {
            if (!e1.getMessage().equals("demographicData/education Mismatch: null 12")) {
                throw e1;
            }
        }

        //compare to original (should match)
        response.compareBeanXML(GetSourceFile("test_subject_v2.xml"), ignore);

        ////////////
        //third modification to add weight/height
        put("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials(), "test_subject_v3.xml");

        //retrieve subject 1 xml
        response = get("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

        try {
            //compare to original
            response.compareBeanXML(GetSourceFile("test_subject_v1.xml"), ignore);
            fail("shouldn't match");
        } catch (ComparisonException e1) {
            if (!e1.getMessage().equals("demographicData/weight Mismatch: 12.0 null")) {
                throw e1;
            }
        }

        //compare to last saved
        response.compareBeanXML(GetSourceFile("test_subject_v3.xml"), ignore);

        //retrieve list of subjects (not sure why)
        get("/REST/projects/" + PROJECT2 + "/subjects?format=html", getUserCredentials());

        //delete subject
        delete("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

        //retrieve list of subjects (not sure why)
        get("/REST/projects/" + PROJECT2 + "/subjects?format=html", getUserCredentials());

        //check for deletion
        try {
            get("/REST/projects/" + PROJECT2 + "/subjects/1?format=xml", getUserCredentials());

            fail("Item should have been deleted");
        } catch (RESTException e) {
            if (e.getStatusCode() != 404) {
                fail(e.getMessage());
            }
        }
    }

}
