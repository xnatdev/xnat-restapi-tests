/*
 * TestImport.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

/**
 * 
 */
package org.nrg.xnat.systest.tests;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

import com.google.common.collect.Lists;

public class TestImport extends BaseTestCase{
	private static final String PROJECT1= createProjectId("IMP");
	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, RESTException {
		//upload data
		put(TestImport.class,"/REST/projects/"+ PROJECT1,getUserCredentials(),NONE);
        put(TestImport.class,"/REST/projects/"+ PROJECT1 +"/prearchive_code/0",getUserCredentials(),NONE);
		
		// disable the site-wide edit script
		String uri = "/REST/config/edit/image/dicom/status?activate=false";
		put(TestImport.class,uri,getAdminCredentials(),NONE);
	}//
	
	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws IOException, RESTException {
		// enable the site-wide edit script
		String uri = "/REST/config/edit/image/dicom/status?activate=true";
		put(TestImport.class,uri,getAdminCredentials(),NONE);
		
		delete(TestImport.class,"/REST/projects/" + PROJECT1 + "?removeFiles=true",getUserCredentials(),NONE);
	}
		
	//TESTS
	@Test
	public void testPrearchive() throws IOException, RESTException, JSONException {
	// services/import?dest=/prearchive POST	
		final String tag="prearc_import";
		String url=post(getServiceURL("import")+"?triggerPipelines=false&dest=/prearchive",getUserCredentials(),"images/localizer.zip").getResponseTxt1stLine();
		
		//SHOULD GO INTO PROJECT2
		final List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/Unassigned?format=json",getAdminCredentials()));
		Map<String,Object> map=new HashMap<String,Object>(){{
			put(TestPrearchiveMgmtAPI.SUBJECT_PARAM,"PATIENT_NAME");
			put(TestPrearchiveMgmtAPI.SESSION_PARAM,"PATIENT_ID");
			put("status","READY");}};

		final JSONObject match=getJSONObject(map,objects);
		
		if(url.startsWith("/data/prearchive")){
			delete(url,getAdminCredentials());
		}
		
		assertNotNull("Upload Failed.",match);
		
	}
	@Test
	public void testProjectPrearchive() throws IOException, RESTException, JSONException {
		// services/import?dest=/prearchive/projects/{PROJECT) POST
		final String tag="prearc_proj_import";
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/prearchive/projects/"+ PROJECT1,getUserCredentials(),"images/localizer.zip");
		
		//SHOULD GO INTO PROJECT2
		final List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT1 + "?format=json",getUserCredentials()));
		Map<String,Object> map=new HashMap<String,Object>(){{
			put("project",PROJECT1);
			put(TestPrearchiveMgmtAPI.SUBJECT_PARAM,"PATIENT_NAME");
			put(TestPrearchiveMgmtAPI.SESSION_PARAM,"PATIENT_ID");
			put("status","READY");}};

		final JSONObject match=getJSONObject(map,objects);
			
		assertNotNull("Upload Failed.",match);
	}
//	@Test
//	public void testData() throws IOException {
//	// services/import?dest=/data POST
//		final String tag="data_import";
//		process(TestImport.class,getServiceURL("import")+"?triggerPipelines=false&dest=/archive",getUserCredentials(),POST,"images/localizer.zip",tag+".txt");
//	
//	//  this should go into a project based on DICOM mapping, but that isn't functional yet.  Waiting on changes to the restructorer by kevin.
//		final String expected_dest=getExptURL(PROJECT2, "PATIENT_NAME", "PATIENT_ID");
//		//SHOULD GO INTO PROJECT2
//		process(TestImport.class,expected_dest,getUserCredentials(),GET,NONE,tag+".txt");
//			
//		process(TestImport.class,expected_dest+"?removeFiles=true",getUserCredentials(),DELETE,"none",tag+".txt");
//	}
	@Test
	public void testDataProject() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT) POST
		final String tag="data_proj_import";
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1,getUserCredentials(),"images/localizer.zip");
		
		final String expected_dest=getExptURL(PROJECT1, "PATIENT_NAME", "PATIENT_ID");
		//SHOULD GO INTO PROJECT2
		get(expected_dest,getUserCredentials());
			
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
		
	}
	@Test
	public void testDataSubject() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT POST
		final String subject="SUB1";
		
		final String tag="data_proj_subj_import";
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject,getUserCredentials(),"images/localizer.zip");
		
		final String expected_dest=getExptURL(PROJECT1, subject, "PATIENT_ID");
		//SHOULD GO INTO PROJECT2
		get(expected_dest,getUserCredentials());
			
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
		
	}
	
	static int MR_COUNT=0;
	@Test
	public void testDataProjectExperimentNew() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB2";
		final String session="MR"+(MR_COUNT++);
		
		final String tag="data_proj_subj_expt_import";
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
		
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		//SHOULD GO INTO PROJECT2
		get(expected_dest,getUserCredentials());
			
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
		
	}
	@Test
	public void testDataProjectExperimentOldRes() throws IOException, RESTException, JSONException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB2";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_proj_subj_expt_old_import";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//create session with custom date
		put(expected_dest+"?req_format=qs&xsiType=xnat:mrSessionData&date="+date1,getUserCredentials());
		
		//add misc resource
		put(expected_dest+"/resources/TEST/files/files.zip?inbody=true",getUserCredentials(),"images/localizer.zip");
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
		
		//test scan upload
		final List<JSONObject> scans=parseJSONTable(get(expected_dest+"/scans?format=json",getUserCredentials()));
		
		assertEquals("Should have one and only one scan.",1,scans.size());
		
		//test session date, should be overwritten
		final List<JSONObject> expts=parseJSONTable(get(getSubjectURL(PROJECT1, subject) +"/experiments?format=json",getUserCredentials()));
			
		Map<String,Object> map=new HashMap<String,Object>(){{
			put("label",session);}};

		final JSONObject match=getJSONObject(map,expts);
		
		final String date2=match.getString("date");
		assertEquals("Date should not have been overwritten.","1999-12-12",date2);
		
		//test misc resource
		validateUpload(expected_dest+"/resources/TEST/files/files.zip","images/localizer.zip",getUserCredentials());
		
		//delete
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}

	/**
	 * Tests merging MR scans into an existing PET session... this should fail
	 * @throws IOException
	 * @throws RESTException 
	 */
	@Test
	public void testDataProjectExperimentNewExptXSIOverwriteFDeleteF() throws IOException, RESTException {
		// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB3";
		final String session="PET"+(MR_COUNT++);
		final String tag="data_proj_subj_expt_new_xsi_import";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//create session with custom date
		put(expected_dest+"?req_format=qs&xsiType=xnat:petSessionData&date="+date1,getUserCredentials());

		try {
			//upload scan
			post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
		
			fail("This should fail to overwrite existing session.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409)
				fail(e.getMessage());
		}
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}

	/**
	 * Tests merging MR scans into an existing PET session with overwrite=delete... this should fail
	 * @throws IOException
	 * @throws RESTException 
	 */
	@Test
	public void testDataProjectExperimentNewExptXSIOverwriteTDeleteF() throws IOException, RESTException {
		final String subject="SUB3";
		final String session="PET"+(MR_COUNT++);
		final String tag="data_proj_subj_expt_new_xsi_import";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//create PET session
		put(expected_dest+"?req_format=qs&xsiType=xnat:petSessionData&date="+date1,getUserCredentials());

		try {
			//upload MR data
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
		
			fail("This should fail to overwrite existing session.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409)
				fail(e.getMessage());
		}
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}
	

	@Test
	public void testDataDuplicateScanFilesOverwriteFDeleteF() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_duplicate_scan_FF";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");

		try {
			//upload scan
			post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
			
			fail("This should have failed.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409)
				fail(e.getMessage());
		}
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}

	/**
	 * @throws IOException
	 */
	@Test
	public void testDataDuplicateScanFilesOverwriteTDeleteF() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_duplicate_scan_TF";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");

		try {
			//upload scan
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
			
			fail("This should have failed.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409)
				fail(e.getMessage());
		}
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}

	/**
	 * This used to fail.  But now it should succeed.  Reimporting the same files is OK.
	 * @throws IOException
	 */
	@Test
	public void testDataDuplicateScanFilesOverwriteTDeleteFWithFileFlag() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_duplicate_scan_TF";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");

		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&overwrite_files=true&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
			
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}

	@Test
	public void testDataDuplicateScanFilesOverwriteTDeleteT() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_duplicate_scan_TT";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");

		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/localizer.zip");
				
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}

	@Test
	public void testDataScan2partsFilesOverwriteTDeleteF() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_scan_2_parts_TF";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p1.zip");

		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p2.zip");

		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}

	@Test
	public void testDataScan2partsOverlapFilesOverwriteTDeleteT() throws IOException, RESTException, JSONException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_scan_2_parts_overlap_TT";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p1plus1.zip");

		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p2.zip");

		final int count_again=parseJSONTable(get(expected_dest+"/files?format=json&all=true",getUserCredentials())).size();
		
		assertEquals("Scan 1 file counts should match",24,count_again);
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}

	@Test
	public void testDataScan2partsOverlapFilesOverwriteTDeleteF() throws IOException, RESTException {
	// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_scan_2_parts_overlap_TF";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p1plus1.zip");

		try {
			//upload scan
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1p2.zip");
			
			fail("This should have failed.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409)
				fail(e.getMessage());
		}
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}
	
	@Test
	public void testPrearcMergeNewScan() throws IOException, RESTException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="prearc_merge_new_scan";
		
		//upload scan 1
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session,getUserCredentials(),"images/scan1.zip");
		
		//upload scan 2
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session,getUserCredentials(),"images/scan2.zip");
		

		//test misc resource
		validateUpload("/REST/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session +"/scans/1/resources/DICOM/files/000001.IMA","images/scan1/000001.IMA",getUserCredentials());
		
		validateUpload("/REST/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session +"/scans/2/resources/DICOM/files/000001.IMA","images/scan2/000001.IMA",getUserCredentials());

	}
	
	@Test
	public void testPrearcMergeDiffUID() throws IOException, RESTException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="prearc_merge_new_scan";
		
		//upload scan 1
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session,getUserCredentials(),"images/scan1.zip");
		
		//upload scan 2
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session,getUserCredentials(),"images/scan2_diffUID.zip");
		

		//test misc resource
		validateUpload("/REST/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session +"/scans/1/resources/DICOM/files/000001.IMA","images/scan1/000001.IMA",getUserCredentials());
		
	
		validateUpload("/REST/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session +"/scans/2/resources/DICOM/files/000001.IMA","images/scan2_diffUID/000001.IMA",getUserCredentials());
	
		delete("/REST/prearchive/projects/"+PROJECT1 + "/" + timestamp +"/"+session,getUserCredentials());
	}

	@Ignore
	@Test
	public void testArcDiffUID() throws IOException, RESTException, JSONException {
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String url="/archive/projects/"+PROJECT1 + "/subjects/" + subject +"/experiments/"+session;
		//upload scan 1
		post(getServiceURL("import")+"?triggerPipelines=false&dest="+url,getUserCredentials(),"images/scan1.zip");
		
		try {
			//upload scan 2
			post(getServiceURL("import")+"?triggerPipelines=false&dest="+url,getUserCredentials(),"images/scan2_diffUID.zip");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409){
				throw e;
			}
		}
		
		delete("/REST"+url,getUserCredentials());
		clearProjectPrearchive(TestImport.class,PROJECT1);
		
		//set to auto-archive with no overwrites
		put("/REST/archive/projects/"+PROJECT1+"/prearchive_code/4",getUserCredentials());

		ResponseWrapper response1=get("/REST/archive/projects/"+PROJECT1+"/experiments?format=json",getUserCredentials());
		assertEquals(0,response1.getResponseAsJSONList().size());
		
		//upload dataset to auto -archive
		post(getServiceURL("import")+"?triggerPipelines=false&project="+PROJECT1,getUserCredentials(),"images/scan1.zip");
		
		//confirm archive
		List<JSONObject> expts=get("/REST/archive/projects/"+PROJECT1+"/experiments?format=json",getUserCredentials()).getResponseAsJSONList();
		//should have auto-archived
		assertEquals(1,expts.size());
		

		//test reuploading the second scan, prearc_code=4 should allow
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite_files=false&overwrite=append&project="+PROJECT1,getUserCredentials(),"images/scan2.zip");
		
		//confirm merger
		expts=get("/REST/archive/projects/"+PROJECT1+"/experiments?format=json",getUserCredentials()).getResponseAsJSONList();
		//should have auto-archived
		assertEquals(1,expts.size());
		
		try{
			//test reuploading the same thing, prearc_code=4 should prevent
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite_files=false&overwrite=append&project="+PROJECT1,getUserCredentials(),"images/scan1.zip");	
		} catch (RESTException e) {
			if(e.getStatusCode()!=409){
				throw e;
			}
		}
		//remove failed merge
		clearProjectPrearchive(TestImport.class,PROJECT1);
		
		//set to auto-archive with overwrites
		put("/REST/archive/projects/"+PROJECT1+"/prearchive_code/5",getUserCredentials());
		
		//test reuploading the same thing, prearc_code=5 should allow
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite_files=true&overwrite=append&project="+PROJECT1,getUserCredentials(),"images/scan1.zip");	

		try {
			//test reuploading the same thing, but with a different UID.  This should fail.
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite_files=true&overwrite=append&project="+PROJECT1,getUserCredentials(),"images/scan2_diffUID.zip");
		} catch (RESTException e) {
			if(e.getStatusCode()!=409){
				throw e;
			}
		}
	}

	/**
	 * This mimics the behavior of the upload assistant on a non-autoarchive project.
	 * @throws IOException
	 */
	@Test
	public void testPrearcGradualImport() throws IOException, RESTException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="prearc_gradual_import";
		
		//upload scan 1
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),"images/scan1.zip");
		
		//upload scan 2
		ResponseWrapper post2=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),"images/scan2.zip");
		
		String uri1=post1.getResponseTxt1stLine();
		assertEquals(uri1,post2.getResponseTxt1stLine());
		
		ResponseWrapper post3=post(uri1+"?action=commit",getUserCredentials());
		
		assertEquals(uri1,post3.getResponseTxt1stLine());
		
		//test misc resource
		ResponseWrapper scan1=get(uri1 +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		if(!compareBinaryFiles(GetSourceFile("images/scan1/000001.IMA"),scan1.file())){
			fail("Files do not match.");
		}
		
		ResponseWrapper scan2=get(uri1 +"/scans/2/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		if(!compareBinaryFiles(GetSourceFile("images/scan2/000001.IMA"),scan2.file())){
			fail("Files do not match.");
		}
	}

	/**
	 * This mimics a session sent from DICOM Browser into Unassigned, moved to a valid project, and archived.
	 * @throws IOException
	 */
	@Test
	public void testUnassignedManualArchiveGradualImport() throws IOException, RESTException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="prearc_ma_gradual_import";
		
		//upload scan 1
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive",getUserCredentials(),"images/scan1.zip");
		
		//upload scan 2
		ResponseWrapper post2=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive",getUserCredentials(),"images/scan2.zip");
		
		String unassigned=post1.getResponseTxt1stLine();
		assertEquals(unassigned,post2.getResponseTxt1stLine());

		//issue commit action with autoarchive=true
		post(unassigned+"?action=build",getAdminCredentials());
		
		//move from unassigned to a new project
		post(unassigned+"?action=move&newProject="+PROJECT1,getAdminCredentials());
		
		final String new_url=unassigned.replace("Unassigned", PROJECT1);
		
		//issue commit action with autoarchive=true
		ResponseWrapper post4=post(new_url+"?action=commit&AA=true",getUserCredentials());
		final String arc=post4.getResponseTxt1stLine();
		
		assertFalse("!=\r\n" + unassigned + "\r\n" + arc,unassigned.equals(arc));
		
		assertTrue(arc.startsWith("/data/archive"));
		
		//test misc resource
		ResponseWrapper scan1=get(arc +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		
		if(!scan1.compareBinaryFile(GetSourceFile("images/scan1/000001.IMA"))){
			fail("Files do not match.");
		}
		
		ResponseWrapper scan2=get(arc +"/scans/2/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		if(!scan2.compareBinaryFile(GetSourceFile("images/scan2/000001.IMA"))){
			fail("Files do not match.");
		}
	}

	/**
	 * This mimics the behavior of the upload assistant but sends multiple copies of the same data concurrently.
	 * ANON IS TURNED OFF in TestImport
	 * @throws IOException
	 * @throws InterruptedException 
	 * @throws JSONException 
	 */
	@Ignore
	@Test
	public void testConcrurentGradualImportNoAnon() throws IOException, RESTException, InterruptedException, JSONException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="concurrent_import";
		
		final List<ResponseWrapper> responses=Lists.newArrayList();
		
		class ThreadCounter implements Callable<Integer> {
			public int running;
			public int threads;
			public ThreadCounter(int count){
				this.running=count;
				this.threads=count;
			}
			
			@Override
			public synchronized Integer call() throws IOException {
				return running--;
			}
		};
		
		final ThreadCounter counter=new ThreadCounter(5);
		
		class Uploader extends Thread{
			int threadNum;
			public Uploader(int num){
				threadNum=num;
			}
			
			@Override
			public void run() {
				try {
					ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),"images/scan1.zip");
					responses.add(post1);
				} catch (Throwable e) {
					//we expect most of these to fail.  Only the first one should work.
				}finally{
					try {
						counter.call();
					} catch (Exception e) {
					}
				}
			}
		};
		
		List<Thread> threads=Lists.newArrayList();
		for(int i=0;i<counter.threads;i++){
			threads.add(new Uploader(i));
		}
		
		for(Thread t: threads){
			t.start();
		}
		
		
		while(counter.running>0){
			Thread.currentThread();
			Thread.sleep(1000);
		}
		
		assertEquals("Expected one successful response.",1,responses.size());
		
		final ResponseWrapper response=responses.get(0);
		
		assertEquals(200,response.getStatusCode());
		
		final String uri1=response.getResponseTxt1stLine();
		
		//issue commit action with autoarchive=true
		ResponseWrapper post3=post(uri1+"?action=commit&AA=true",getUserCredentials());
		
		final String arc=post3.getResponseTxt1stLine();
		
		assertFalse("!=\r\n" + uri1 + "\r\n" + arc,uri1.equals(arc));
		
		assertTrue(arc.startsWith("/data/archive"));
		
		//test misc resource
		ResponseWrapper scan1=get(arc +"/scans/1/resources/DICOM/files?format=json",getUserCredentials());
		assertEquals("Expected 24 files",scan1.getResponseAsJSONList().size(),24);
	}

	/**
	 * This mimics the behavior of the upload assistant on an auto archive project.
	 * @throws IOException
	 */
	@Test
	public void testManualArchiveGradualImport() throws IOException, RESTException {
		final String timestamp="20090804_102241";
		final String session="TEST_MR"+(MR_COUNT++);
		final String tag="prearc_ma_gradual_import";
		
		//upload scan 1
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),"images/scan1.zip");
		
		//upload scan 2
		ResponseWrapper post2=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),"images/scan2.zip");
		
		String uri1=post1.getResponseTxt1stLine();
		assertEquals(uri1,post2.getResponseTxt1stLine());
		
		//issue commit action with autoarchive=true
		ResponseWrapper post3=post(uri1+"?action=commit&AA=true",getUserCredentials());
		
		final String arc=post3.getResponseTxt1stLine();
		
		assertFalse("!=\r\n" + uri1 + "\r\n" + arc,uri1.equals(arc));
		
		assertTrue(arc.startsWith("/data/archive"));
		
		//test misc resource
		ResponseWrapper scan1=get(arc +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		if(!compareBinaryFiles(GetSourceFile("images/scan1/000001.IMA"),scan1.file())){
			fail("Files do not match.");
		}
		
		ResponseWrapper scan2=get(arc +"/scans/2/resources/DICOM/files/000001.IMA",getUserCredentials());
		
		if(!compareBinaryFiles(GetSourceFile("images/scan2/000001.IMA"),scan2.file())){
			fail("Files do not match.");
		}
	}
//	
//	@Test
//	public void testAutoArchiveGradualImport() throws IOException {
//		// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
//		final String timestamp="20090804_102241";
//		final String session="TEST_MR"+(MR_COUNT++);
//		final String tag="prearc_aa_gradual_import";
//		
//		//upload scan
//		ResponseWrapper post1=process(TestImport.class,getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),POST,"images/scan1.zip",tag+"scan1.txt");
//		
//		//re-upload scan
//		ResponseWrapper post2=process(TestImport.class,getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+PROJECT1,getUserCredentials(),POST,"images/scan2.zip",tag+"scan2.txt");
//		
//		String uri1=post1.getResponseTxt1stLine();
//		assertEquals(uri1,post2.getResponseTxt1stLine());
//		
//		process("/REST/projects/" + PROJECT1 + "/prearchive_code/4",getUserCredentials(),"PUT","none",tag+"_mod_pc.txt");
//		
//		ResponseWrapper post3;
//		try {
//			post3 = process(TestImport.class,uri1+"?action=commit",getUserCredentials(),POST,(File)null,tag+"commit.txt");
//		}finally{
//			process("/REST/projects/" + PROJECT1 + "/prearchive_code/0",getUserCredentials(),"PUT","none",tag+"_mod_pc2.txt");
//		}
//		
//		final String arc=post3.getResponseTxt1stLine();
//		
//		assertFalse("!=\r\n" + uri1 + "\r\n" + arc,uri1.equals(arc));
//		
//		assertTrue(arc.startsWith("/data/archive"));
//		
//		//test misc resource
//		ResponseWrapper scan1=process(TestImport.class,arc +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials(),GET,NONE,tag+"000001.IMA");
//		
//		if(!compareBinaryFiles(GetSourceFile("images/scan1/000001.IMA"),scan1.f)){
//			fail("Files do not match.");
//		}
//		
//		ResponseWrapper scan2=process(TestImport.class,arc +"/scans/2/resources/DICOM/files/000001.IMA",getUserCredentials(),GET,NONE,tag+"000001.IMA");
//		
//		if(!compareBinaryFiles(GetSourceFile("images/scan2/000001.IMA"),scan2.f)){
//			fail("Files do not match.");
//		}
//	}
	
	@Test
	public void testDataProjectExperimentNewScanXSI() throws IOException, RESTException, JSONException {
		// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String subject="SUB3";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_proj_subj_expt_new_scan_import";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//create session with custom date
		put(expected_dest+"?req_format=qs&xsiType=xnat:mrSessionData&date="+date1,getUserCredentials());
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan1.zip");
		
		final int count=parseJSONTable(get(expected_dest+"/scans/1/resources/DICOM/files?format=json",getUserCredentials())).size();
		
		ResponseWrapper response=null;
		try {
			//re-upload scan
			response=post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan2.zip");
		}catch(RESTException e){
			delete(expected_dest+"?removeFiles=true",getUserCredentials());
			throw e;
		}
		
		final List<JSONObject> scans=parseJSONTable(get(expected_dest+"/scans?format=json",getUserCredentials()));
		
		assertEquals("Should have two scans.",2,scans.size());
		
		final int count_again=parseJSONTable(get(expected_dest+"/scans/1/resources/DICOM/files?format=json",getUserCredentials())).size();
		
		assertEquals("Scan 1 file counts should match",count,count_again);
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
		
	}
	
	//test against
	// session with reconstruction
	// session with assessment
	@Test
	public void testDataDirectExperiment() throws IOException, RESTException, JSONException {
	// services/import?dest=/archive/projects/{PROJECT)/experiments/EXPERIMENT POST
		final String subject="SUB2";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_proj_subj_expt_direct_import";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		//create session with custom date
		ResponseWrapper rw=put(expected_dest+"?req_format=qs&xsiType=xnat:mrSessionData&date="+date1,getUserCredentials());
		final String ID=rw.getResponseTxt1stLine();
		
		//add misc resource
		put(expected_dest+"/resources/TEST/files/files.zip?inbody=true",getUserCredentials(),"images/localizer.zip");
		
		//upload scan
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=append&dest=/archive/experiments/"+ID,getUserCredentials(),"images/localizer.zip");
		
		//test scan upload
		final List<JSONObject> scans=parseJSONTable(get(expected_dest+"/scans?format=json",getUserCredentials()));
		
		assertEquals("Should have one and only one scan.",1,scans.size());
		
		//test session date, should be overwritten
		final List<JSONObject> expts=parseJSONTable(get(getSubjectURL(PROJECT1, subject) +"/experiments?format=json",getUserCredentials()));
	
		Map<String,Object> map=new HashMap<String,Object>(){{
			put("label",session);}};

		final JSONObject match=getJSONObject(map,expts);
		
		final String date2=match.getString("date");
		assertEquals("Date should not have been overwritten.","1999-12-12",date2);
		
		//test misc resource
		ResponseWrapper rw2=get(expected_dest+"/resources/TEST/files/files.zip",getUserCredentials());
		

		if(!compareBinaryFiles(GetSourceFile("images/localizer.zip"),rw2.file())){
			fail("Files do not match.");
		}
		
		//delete
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
	}
	
	@Test
	public void testNonPathBasedImport() throws IOException, RESTException, JSONException{
		final String subject="SUB2";
		final String session="MR"+(MR_COUNT++);
		final String tag="non_path_import";
		final String transaction_id="s"+Calendar.getInstance().getTimeInMillis();
		
		final ResponseWrapper rw =post("/REST/JSESSION",getUserCredentials());
		
		Credentials cookie=new Credentials() {
			public String toString(){
				return rw.JSESSIONID;
			}
		};
		
		post(StringUtils.join(new String[]{getServiceURL("import"),"?triggerPipelines=false&project=" + PROJECT1 +"&subject=",subject,"&session=",session,"&overwrite=append&http-session-listener=",transaction_id}),cookie,"images/localizer.zip");
		
		JSONObject o=parseJSON(get("/REST/status/"+transaction_id+"?format=json",cookie));
		
		JSONArray a=o.getJSONArray("msgs").getJSONArray(0);
		JSONObject last=a.getJSONObject(a.length()-1);
		final String status = last.getString("status");
		assertEquals(status, "COMPLETED");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws IOException {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws IOException {
		//some errors occur when sessions are left in the prearchive after each step
		clearProjectPrearchive(TestImport.class,PROJECT1);
		
		clearProjectArchive(TestImport.class, PROJECT1);
	}
	


	@Test
	public void testModifiedSeriesUIDs() throws IOException, RESTException, JSONException {
		clearUnassigned();
		this.clearProjectPrearchive(this.getClass(), PROJECT1);
		
		//5 DICOM files from the original scan will be uploaded.
		//one frame has been modified to change the series UID.
		//that file should be uploaded first creating scan 1
		//the second file should be uploaded and generate a new scan (1-MR1) because it has a different series UID
		//the third file will match the second file, and should be added to 1-MR1.
		//the fourth and fifth files will be uploaded together and should go into scan 1.
		final String subject="SUB5";
		final String session="MR"+(MR_COUNT++);
		final String tag="data_modified_series_uid_scan_TT";
		final String date1="12/12/1999";
		final String expected_dest=getExptURL(PROJECT1, subject, session);
		
		//upload frame 1, should create scan 1
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan_mod/000001.zip");

		//confirm that the data went where we expected
		get(expected_dest, getUserCredentials());
		get(expected_dest+"/scans/1", getUserCredentials());
		assertEquals("Should have 1 frame",1,get(expected_dest+"/scans/1/files?format=json", getUserCredentials()).getResponseAsJSONList().size());
		
		//upload frame 2, should create scan 1-MR1
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan_mod/000002.zip");

		//confirm that the data went where we expected
		get(expected_dest, getUserCredentials());
		get(expected_dest+"/scans/1-MR1", getUserCredentials());
		assertEquals("Should have 1 frame",1,get(expected_dest+"/scans/1-MR1/files?format=json", getUserCredentials()).getResponseAsJSONList().size());
		
		//upload frame 3, should be added to scan 1-MR1
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan_mod/000003.zip");

		//confirm that the data went where we expected
		get(expected_dest, getUserCredentials());
		get(expected_dest+"/scans/1-MR1", getUserCredentials());
		assertEquals("Should have 2 frame",2,get(expected_dest+"/scans/1-MR1/files?format=json", getUserCredentials()).getResponseAsJSONList().size());
		
		//upload frame 4 & 5, should be added to scan 1
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=delete&dest=/archive/projects/"+PROJECT1 + "/subjects/"+subject + "/experiments/" +session,getUserCredentials(),"images/scan_mod/000004.zip");
		
		//confirm that the data went where we expected
		get(expected_dest, getUserCredentials());
		get(expected_dest+"/scans/1", getUserCredentials());
		assertEquals("Should have 3 frame",3,get(expected_dest+"/scans/1/files?format=json", getUserCredentials()).getResponseAsJSONList().size());
		
		//test scan creations
		final List<JSONObject> scans=parseJSONTable(get(expected_dest+"/scans?format=json",getUserCredentials()));
		
		assertEquals("Should have 2 scans.",2,scans.size());
		

		//test scan creations
		final List<JSONObject> catalogs=parseJSONTable(get(expected_dest+"/scans/1-MR1/resources?stats=true&format=json",getUserCredentials()));
		
		assertEquals("Should have 1 catalog (DICOM).",1,catalogs.size());
		assertEquals("Should have 2 files.","2",catalogs.get(0).get("file_count"));
		

		//test scan creations
		final List<JSONObject> catalogs2=parseJSONTable(get(expected_dest+"/scans/1/resources?stats=true&format=json",getUserCredentials()));
		
		assertEquals("Should have 1 catalog (DICOM).",1,catalogs2.size());
		assertEquals("Should have 3 files.","3",catalogs2.get(0).get("file_count"));
		
		delete(expected_dest+"?removeFiles=true",getUserCredentials());
				
	}
}
