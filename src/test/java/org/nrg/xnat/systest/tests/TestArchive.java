/*
 * TestArchive.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestArchive extends BaseTestCase {
	
	private String project= createProjectId("ARC");

	@Before
	public void setUp() throws IOException, RESTException {
		put("/REST/projects/" + project + "?format=xml&req_format=qs",getUserCredentials());
		
		// disable the site-wide edit script
		put("/REST/config/edit/image/dicom/status?activate=false",getAdminCredentials());
	}
	
	
	@Test
	public void testWebQCGeneration() throws IOException, RESTException, InterruptedException {
		//create subject
		put("/REST/projects/" + project + "/subjects/1?format=xml&req_format=qs&gender=male",getUserCredentials());

		//create session
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());

		//create scan 1
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=usable",getUserCredentials());
		
		//create scan 2
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/2?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=questionable",getUserCredentials());

		//create resource1
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1/resources/DICOM?format=DICOM",getUserCredentials());

		//create resource2
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/2/resources/DICOM?format=DICOM",getUserCredentials());

		////// upload files
		
		//scan 1 files
		for(int fC=1;fC<=3;fC++){
			put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/" + fC + ".dcm?inbody=true",getUserCredentials(),"images" + File.separator + "" + fC + ".dcm");
		}
		

		//scan 2 files
		for(int fC=1;fC<=3;fC++){
			put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/2/resources/DICOM/files/" + fC + ".dcm?inbody=true",getUserCredentials(),"images" + File.separator + "" + fC + ".dcm");
		}
		
		//call triggerPipelines to run the snapshot generator
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1?triggerPipelines=true",getUserCredentials());

		//give it time to run
		Thread.sleep(60000);

        get("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1/resources/SNAPSHOTS/files?file_content=ORIGINAL&index=0", getUserCredentials());

	}
	
	@Test
	public void testFixScanTypes() throws IOException, RESTException, SAXException {
		//create subject
		put("/REST/projects/" + project + "/subjects/1?format=xml&req_format=qs&gender=male",getUserCredentials());
		
		//create mr with unmapped types, but series_description=localizer
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=usable",getUserCredentials());
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/2?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=questionable",getUserCredentials());
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1/resources/DICOM?format=DICOM",getUserCredentials());
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/2/resources/DICOM?format=DICOM",getUserCredentials());
		
		//create second mr with scan type mapped to 'MAPPED'
		put("/REST/projects/" + project + "/subjects/1/experiments/MR2?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/13/1999",getUserCredentials());
		put("/REST/projects/" + project + "/subjects/1/experiments/MR2/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=usable&type=MAPPED",getUserCredentials());
				
		//call fixScanTypes
		//all other LOCALIZER scans in this project have been labeled as 'MAPPED', so these should be to.
		put("/REST/projects/" + project + "/subjects/1/experiments/MR1?fixScanTypes=true",getUserCredentials());
		
		//retrieve MR1
		ResponseWrapper response=get("/REST/projects/" + project + "/subjects/1/experiments/MR1?format=xml",getUserCredentials());

		final BaseElement base1=response.getResponseAsBean();
			
        if(base1 instanceof XnatMrsessiondataBean){
        	XnatMrsessiondataBean mr=(XnatMrsessiondataBean)base1;
        	for(XnatImagescandataI scan: mr.getScans_scan()){
        		if(!scan.getType().equals("MAPPED"))
    	        	fail("Scan type improperly matched " + scan.getType());
        	}
        }else{
        	fail("Type mis-match:" + base1.getSchemaElementName());
        }
	}

	@Test
	public void testImportToArchive() throws IOException, RESTException {
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive&project=" + project + "&subject=1&session=MR1&overwrite=append",getUserCredentials(),"images/localizer.zip");

		ResponseWrapper response=get("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/1.dcm",getUserCredentials());
		
		if(!response.compareBinaryFile(GetSourceFile(this.getClass(),"images/localizer/1.dcm"))){
			fail("Files don't match.");
		}
	}

	@Test
	public void testBasicArchiveFromPrearc() throws IOException, RESTException {		
		String destination="/prearchive/projects/" + project + "";
		ResponseWrapper rw= post(getServiceURL("import") + "?triggerPipelines=false&dest=" + destination + "&overwrite=append",getUserCredentials(),"images/localizer.zip");

		String prearc_url=rw.getResponseTxt1stLine();
				
		ResponseWrapper archive=post(getServiceURL("archive")+"?triggerPipelines=false&src="+prearc_url+ "&overwrite=append",getUserCredentials());
		assertEquals(200, archive.getStatusCode());
        		
		ResponseWrapper file=get("/REST/projects/" + project + "/subjects/PATIENT_NAME/experiments/PATIENT_ID/scans/1/resources/DICOM/files/1.dcm",getUserCredentials());
		
		if(!file.compareBinaryFile(GetSourceFile(this.getClass(),"images/localizer/1.dcm"))){
			fail("Files don't match.");
		}
	}

	@Test
	public void testBasicArchiveFromPrearcWParams() throws IOException, RESTException {		
		String destination="/prearchive/projects/" + project + "";
		ResponseWrapper rw= post(getServiceURL("import") + "?triggerPipelines=false&dest=" + destination + "&overwrite=append",getUserCredentials(),"images/localizer.zip");

		String prearc_url=rw.getResponseTxt1stLine();
				
		ResponseWrapper archive=post(getServiceURL("archive")+"?xnat:mrSessionData/project=" + project + "&xnat:mrSessionData/subject_id=1&xnat:mrSessionData/label=MR1&xnat:mrSessionData/scans/scan[0][@xsi:type%3Dxnat:mrScanData]/type=LOCAL_TEST&triggerPipelines=false&src="+prearc_url+ "&overwrite=append",getUserCredentials());
        assertEquals(200, archive.getStatusCode());

		ResponseWrapper file=get("/REST/projects/" + project + "/subjects/1/experiments/MR1/scans/LOCAL_TEST/resources/DICOM/files/1.dcm",getUserCredentials());
		
		if(!file.compareBinaryFile(GetSourceFile(this.getClass(),"images/localizer/1.dcm"))){
			fail("Files don't match.");
		}
	}

	@After
	public void tearDown() throws IOException, RESTException {
		try {
			delete("/REST/projects/" + project + "?removeFiles=true",getUserCredentials());
		} catch (Throwable ignore) {
		}
		
		// enable the site-wide edit script
		put("/REST/config/edit/image/dicom/status?activate=true",getAdminCredentials());
	}
}
