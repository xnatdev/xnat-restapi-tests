/*
 * TestConfigService.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;
import org.nrg.xnat.systest.util.IOUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class TestConfigService  extends BaseTestCase {

	/* just a helper to create a new project in XNAT as the user passed in. */
	private void createProject(String project, Credentials credentials) throws IOException, RESTException{
		put("/REST/projects/"+project+"?format=xml&req_format=qs", credentials);
	}
	
	@Test
	public void testPut() throws IOException, RESTException, JSONException {
		/* This test does the following:
		 * Do a put and a get. make sure what you put is what you get...
		 */
		put("/REST/config/test/newPath/goes/here", getAdminCredentials(), "dummy.txt");

		final List<JSONObject> configs=parseJSONTable(get("/REST/config/test/newPath/goes/here?format=json", getUserCredentials()));
		
		//make sure only one comes back
		assertEquals(1, configs.size());
		
		//make sure the contents are equal
		JSONObject config = configs.get(0);
		String contents = config.getString("contents");
		String dummyFile = IOUtils.RetrieveContents(GetSourceFile(TestConfigService.class, "dummy.txt"));	
		assertEquals(dummyFile, contents);	
	}
	
	@Test
	public void testGetContents() throws IOException, RESTException {
		/* This test does the following:
		 * Do a put and a get. make sure what you put is what you get...
		 */
		put("/REST/config/test/newPath/goes/here", getAdminCredentials(), "dummy.txt");

		ResponseWrapper rw = get("/REST/config/test/newPath/goes/here?contents=true", getUserCredentials());

		//make sure the contents are equal
		assertTrue(rw.compareTextFile(GetSourceFile(TestConfigService.class, "dummy.txt")));
	}
	
	@Test
	public void testReplace() throws IOException, RESTException, JSONException {
		/* This test does the following:
		 * Do 2 puts. Do a get and make sure what you get is what you put the second time...
		 */
		put("/REST/config/test/newPath/goes/here", getAdminCredentials(),"dummy.txt");
		put("/REST/config/test/newPath/goes/here", getAdminCredentials(),"test_asst_v1.xml");

		final ResponseWrapper response=get("/REST/config/test/newPath/goes/here?format=json",getUserCredentials());
		final List<JSONObject> configs=parseJSONTable(response);
		
		//make sure only one comes back
		assertEquals(1, configs.size());
		
		//make sure the contents are equal to the correct one
		JSONObject config = configs.get(0);
		String contents = config.getString("contents");
		String dummyFile = IOUtils.RetrieveContents(GetSourceFile(TestConfigService.class, "test_asst_v1.xml"));	
		assertEquals(dummyFile, contents);	
	}
	
	@Test
	public void testDisableAndEnable() throws IOException, JSONException, RESTException {
		/*  This test does the following:
		 *	PUT REST/config/test/testPath  (random data)
		 *	PUT REST/config/test/testPath&status=disabled
		 *	GET REST/config/test/testPath  (assert no data)
		 *	PUT REST/config/test/testPath&status=enabled
		 *	GET REST/config/test/testPath  (assert random data is returned)
		 *
		 *	and repeat test for URLs that include a project like:
		 *
		 *	REST/projects/someproject/config/test/testPath
		*/
		
		List<JSONObject> configs;
		
		//make up a unique URL
		String toolName = UUID.randomUUID().toString();
		String project = UUID.randomUUID().toString();
		String path = UUID.randomUUID().toString() + "/" + UUID.randomUUID().toString();
		
		createProject(project, getUserCredentials());
		
		List<String> urlsToTest = new ArrayList<>();
		
		urlsToTest.add("/REST/config/" + toolName + "/" + path);
		urlsToTest.add("/REST/projects/" + project + "/config/" + toolName + "/" + path);
		
		for(String url:urlsToTest){
			// If this is a project URL, then the standard user should be able to put. Otherwise, the admin has to put.
			final Credentials putCredentials;
			if (url.contains("/projects/")) {
				putCredentials = getUserCredentials();
			} else {
				putCredentials = getAdminCredentials();
			}
			put(url, putCredentials, "test_asst_v1.xml");  //PUT
			configs=parseJSONTable(get(url+"?format=json", getUserCredentials()));  //GET
			assertEquals(1, configs.size());
			put(url+"?status=disabled", putCredentials);  //DISABLE
			configs=parseJSONTable(get(url+"?format=json",getUserCredentials()));  //GET
			assertEquals(1, configs.size());
			
			JSONObject config = configs.get(0);
			String status = config.getString("status");
			assertEquals("disabled", status);

            ResponseWrapper wrapper;
            try {
                wrapper = put(url + "?status=flarn", putCredentials);
            } catch (RESTException e) {
                wrapper = e.getResponse();
            }

            assertNotNull(wrapper);
            assertEquals(400, wrapper.getStatusCode());

            put(url+"?status=enabled", putCredentials);   //ENABLE
			configs=parseJSONTable(get(url+"?format=json",getUserCredentials()));  //GET
			assertEquals(1, configs.size());
	
			config = configs.get(0);
			status = config.getString("status");
			assertEquals("enabled", status);
			
			//make sure the contents are equal to the originally uploaded config.
			config = configs.get(0);
			String contents = config.getString("contents");
			String dummyFile = IOUtils.RetrieveContents(GetSourceFile(TestConfigService.class, "test_asst_v1.xml"));	
			assertEquals(dummyFile, contents);
		}
	}
	
	@Test
	public void testVersion() throws IOException, JSONException, RESTException {
		/*   This test does the following:
		 *	REST/config/test/testPath  PUT V1
		 *	REST/config/test/testPath  PUT V2
		 *	REST/config/test/testPath  GET to retrieve the version number
		 *	REST/config/test/testPath  PUT V3
		 *	
		 *	REST/config/test/testPath&version=(whatever V2's version number is)
		 *	
		 *	Assert the V2 file matches the contents retrieved at last GET.
		 *
		 *	repeat with project URL's: /projects/{PROJECT_ID}/config/{TOOL_NAME}
		 */
		List<JSONObject> configs;
		
		//make up a unique URL
		String toolName = UUID.randomUUID().toString();
		String project = UUID.randomUUID().toString();
		String path = UUID.randomUUID().toString() + "/" + UUID.randomUUID().toString();
		
		createProject(project, getUserCredentials());
		
		List<String> urlsToTest = new ArrayList<>();
		
		urlsToTest.add("/REST/config/" + toolName + "/" + path);
		urlsToTest.add("/REST/projects/" + project + "/config/" + toolName + "/" + path);

		for(String url:urlsToTest){
			// If this is a project URL, then the standard user should be able to put. Otherwise, the admin has to put.
			final Credentials putCredentials;
			if (url.contains("/projects/")) {
				putCredentials = getUserCredentials();
			} else {
				putCredentials = getAdminCredentials();
			}
			put(url, putCredentials, "test_subject_v1.xml");  //PUT  V1
			put(url, putCredentials, "test_subject_v2.xml");  //PUT  V2
			configs=parseJSONTable(get(url+"?format=json",getUserCredentials()));  //GET for version number
			assertEquals(1, configs.size());
			JSONObject config = configs.get(0);
			String version2 = config.getString("version");
			put(url, putCredentials,"test_subject_v3.xml");  //PUT  V3
			configs=parseJSONTable(get(url+"?format=json&version="+version2,getUserCredentials()));  //GET V2
			assertEquals(1, configs.size());
	
			//make sure the contents are equal to the V2 uploaded config.
			config = configs.get(0);
			String contents = config.getString("contents");
			String dummyFile = IOUtils.RetrieveContents(GetSourceFile(TestConfigService.class, "test_subject_v2.xml"));	
			assertEquals(dummyFile, contents);
		}
	}
	
	@Test
	public void testGetToolListing() throws IOException, JSONException, RESTException {
		/*
		 * This test does the following:
		 * 
		 * GET REST/config  save result (list of tools)
		 * 
		 * PUT REST/config/randomToolName/newPath  some random new configuration with a new tool name
		 * 
		 * GET REST/config  save result (list of tools)
		 * 
		 * assert new result = old result plus new toolName
		 * 
		 * repeat with project urls: /projects/{PROJECT_ID}/config
		 * 
		 */
		List<JSONObject> baselineTools;
		List<JSONObject> updatedTools;
		
		//make up a unique URL
		String project = UUID.randomUUID().toString();
		String toolNameToAdd = UUID.randomUUID().toString();
		String path = UUID.randomUUID().toString() + "/" + UUID.randomUUID().toString();
		
		createProject(project, getUserCredentials());
		
		List<String> urlsToTest = new ArrayList<>();

		urlsToTest.add("/REST/config/" );
		urlsToTest.add("/REST/projects/" + project + "/config/");
		for(String url:urlsToTest){
			// If this is a project URL, then the standard user should be able to put. Otherwise, the admin has to put.
			final Credentials putCredentials;
			if (url.contains("/projects/")) {
				putCredentials = getUserCredentials();
			} else {
				putCredentials = getAdminCredentials();
			}
			put(url+"baselineTool/"+path, putCredentials, "test_subject_v1.xml"); //add a baseline tool
			baselineTools=parseJSONTable(get(url+"?format=json",getUserCredentials()));  //GET baseline tools
			put(url+toolNameToAdd+"/"+path, putCredentials, "test_subject_v1.xml"); //add a random new tool
			updatedTools=parseJSONTable(get(url+"?format=json", putCredentials));  //GET updated tools
			
			assertTrue( baselineTools.size()+1 == updatedTools.size());

			//there has to be a better way to do this!
			boolean found = false;
			for (JSONObject updatedTool : updatedTools) {
				String t = updatedTool.getString("tool");
				if (toolNameToAdd.equals(t)) {
					found = true;
					break;
				}
			}
			assertTrue(found);	
		}
	}
	
	@Test
	public void testProjectLevelSecurity() throws IOException, RESTException{
		String project = UUID.randomUUID().toString();
		String path = UUID.randomUUID().toString() + "/" + UUID.randomUUID().toString();
		String urlToTest = "/REST/projects/" + project + "/config/" + path;
		
		//create a new project as admin.
		createProject(project, getAdminCredentials());
		
		//put a config in that project
		put(urlToTest, getAdminCredentials(), "dummy.txt");

		//pull it back and make sure the contents are equal
		ResponseWrapper rw = get(urlToTest +"?contents=true",getAdminCredentials());
		assertTrue(rw.compareTextFile(GetSourceFile(TestConfigService.class, "dummy.txt")));
		
		//now, try to retrieve the config we added above as a plain user who doesn't have access to the project.
		//process will throw an IOException on an 403 error, so we catch that, here. If we get an error, we win.
		boolean authorized = true;
		try{
			get(urlToTest +"?contents=true",getUserCredentials());
		} catch (Throwable e){
			authorized = false;
		}
		assertTrue(!authorized);
	}
}
