/*
 * TestSearch.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSearch extends BaseTestCase {
    private final Logger logger = LoggerFactory.getLogger(TestSearch.class);
    
    private static class SearchElement {
        final String name, singular, plural;
        final boolean secured;
        
        SearchElement(final String name, final String singular, final String plural, final boolean secured) {
            this.name = name;
            this.singular = singular;
            this.plural = plural;
            this.secured = secured;
        }
        
        SearchElement(final JSONObject o) throws JSONException {
            this(o.getString("ELEMENT_NAME"), o.getString("SINGULAR"), o.getString("PLURAL"), o.getBoolean("SECURED"));
        }
    }

    private static Map<String,SearchElement> elements(final JSONArray results) throws JSONException {
        final Map<String,SearchElement> m = new LinkedHashMap<>();
        for (int i = 0; i < results.length(); i++) {
            final SearchElement e = new SearchElement(results.getJSONObject(i));
            m.put(e.name, e);
        }
        return m;
    }

    @Test
    public void testElements() throws RESTException,IOException,JSONException {
        final ResponseWrapper r = get(TestSearch.class,"/data/search/elements?format=json",getUserCredentials(),NONE);
        final Map<String,SearchElement> elements;
        try {
            final JSONArray emaps = r.getResponseAsJSON().getJSONObject("ResultSet").getJSONArray("Result");
            elements = elements(emaps);
        } catch (JSONException e) {
            logger.error("parsing failure on " + r.getResponseTxt(), e);
            throw new RESTException(r, e);
        }
        assertFalse(elements.isEmpty());
        
        // These can be changed by a site administrator, but these are the default values.
        assertTrue(elements.containsKey("xnat:mrSessionData"));
        assertEquals("MR Session", elements.get("xnat:mrSessionData").singular);
        assertFalse(elements.containsKey("xnat:mrScanData"));
        assertEquals("MR Sessions", elements.get("xnat:mrSessionData").plural);
        
        // Again, a site administrator can mess with this, but xnat:investigatorData is the only default unsecured
        assertFalse(elements.get("xnat:investigatorData").secured);
        for (final Map.Entry<String,SearchElement> me : elements.entrySet()) {
            assertTrue("xnat:investigatorData".equals(me.getValue().name) || me.getValue().secured);
        }
    }
}
