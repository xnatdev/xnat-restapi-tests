/*
 * TestRename.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;


import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestRename  extends BaseTestCase{
	private static final String JUNIT_RENAME = createProjectId("REN");
	public static final String JUNIT_RENAME2 = createProjectId("REN");

	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, RESTException {
		put(TestRename.class,getProjectURL(JUNIT_RENAME)+"?format=xml&req_format=qs&alias=JUNIT_PRE_ALIAS",getAdminCredentials(),NONE);
		put(TestRename.class,getProjectURL(JUNIT_RENAME2),getAdminCredentials(),NONE);
	}

	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws IOException, RESTException {
		delete(TestRename.class,getProjectURL(JUNIT_RENAME)+"?removeFiles=true",getAdminCredentials(),NONE);
		delete(TestRename.class,getProjectURL(JUNIT_RENAME2)+"?removeFiles=true",getAdminCredentials(),NONE);
	}

	@Test
	public void testExptRename() throws IOException, RESTException{
		//process
		final String subject = "1";
		final String session = "MR1";
		final String scan = "1";
		
		final String new_label="MOD1";
		final String resource = "TEST";
		final String fileName = "images/1.dcm";
		
		createSubject(JUNIT_RENAME, subject,null, getAdminCredentials());
		createExpt(JUNIT_RENAME, subject,session,XNAT_MR_SESSION_DATA,null, getAdminCredentials());
		createScan( JUNIT_RENAME, subject,session,scan,XNAT_MR_SCAN_DATA,null, getAdminCredentials());
		
		uploadExptResource(JUNIT_RENAME, subject,session, resource, fileName, fileName,null, getAdminCredentials());
		uploadScanResource(JUNIT_RENAME, subject,session,scan, resource, fileName, fileName,null, getAdminCredentials());
		
		put(getExptURL(JUNIT_RENAME, subject, session)+"?label="+new_label,getAdminCredentials());
		
		validateExptResource(JUNIT_RENAME, subject,new_label, resource, fileName, fileName, getAdminCredentials());
		validateScanResource(JUNIT_RENAME, subject,new_label,scan, resource, fileName, fileName, getAdminCredentials());
	}

	@Test
	//this was a forward looking test.  The rename service doesn't support renaming subjects yet.  Only experiments.
	public void testSubjRename() throws IOException, RESTException{
		//process
		final String subject = "2";
		final String session = "MR2";
		final String scan = "2";
		
		final String new_label="MOD2";
		final String resource = "TEST";
		final String fileName = "images/1.dcm";
		
		createSubject(JUNIT_RENAME, subject,null, getAdminCredentials());
		createExpt(JUNIT_RENAME, subject,session,XNAT_MR_SESSION_DATA,null, getAdminCredentials());
		
		uploadExptResource(JUNIT_RENAME, subject,session, resource, fileName, fileName,null, getAdminCredentials());
		uploadSubjResource(JUNIT_RENAME, subject, resource, fileName, fileName,null, getAdminCredentials());
		
		put(getSubjectURL(JUNIT_RENAME, subject)+"?label="+new_label,getAdminCredentials());
		
		validateSubjResource(JUNIT_RENAME, new_label, resource, fileName, fileName, getAdminCredentials());
		validateExptResource(JUNIT_RENAME, new_label, session, resource, fileName, fileName, getAdminCredentials());
	}

	@Test
	public void testExptWAssessRename() throws IOException, RESTException{
		//process
		final String subject = "3";
		final String session = "MR3";
		final String scan = "3";
		final String assess = "ASSESS3";
		
		final String new_label="MOD3";
		final String resource = "TEST";
		final String fileName = "images/1.dcm";
		
		addUserToProject(JUNIT_RENAME2, MEMBER, getProperties().getUser(), getAdminCredentials());
		
		createSubject(JUNIT_RENAME, subject,null, getAdminCredentials());
		createExpt(JUNIT_RENAME, subject,session,XNAT_MR_SESSION_DATA,null, getAdminCredentials());

		shareSubject(JUNIT_RENAME, subject,JUNIT_RENAME2,subject, getAdminCredentials());
		shareExpt(JUNIT_RENAME, subject,session,JUNIT_RENAME2,session, getAdminCredentials());
		
		createAssess(JUNIT_RENAME2, subject,session,assess,ASSESSOR_TYPE,null, getAdminCredentials());
		
		uploadExptResource(JUNIT_RENAME, subject,session, resource, fileName, fileName,null, getAdminCredentials());
		uploadAssessResource(JUNIT_RENAME2, subject,session,assess, resource, fileName, fileName,null, getAdminCredentials());
		
		//attempt without permission
		try {
			put(getExptURL(JUNIT_RENAME, subject,session)+"?label="+new_label,getUserCredentials());
			org.junit.Assert.fail("User shouldn't be able to rename the session");
		} catch (Throwable e) {
			
		}
		
		//add user as collaborator		
		addUserToProject(JUNIT_RENAME, COLLABORATOR, getProperties().getUser(), getAdminCredentials());
		try {
			put(getExptURL(JUNIT_RENAME, subject,session)+"?label="+new_label,getUserCredentials());
			org.junit.Assert.fail("User shouldn't be able to rename the session");
		} catch (Throwable e) {
			
		}		
		
		//add user as member
		addUserToProject(JUNIT_RENAME, MEMBER, getProperties().getUser(), getAdminCredentials());
		
		put(getExptURL(JUNIT_RENAME, subject,session)+"?label="+new_label,getUserCredentials());
		
		validateExptResource(JUNIT_RENAME, subject,new_label, resource, fileName, fileName, getUserCredentials());
		validateAssessResource(JUNIT_RENAME, subject,new_label,assess, resource, fileName, fileName, getUserCredentials());
		
	}

}
