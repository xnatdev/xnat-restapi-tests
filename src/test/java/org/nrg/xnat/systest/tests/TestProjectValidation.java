/*
 * TestProjectValidation.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import java.io.File;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

/**
 * Created by James on 2/24/14.
 */
public class TestProjectValidation extends BaseTestCase{

	private static String _projectId, _secondaryId, _alias,_title;

	@BeforeClass
	public static void setUp() throws Exception, RESTException{
		_projectId   = createProjectId("p_");
		_secondaryId = createProjectId("s_");
		_alias       = createProjectId("a_");
		_title       = createProjectId("t_");

		// Create project to test against.
		process(TestProjectValidation.class,"/REST/projects/"+_projectId+"?alias="+_alias+"&secondary_ID="+_secondaryId+"&name="+_title,getAdminCredentials(),"PUT",(File)null);
	}

	@AfterClass
	public static void tearDown() throws RESTException{
		try {
			process(TestProjectValidation.class, "/REST/projects/" + _projectId, getAdminCredentials(), "DELETE", (File)null);
		} catch (Exception e) { }
	}

	private boolean executeValidationURL(String url) throws Exception{
		try{
			put(url,getAdminCredentials());
		}catch(RESTException e){
			return (e.getStatusCode() == 403);
		}
		return false;
	}

	/**
	 * Handles validation errors
	 * @param errMsg - Error to throw
	 * @param projectId - project to delete
	 * @throws Exception
	 * @throws RESTException
	 */
	private void handleValidationError(String errMsg, String projectId) throws Exception, RESTException{
		try{delete("/REST/projects/" +projectId,getAdminCredentials());}catch(Exception e){}
		handleValidationError(errMsg);
	}

	/**
	 * Handles validation errors
	 * @param errMsg - Error to throw
	 * @throws Exception
	 */
	private void handleValidationError(String errMsg) throws Exception{
		throw new Exception(errMsg);
	}

	/////////////////////////////////
	//
	// Project Id specific tests
	//
	/////////////////////////////////

	/*
	@Test
	public void testDuplicateId()throws Exception, RESTException{
		// Unable to test this via REST since the URL for adding a new project and editing an existing
		// project are the same.
	}
	*/

	@Test
	public void testProjectIdAsTitle()throws Exception, RESTException{
		String local_id = createProjectId("tpv1");
		if(executeValidationURL("/REST/projects/" + local_id + "?name="+_projectId.toUpperCase())){ return; }
		handleValidationError("testProjectIdAsTitle() FAILED: Should not be able to create a project with a title that is used as another project's ID.", local_id);
	}

	@Test
	public void testProjectIdAsSecondaryId()throws Exception, RESTException{
		String local_id = createProjectId("tpv2");
		if(executeValidationURL("/REST/projects/" + local_id + "?secondary_ID="+_projectId.toUpperCase())){ return; }
		handleValidationError("testProjectIdAsSecondaryId() FAILED: Should not be able to create a project with a secondary ID that is used as another project's ID.", local_id);
	}

	@Test
	public void testProjectIdAsAlias()throws Exception, RESTException{
		String local_id = createProjectId("tpv3");
		if(executeValidationURL("/REST/projects/" + local_id + "?alias="+_projectId.toUpperCase())){ return; }
		handleValidationError("testProjectIdAsAlias() FAILED: Should not be able to create a project with an alias that is used as another project's ID.", local_id);
	}


	/////////////////////////////////
	//
	// Project Title specific tests
	//
	/////////////////////////////////

	@Test
	public void testDuplicateTitle()throws Exception, RESTException{
		String local_id = createProjectId("tpv4");
		if(executeValidationURL("/REST/projects/" + local_id + "?name="+_title.toUpperCase())){ return; }
		handleValidationError("testDuplicateTitle() FAILED: Should not be able to create a project with a Title that is already used by another project.", local_id);
	}

	@Test
	public void testTitleAsSecondaryId() throws Exception, RESTException{
		String local_id = createProjectId("tpv5");
		if(executeValidationURL("/REST/projects/" + local_id + "?secondary_ID="+_title.toUpperCase())){ return; }
		handleValidationError("testTitleAsSecondaryId() FAILED: Should not be able to create a project with secondary ID that is already used as another project's title.", local_id);
	}

	@Test
	public void testTitleAsProjectId() throws Exception, RESTException{
		if(executeValidationURL("/REST/projects/" + _title)){ return; }
		handleValidationError("testTitleAsProjectId() FAILED: Should not be able to create a project with an Id that is already used as another project's title.",_title);
	}

	@Test
	public void testTitleAsAlias() throws Exception, RESTException{
		if(executeValidationURL("/REST/projects/" + _projectId + "?alias="+_title.toUpperCase())){ return; }
		handleValidationError("testTitleAsAlias() FAILED: Should not be able to create a project with an alias that is already used as it's title.");
	}

	/////////////////////////////////
	//
	// Secondary Id specific tests
	//
	/////////////////////////////////

	@Test
	public void testDuplicateSecondaryId()throws Exception, RESTException{
		String local_id = createProjectId("tpv6");
		if(executeValidationURL("/REST/projects/" + local_id + "?secondary_ID="+_secondaryId.toUpperCase())){ return; }
		handleValidationError("testDuplicateSecondaryId() FAILED: Should not be able to create a project with secondary ID that is already used by another project.", local_id);
	}

	@Test
	public void testSecondaryIdAsProjectId() throws Exception, RESTException{
		if(executeValidationURL("/REST/projects/" + _secondaryId)){ return; }
		handleValidationError("testSecondaryIdAsProjectId() FAILED: Should not be able to create a project with an ID that is already used as another project's secondary Id.", _secondaryId);
	}

	@Test
	public void testSecondaryIdAsAlias()throws Exception, RESTException{
		if(executeValidationURL("/REST/projects/" + _projectId + "?alias="+_secondaryId.toUpperCase())){ return; }
		handleValidationError("testSecondaryIdAsAlias() FAILED: Should not be able to create a project with an alias that is already used as it's secondary Id.");
	}

	/////////////////////////////////
	//
	// Alias specific tests
	//
	/////////////////////////////////

	@Test
	public void testDuplicateAlias()throws Exception, RESTException{
		String local_id = createProjectId("tpv7");
		if(executeValidationURL("/REST/projects/" + local_id + "?alias=" +_alias.toUpperCase())){ return; }
		handleValidationError("testDuplicateAlias() FAILED: Should not be able to create a project with an alias that already exists.", local_id);
	}

	@Test
	public void testAliasAsProjectId() throws Exception, RESTException{
		if(executeValidationURL("/REST/projects/" + _alias)){ return; }
		handleValidationError("testAliasAsProjectId() FAILED: Should not be able to create a project with an ID that already exists as another project's alias.",_alias);
	}

	@Test
	public void testAliasAsTitle() throws Exception, RESTException{
		String local_id = createProjectId("tpv8");
		if(executeValidationURL("/REST/projects/" + local_id + "?name=" +_alias.toUpperCase())){ return; }
		handleValidationError("testAliasAsTitle() FAILED: Should not be able to create a project with a title that already exists as another project's alias.",local_id);
	}

	@Test
	public void testAliasAsSecondaryId() throws Exception, RESTException{
		String local_id = createProjectId("tpv9");
		if(executeValidationURL("/REST/projects/" + local_id + "?secondary_ID=" +_alias.toUpperCase())){ return; }
		handleValidationError("testAliasAsSecondaryId() FAILED: Should not be able to create a project with a secondary ID that already exists as another project's alias.",local_id);
	}
}
