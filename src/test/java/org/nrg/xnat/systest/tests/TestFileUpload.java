/*
 * TestFileUpload.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

public class TestFileUpload extends BaseTestCase{

	public static final String TEST_ZIP = "localizer.zip";

	private static String CURRENT_PROJECT= createProjectId("TFU");
	
	@Before
	public void setUp() throws IOException, RESTException {

		CURRENT_PROJECT= createProjectId("TFU");
		
		this.createProject(CURRENT_PROJECT, null, getUserCredentials());
		
		// disable the site-wide edit script
		put("/REST/config/edit/image/dicom/status?activate=false",getAdminCredentials());
		
		createSubject(CURRENT_PROJECT, "1", "req_format=qs&gender=male", getUserCredentials());

		createExpt(CURRENT_PROJECT, "1", "MR1", "xnat:MRSession", "req_format=qs&date=12/12/1999", getUserCredentials());

	}

	@After
	public void tearDown() throws IOException, RESTException {
		delete("/REST/projects/" + CURRENT_PROJECT + "?removeFiles=true",getUserCredentials());
		
		// enable the site-wide edit script
		put("/REST/config/edit/image/dicom/status?activate=true",getAdminCredentials());
	}
	
	@Test
	public void testImageUploadWResourcePrecreate() throws IOException, RESTException{

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable", getUserCredentials());
		
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/2?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=questionable", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM", getUserCredentials());
	
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/1.dcm?inbody=true", getUserCredentials(),"images/1.dcm");

		ResponseWrapper response=get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/1.dcm", getUserCredentials());
			
		File original=GetSourceFile("images" + File.separator + "1.dcm");
		assertTrue(response.compareBinaryFile(original));
	}
	
	@Test
	public void testTxtUploadWResourcePrecreate() throws IOException, RESTException{
		//put files

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/2?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=questionable", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/project_DELETE_2.txt?inbody=true", getUserCredentials(),"images/project_DELETE_2.txt");


		ResponseWrapper response=get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/DICOM/files/project_DELETE_2.txt", getUserCredentials());
		assertTrue(response.compareBinaryFile(GetSourceFile("images" + File.separator + "project_DELETE_2.txt")));
	}

	//@Test - not supported
	public void testZipUpload() throws IOException, RESTException, SAXException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/ZIP/files/"+ TEST_ZIP +"?inbody=true", getUserCredentials(),"images/" + TEST_ZIP);

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/ZIP/files/"+ TEST_ZIP +"?inbody=true", getUserCredentials(),"images/" + TEST_ZIP);

		put("/REST/projects/" + CURRENT_PROJECT + "/resources/ZIP/files/"+ TEST_ZIP +"?inbody=true", getUserCredentials(),"images/" + TEST_ZIP);

		get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/ZIP/files/"+ TEST_ZIP +"",getUserCredentials());
		
		get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/ZIP/files/"+ TEST_ZIP +"",getUserCredentials());
		
		get("/REST/projects/" + CURRENT_PROJECT + "/resources/ZIP/files/"+ TEST_ZIP +"",getUserCredentials());

		//validate uploaded file
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/ZIP/files/"+ TEST_ZIP +"!/1.dcm", "images/localizer/1.dcm", getUserCredentials());
		
		//create scan
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/SCAN1?req_format=qs&xsiType=xnat:MRScan",getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/SCAN1/resources/EXTRACTION/files/"+ TEST_ZIP +"?inbody=true&extract=true",getUserCredentials());

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/SCAN1/resources/EXTRACTION/files/2.dcm","images/2.dcm",getUserCredentials());
		
		XnatMrsessiondataBean mr= (XnatMrsessiondataBean)get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1?format=xml",getUserCredentials()).getResponseAsBean();
		SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/YYYY");
		assertEquals(formatter.format(mr.getDate()),"12/12/1999");
	}

	@Test
	public void testResourcesUpload() throws IOException, RESTException, SAXException, JSONException{
		
		XnatMrsessiondataBean mr= (XnatMrsessiondataBean)get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1?format=xml",getUserCredentials()).getResponseAsBean();
		SimpleDateFormat formatter=new SimpleDateFormat("MM/dd/yyyy");
		assertNotNull(mr.getDate());
		assertEquals(formatter.format(mr.getDate()),"12/12/1999");
		
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST/files/3.dcm?inbody=true", getUserCredentials(),"images/3.dcm");

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/TEST/files/3.dcm?inbody=true", getUserCredentials(),"images/3.dcm");

		put("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST/files/3.dcm?inbody=true",getUserCredentials(), "images/3.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST/files/3.dcm", "images/3.dcm", getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/TEST/files/3.dcm", "images/3.dcm", getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST/files/3.dcm", "images/3.dcm", getUserCredentials());

		//validate zip code
		File zip=get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST/files?format=zip&compression=0",getUserCredentials()).file();

		File temp=GetResultsFile("/temp.dcm");
		if(temp.exists()){
			temp.delete();
		}
		
		//pull this file out of the zip so we can compare it to the original
		ZipFile zipF=new ZipFile(zip);
		ZipEntry entry=zipF.getEntry("MR1/resources/TEST/files/3.dcm");
		assertNotNull(entry);

		InputStream s=zipF.getInputStream(entry);
		OutputStream out=new FileOutputStream(temp);
		
		IOUtils.copy(s, out);
		
		IOUtils.closeQuietly(s);
		IOUtils.closeQuietly(out);
		
		assertTrue(compareBinaryFiles(GetSourceFile("images/3.dcm"), temp));
		
		mr= (XnatMrsessiondataBean)get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1?format=xml",getUserCredentials()).getResponseAsBean();
		assertNotNull(mr.getDate());
		assertEquals(formatter.format(mr.getDate()),"12/12/1999");
		
		//test sub directory access

		put("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/1.dcm?inbody=true",getUserCredentials(), "images/3.dcm");
		put("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/2.dcm?inbody=true",getUserCredentials(), "images/3.dcm");
		put("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/folder/3.dcm?inbody=true",getUserCredentials(), "images/3.dcm");
		
		assertEquals(2,get("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/?format=json",getUserCredentials()).getResponseAsJSONList().size());
		assertEquals(1,get("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/?format=json&recursive=false",getUserCredentials()).getResponseAsJSONList().size());
	
		File zipFile=get("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/?format=zip",getUserCredentials()).file();
		zipF=new ZipFile(zipFile);
		assertEquals(2,zipF.size());
		
		delete("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/folder/",getUserCredentials());
		assertEquals(1,get("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST2/files/sub/?format=json",getUserCredentials()).getResponseAsJSONList().size());
	}
	
	@Test
	public void testReconstructionUpload() throws IOException, RESTException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/1_MR1_2?format=xml&req_format=qs&type=LOCALIZER", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/1_MR1_2/resources/TEST/files/3.dcm?inbody=true", getUserCredentials(),"images/3.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/1_MR1_2/resources/TEST/files/3.dcm","images/3.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/1_MR1_2/files/3.dcm","images/3.dcm",getUserCredentials());
	}
	
	@Test
	public void testTextIMGUploadWOPrecreate() throws IOException, RESTException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/2?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=localizer&quality=questionable", getUserCredentials());

		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/TEST/files/3.dcm?inbody=true", getUserCredentials(),"images/3.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/resources/TEST/files/3.dcm?inbody=true", "images/3.dcm", getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/1/files/3.dcm?inbody=true", "images/3.dcm", getUserCredentials());
	}
	

	@Test
	public void testManQCUpload() throws IOException, RESTException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/assessors/MR1_ManualQC?req_format=qs&xsiType=xnat:qcManualAssessorData&xnat:qcManualAssessorData/pass=true&date=12/12/2001",getUserCredentials());
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/assessors/MR1_ManualQC/out/resources/DICOM/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/assessors/MR1_ManualQC/out/resources/DICOM/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/assessors/MR1_ManualQC/out/files/1.dcm","images/1.dcm",getUserCredentials());

	}

	@Test
	public void testScanUpload() throws IOException, RESTException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/MR1_scan1?req_format=qs&xsiType=xnat:mrScanData",getUserCredentials());
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/MR1_scan1/resources/DICOM/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/MR1_scan1/resources/DICOM/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/scans/MR1_scan1/files/1.dcm","images/1.dcm",getUserCredentials());
	}

	@Test
	public void testReconUpload() throws IOException, RESTException{
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/MR1_recon1",getUserCredentials());
		
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/MR1_recon1/resources/DICOM/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/MR1_recon1/resources/DICOM/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/reconstructions/MR1_recon1/files/1.dcm","images/1.dcm",getUserCredentials());
		
	}

	@Test
	public void testMRResourceUpload() throws IOException, RESTException{		
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/DICOM/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/DICOM/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/files/1.dcm","images/1.dcm",getUserCredentials());
	}

	@Test
	public void testSubjectResourceUpload() throws IOException, RESTException{		
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/TEST1/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");

		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/resources/TEST1/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/files/1.dcm","images/1.dcm",getUserCredentials());
	}

	@Test
	public void testProjectResourceUpload() throws IOException, RESTException{		
		put("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST1/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");
		
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/resources/TEST1/files/1.dcm","images/1.dcm",getUserCredentials());
		validateUpload("/REST/projects/" + CURRENT_PROJECT + "/files/1.dcm","images/1.dcm",getUserCredentials());

	}
	

	/**
	 * Checks the the totalRecords for an MR Session.
	 * Test will first test against an empty MR session.
	 * Then, the test will upload 3 new resources and test again.
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testMRResourcesTotalRecordsCount() throws IOException, RESTException, JSONException{
		
		// Test Records Count with ?file_stats=true
		JSONObject obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json&file_stats=true", getAdminCredentials()));
		String records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("0"));
		
		// Test Records Count 
		obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json", getAdminCredentials()));
		records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("0"));
		
		// Test Records Count with ?file_stats=false
		obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json&file_stats=false", getAdminCredentials()));
		records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("0"));
		
		// Upload three new resources to the MR1 experiment
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST1/files/1.dcm?inbody=true",getUserCredentials(),"images/1.dcm");
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST2/files/2.dcm?inbody=true",getUserCredentials(),"images/2.dcm");
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources/TEST3/files/3.dcm?inbody=true",getUserCredentials(),"images/3.dcm");
		
		// Test Records Count with ?file_stats=true
		obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json&file_stats=true", getAdminCredentials()));
		records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("3"));
		
		// Test Records Count 
		obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json", getAdminCredentials()));
		records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("3"));
		
		// Test Records Count with ?file_stats=false
		obj = parseJSON(get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/MR1/resources?format=json&file_stats=false", getAdminCredentials()));
		records = obj.getJSONObject("ResultSet").getString("totalRecords");
		assertTrue("Total Records count not correct.", records.equals("3"));
	}
}
