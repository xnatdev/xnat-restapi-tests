/*
 * TestSharing.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestSharing extends BaseTestCase {
	

	private static  String USER_PROJECT = createProjectId("SHAR");
	private static  String ADMIN_PROJECT = createProjectId("SHAR");

	@Before
	public void setUp() throws IOException, RESTException {
		//create new projects for each test
		USER_PROJECT = createProjectId("SHAR");
		ADMIN_PROJECT = createProjectId("SHAR");
		
		put("/REST/projects/" + ADMIN_PROJECT + "?format=xml",getAdminCredentials());
		put("/REST/projects/"+USER_PROJECT + "?format=xml",getUserCredentials());
	}

	@After
	public void tearDown() throws IOException, RESTException {

		try {
			delete("/REST/projects/" + ADMIN_PROJECT + "?format=xml",getAdminCredentials());
			
		} catch (Throwable e) {
			
		}

		try {
			delete("/REST/projects/"+USER_PROJECT + "?format=xml",getAdminCredentials());
			
		} catch (Throwable e) {
		}
	}
	
	@Test
	public void testUnShared() throws IOException, RESTException {
		String params="";
		params+="&group=control";
		params+="&src=12";
		params+="&pi_firstname=Tim";
		params+="&pi_lastname=Olsen";
		params+="&dob=1990-09-08";
		params+="&gender=male";
		params+="&handedness=left";
		
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1?format=xml&req_format=qs"+params,getAdminCredentials());

		
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getAdminCredentials());

		

		//check gets
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1?format=xml",getUserCredentials());
			
			fail("Foreigner retrieved expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}

		
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			
			fail("Foreigner retrieved subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		

		//check deletes
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
			
			fail("Foreigner deleted expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}

		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			
			fail("Foreigner deleted subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		

		//share subject
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/projects/"+USER_PROJECT + "?label=TOLSEN_1",getAdminCredentials());


		//check get of original by admin
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getAdminCredentials());

		
		get("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_1",getUserCredentials());

		
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
			
			fail("Foreigner retrieved expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}


		//share expt
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1/projects/"+USER_PROJECT + "?label=TOLSEN_E1",getAdminCredentials());


		//check get
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getAdminCredentials());

		//check get
		get("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_1/experiments/TOLSEN_E1",getUserCredentials());

		//check delete failure
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			fail("Foreigner deleted subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
			
			fail("Foreigner deleted expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}

		
		delete("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_1",getUserCredentials());

		//confirm admin get
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getAdminCredentials());

		
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getAdminCredentials());

		


		//check valid user access
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			
			fail("Foreigner retrieved subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
			
			fail("Foreigner deleted expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		
		//add valid user to project
		put("/REST/projects/" + ADMIN_PROJECT + "/users/member/"+getProperties().getUser(),getAdminCredentials());



		//confirm valid user get
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
		

		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());

		


		//share subt
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/projects/"+USER_PROJECT + "?label=TOLSEN_1",getUserCredentials());

		//share expt
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1/projects/"+USER_PROJECT + "?label=TOLSEN_E1",getUserCredentials());

		


		//confirm valid user get
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
		
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());


		//test that a image assessor can be created for a shared project
		get("/REST/projects/" + USER_PROJECT + "/subjects/TOLSEN_1",getUserCredentials());
		get("/REST/projects/" + USER_PROJECT + "/subjects/TOLSEN_1/experiments/TOLSEN_E1",getUserCredentials());
		put("/REST/projects/" + USER_PROJECT + "/subjects/TOLSEN_1/experiments/TOLSEN_E1/assessors/QC1?format=xml&req_format=qs&xsiType=xnat:QCManualAssessment&xnat:qcManualAssessorData/pass=0",getUserCredentials());
		delete("/REST/projects/" + USER_PROJECT + "/subjects/TOLSEN_1/experiments/TOLSEN_E1/assessors/QC1",getUserCredentials());


		delete("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_1",getUserCredentials());

		

		//confirm valid user get
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
		


		//share expt
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1/projects/"+USER_PROJECT + "",getUserCredentials());

		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			fail("Foreigner deleted subj.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		//delete
		delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getAdminCredentials());
		
		

		//add subject
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/2",getAdminCredentials());


		//share into other project
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/2/projects/"+USER_PROJECT + "?label=TOLSEN_2",getAdminCredentials());


		//confirm valid user get
		get("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_2",getUserCredentials());
		
		//insert MR
		put("/REST/projects/"+USER_PROJECT + "/subjects/TOLSEN_2/experiments/MR2?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getAdminCredentials());


		//check admin get
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/2/experiments/MR2",getAdminCredentials());
			
			fail("admin should not be able to access this expt.");
			
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		

		
		
		//test DELETE by member
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/5",getUserCredentials());
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5/scans/SCAN5?format=xml&req_format=qs&xsiType=xnat:MRScan",getUserCredentials());
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5/reconstructions/RECON5?format=xml&req_format=qs&xsiType=xnat:ReconstructedImage",getUserCredentials());
		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/5",getUserCredentials());
			
			fail("Member deleted subj.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5",getUserCredentials());
			
			fail("Member deleted expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5/scans/SCAN5",getUserCredentials());
			
			fail("Member deleted scan.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		get("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5/reconstructions/RECON5",getUserCredentials());

		
		try {	
			//should not be able to delete this.
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/5/experiments/MR5_5/reconstructions/RECON5",getUserCredentials());
			
			fail("Member deleted recon.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		//remove user from shared project
		delete("/REST/projects/" + ADMIN_PROJECT + "/users/member/"+getProperties().getUser(),getAdminCredentials());


		/**********************************************
		/* recheck that this user cannot access the project resources
		**********************************************/
		
		//check gets
		try {			
			final ResponseWrapper rw=get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1?format=xml",getUserCredentials());
			
			fail("Foreigner retrieved expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}

		
		try {			
			get("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			
			fail("Foreigner retrieved subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		

		//check deletes
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials());
			
			fail("Foreigner deleted expt.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}

		
		try {			
			delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/1",getUserCredentials());
			
			fail("Foreigner deleted subject.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
	}
	
	@Test
	public void testImageAssessorCreation() throws IOException, RESTException{
		//in this test the USER_PROJECT is the aggregator project and the ADMIN_PROJECT is a site-project
		
		//cross share users as collaborators
		put("/REST/projects/" + ADMIN_PROJECT + "/users/collaborator/"+getProperties().getUser(),getAdminCredentials());
		put("/REST/projects/" + USER_PROJECT + "/users/collaborator/"+getProperties().getAdminUser(),getUserCredentials());

		//create subject in user project and share to admin project
		put("/REST/projects/" + USER_PROJECT + "/subjects/6",getUserCredentials());
		put("/REST/projects/" + USER_PROJECT + "/subjects/6/projects/"+ADMIN_PROJECT + "?label=SHARE_S6",getAdminCredentials());
		
		//create session in admin project and share to user project
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/6/experiments/MR6_6?req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getAdminCredentials());
		put("/REST/projects/" + ADMIN_PROJECT + "/subjects/6/experiments/MR6_6/projects/"+USER_PROJECT + "?label=SHARE_E6",getUserCredentials());
		
		//create qc in the user project
		put("/REST/projects/" + USER_PROJECT + "/subjects/6/experiments/SHARE_E6/assessors/QC1?req_format=qs&xsiType=xnat:QCManualAssessment&xnat:qcManualAssessorData/pass=0",getUserCredentials());
		put("/REST/projects/" + USER_PROJECT + "/subjects/6/experiments/SHARE_E6/assessors/QC1/projects/" + ADMIN_PROJECT + "?label=ADMIN_QC1",getAdminCredentials());
		
		//house cleaning
		delete("/REST/projects/" + USER_PROJECT + "/subjects/6/experiments/SHARE_E6/assessors/QC1/projects/"+ADMIN_PROJECT,getAdminCredentials());
		delete("/REST/projects/" + USER_PROJECT + "/subjects/6/experiments/SHARE_E6/assessors/QC1",getUserCredentials());
		delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/6/experiments/MR6_6/projects/"+USER_PROJECT,getUserCredentials());
		delete("/REST/projects/" + ADMIN_PROJECT + "/subjects/6/experiments/MR6_6",getAdminCredentials());
		delete("/REST/projects/" + USER_PROJECT + "/subjects/6",getUserCredentials());
		
	}
}
