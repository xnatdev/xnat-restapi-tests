/*
 * TestInvalidUserAccess.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestInvalidUserAccess extends BaseTestCase {
	private static final String PROJECT = createProjectId("TIUA");

	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, RESTException {
		//upload data
		put(TestInvalidUserAccess.class,"/REST/projects/"+ PROJECT,getAdminCredentials(),NONE);

	}//
	
	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws IOException, RESTException {
		delete(TestInvalidUserAccess.class,"/REST/projects/"+ PROJECT + "?removeFiles=true",getAdminCredentials(),NONE);
	}

	@Before
	public void setUp() throws IOException, RESTException {		
		//add subject
		post("/REST/projects/" + PROJECT + "/subjects?format=xml", getAdminCredentials(),"test_subject_v1.xml");

		//add session
		post("/REST/projects/" + PROJECT + "/subjects/1/experiments?format=xml", getAdminCredentials(),"test_expt_v1.xml");

		//add assessor
		post("/REST/projects/" + PROJECT + "/subjects/1/experiments/JUNIT1_E1/assessors?format=xml", getAdminCredentials(),"test_asst_v1.xml");

	}
	
	@Test
	public void testProtectedXMLCRUD() throws IOException, RESTException{
		//set accessibility
		put("/REST/projects/" + PROJECT + "/accessibility/protected?format=xml", getAdminCredentials());
		
		//confirm change
		ResponseWrapper response =get("/REST/projects/" + PROJECT + "/accessibility?format=xml", getAdminCredentials());
		if(response.getResponseTxt().equals("protected")){
			assertTrue(true);
		}else{
			fail("project accessibility modification failed.");
		}
		
		//confirm invalid user can't access
		try {
			get("/REST/projects/" + PROJECT + "/accessibility?format=xml", getInvalidCredentials());

			fail("Invalid user accessed accessibility.");
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=get("/REST/projects/" + PROJECT + "/accessibility?format=xml", getUserCredentials());
			if(response.getResponseTxt().equals("protected")){
				assertTrue(true);
			}else{
				fail("foreign user access to protected project failed.");
			}
			
			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/accessibility/public?format=xml", getInvalidCredentials());
			
			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user modified accessibility.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/accessibility/public?format=xml", getUserCredentials());
            
			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user modified accessibility.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		//project modification-deletion
		try {
			response=delete("/REST/projects/" + PROJECT + "?format=xml&removeFiles=true", getInvalidCredentials());

			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user deleted project.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=delete("/REST/projects/" + PROJECT + "?format=xml&removeFiles=true", getUserCredentials());

			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user deleted project.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		

		
		try {
			response=put("/REST/projects/" + PROJECT + "?format=xml&alias=JUNIT_TEST", getInvalidCredentials());

			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user modified alias.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "?format=xml&alias=JUNIT_TEST", getUserCredentials());
			
			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user modified alias.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		//subject modification-deletion
		try {
			response=post("/REST/projects/" + PROJECT + "/subjects?format=xml", getInvalidCredentials(),"iu_subject_v1.xml");

			if(response.getResponseTxt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user logged in.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/subjects/2?format=xml", getInvalidCredentials(),"iu_subject_v1.xml");

			if(response.txt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user PUT subject.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}

		try {
			response=post("/REST/projects/" + PROJECT + "/subjects?format=xml", getUserCredentials(),"iu_subject_v1.xml");

			if(response.txt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user POSTed subject.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/subjects/2?format=xml", getUserCredentials(),"iu_subject_v1.xml");

			if(response.txt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user PUT subject.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/subjects/1?format=xml&req_format=qs&gender=male", getInvalidCredentials());

			if(response.txt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user PUT subject.");
			}			
		}  catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
		
		try {
			response=put("/REST/projects/" + PROJECT + "/subjects/1?format=xml&req_format=qs&gender=male", getUserCredentials());

			if(response.txt().indexOf("<!-- LOGIN -->")>-1){
				assertTrue(true);
			}else{
				fail("Invalid user PUT subject.");
			}			
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
	}
}
