/*
 * TestSubjAssessmentResources.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import com.google.common.collect.Lists;
import org.junit.*;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.fail;

public class TestSubjAssessmentResources extends BaseTestCase {
    public static List<String> projects = Lists.newArrayList();

    private static String registerProject(final String header) {
        final String projectId = createProjectId(header);
        projects.add(projectId);
        return projectId;
    }

    @AfterClass
    public static void tearDownAfterClass() throws IOException {
        for (final String project : projects) {
            try {
                delete(TestSubjAssessmentResources.class, "/REST/projects/" + project + "?removeFiles=true", getUserCredentials(), NONE);
            } catch (Throwable e) {
                //ignore
            }
        }
    }

    @Before
    public void setUp() throws IOException, RESTException {
        CURRENT_PROJECT = registerProject("TSA");

        createProject(CURRENT_PROJECT, null, getUserCredentials());

        createSubject(CURRENT_PROJECT, "1", null, getUserCredentials());
    }

    @After
    public void tearDown() throws IOException, RESTException {
        delete("/REST/projects/" + CURRENT_PROJECT + "?removeFiles=true", getUserCredentials());
    }

    @Test
    public void testDateFormatValidation() throws IOException {
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_bad_date.xml");

            fail("Stored session xml with invalid date.");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testIntegerFormatValidation() throws IOException {
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_bad_int.xml");

            fail("Stored session xml with invalid integer.");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    @Ignore("Not supported yet")
    public void testURIFormatValidation() throws IOException, RESTException {
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_bad_URI.xml");

            fail("Stored session xml with invalid URI.");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_bad_URI2.xml");

            fail("Stored session xml with URI outside archive space.");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
        try {
            //relative path is OK
            put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/12414324?format=xml", getUserCredentials(), "test_expt_bad_URI3.xml");
        } catch (Exception e) {
            fail("Failed to save URI with relative path" + e.getMessage());
        }
        try {
            //windows slash is OK
            put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/1242?format=xml", getUserCredentials(), "test_expt_bad_URI4.xml");
        } catch (Exception e) {
            fail("Failed to save URI with windows slash" + e.getMessage());
        }
        try {
            put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/124124?format=xml", getUserCredentials(), "test_expt_bad_URI5.xml");

            fail("Stored session xml with URI /../..");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testLengthValidation() throws IOException {
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_bad_length.xml");

            fail("Stored session xml with marker > 255.");
        } catch (RESTException e) {
            if (e.getStatusCode() != 400) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testInvalidSubjectValidation() throws IOException {
        try {
            post("/REST/projects/" + CURRENT_PROJECT + "/subjects/324234/experiments?format=xml", getUserCredentials(), "test_expt_bad_length.xml");

            fail("Stored session xml for invalid subject");
        } catch (RESTException e) {
            if (e.getStatusCode() != 404) {
                fail(e.getMessage());
            }
        }
    }

    @Test
    public void testXMLCRUD() throws IOException, SAXException, ComparisonException, RESTException {
        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=html", getUserCredentials());
        post("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=xml", getUserCredentials(), "test_expt_v1.xml");

        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials());

        ResponseWrapper response = get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials());

        //compare to original
        Map<Class, List<String>> ignore = new Hashtable<>();

        ignore.put(XnatMrsessiondataBean.class, new ArrayList<String>());
        ignore.get(XnatMrsessiondataBean.class).add("project");

        response.compareBeanXML(GetSourceFile("test_expt_v1.xml"), ignore);

        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=html", getUserCredentials());
        put("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials(), "test_expt_v2.xml");
        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials());
        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=html", getUserCredentials());
        delete("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials());
        get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments?format=html", getUserCredentials());

        try {
            get("/REST/projects/" + CURRENT_PROJECT + "/subjects/1/experiments/JUNIT1_E1?format=xml", getUserCredentials());

            fail("Item should have been deleted");
        } catch (RESTException e) {
            if (e.getStatusCode() != 404) {
                fail(e.getMessage());
            }
        }
    }

    private String CURRENT_PROJECT = null;
}
