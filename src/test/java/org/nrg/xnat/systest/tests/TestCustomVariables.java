/*
 * TestCustomVariables.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestCustomVariables extends BaseTestCase {
	

	private static  String USER_PROJECT = createProjectId("CV");

	@Before
	public void setUp() throws IOException, RESTException {
		//create new projects for each test
		USER_PROJECT = createProjectId("SHAR");
		
		put("/REST/projects/"+USER_PROJECT + "?format=xml",getUserCredentials());
	}

	@After
	public void tearDown() throws IOException, RESTException {

		try {
			delete("/REST/projects/"+USER_PROJECT + "?format=xml",getAdminCredentials());
			
		} catch (Throwable e) {
		}
	}
	
	@Test
	public void testPUTGET() throws IOException, RESTException, JSONException {
		String params="";
		params+="&group=control";
		params+="&src=12";
		params+="&pi_firstname=Tim";
		params+="&pi_lastname=Olsen";
		params+="&dob=1990-09-08";
		params+="&gender=male";
		params+="&handedness=left";
		
		//create subject
		put("/REST/projects/" + USER_PROJECT + "/subjects/1?format=xml&req_format=qs"+params,getAdminCredentials());

		//create session
		put("/REST/projects/" + USER_PROJECT+ "/subjects/1/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getAdminCredentials());

		//add field
		put("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments/MR1?xnat:mrSessionData/fields/field[name%3Dtest01]/field=12",getUserCredentials());
		
		ResponseWrapper fields=get("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments?xnat:mrSessionData/label=MR1&format=json&columns=xnat:mrSessionData/fields/field/field,xnat:mrSessionData/fields/field/name",getUserCredentials());
		Map<String,Object> firstField=new HashMap<String,Object>(){{put("xnat:mrsessiondata/fields/field/field","12");put("xnat:mrsessiondata/fields/field/name","test01");}};
		assertTrue(containsJSONObject(firstField,fields.jsonList()));
		
		//modify value
		put("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments/MR1?xnat:mrSessionData/fields/field[name%3Dtest01]/field=14",getUserCredentials());
		fields=get("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments?xnat:mrSessionData/label=MR1&format=json&columns=xnat:mrSessionData/fields/field/field,xnat:mrSessionData/fields/field/name",getUserCredentials());
		Map<String,Object> secondField=new HashMap<String,Object>(){{put("xnat:mrsessiondata/fields/field/field","14");put("xnat:mrsessiondata/fields/field/name","test01");}};
		assertTrue(containsJSONObject(secondField,fields.jsonList()));
		
		//modify value via form post
		Map<String,Object> body=new HashMap<String,Object>(){{put("body","xnat:mrSessionData/fields/field[name%3Dtest01]/field=15");put("Content-Type","application/x-www-form-urlencoded");}};
		put("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments/MR1",getUserCredentials(),body);
		fields=get("/REST/projects/" + USER_PROJECT + "/subjects/1/experiments?xnat:mrSessionData/label=MR1&format=json&columns=xnat:mrSessionData/fields/field/field,xnat:mrSessionData/fields/field/name",getUserCredentials());
		secondField=new HashMap<String,Object>(){{put("xnat:mrsessiondata/fields/field/field","15");put("xnat:mrsessiondata/fields/field/name","test01");}};
		assertTrue(containsJSONObject(secondField,fields.jsonList()));
	}
}
