/*
 * TestUserAccess.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestUserAccess extends BaseTestCase {

	@Test
	public void testValidAccess() throws IOException {
		try {
			ResponseWrapper response=get("/REST/projects?format=xml",getUserCredentials());
			assertNotNull(response.getResponseTxt());
		} catch (RESTException e) {
			if(e.getStatusCode()!=403)
				fail(e.getMessage());
		}
	}

	@Test
	public void testInvalidAccess() throws IOException {
		try {
			get("/REST/projects?format=xml", getInvalidCredentials());
			fail("401 expected");
		} catch (RESTException e) {
			if(e.getStatusCode()!=401)
				fail(e.getMessage());
		}
	}
}
