/*
 * TestWorkflows.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertTrue;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.nrg.xnat.systest.BaseTestCase;

public class TestWorkflows extends BaseTestCase {
	
	private static  String ADMIN_PROJECT;
	private String ADMIN_EXP;
	
	@Before
	public void setUp() throws Exception, RESTException {
		//create new projects for each test
		ADMIN_PROJECT = createProjectId("TestWorkflow_");
		
		// Create User and Admin Projects
        put("/REST/projects/" + ADMIN_PROJECT + "?format=xml",getAdminCredentials());

		// Create Subjects
		put("/REST/projects/"+ADMIN_PROJECT + "/subjects/workflow_subj_1?format=xml",getAdminCredentials());
		
		// Create Experiments
		ADMIN_EXP = put("/REST/projects/" + ADMIN_PROJECT + "/subjects/workflow_subj_1/experiments/workflow_exp_1?req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getAdminCredentials()).getResponseTxt1stLine();
		
	}
	
	@After
	public void tearDown() throws Exception, RESTException {
		try {
			delete("/REST/projects/" + ADMIN_PROJECT,getAdminCredentials());
		} catch (Exception e) { }
	}
	
	@Test 
	public void testModifyWorkflowStatus() throws Exception, RESTException{
		
		// Upload a workflow
		put("/REST/workflows?wrk:workflowData/status=Queued&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/data_type=xnat:mrSessionData&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials());
		
		// Change the status to "Complete"
		put("/REST/workflows?wrk:workflowData/status=Complete&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials());
		
		//Verify that the status was successfully changed
		JSONObject obj = parseJSON(get("/REST/workflows?format=json&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials()));
		String status = obj.getJSONArray("items").getJSONObject(0).getJSONObject("data_fields").getString("status");
		assertTrue("Status is not correct", status.equals("Complete"));
	}	
	
	@Test
	public void testGetWorkflowByWorkflowId() throws Exception, RESTException{
		//Upload a workflow
		put("/REST/workflows?wrk:workflowData/status=Queued&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/data_type=xnat:mrSessionData&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials());
		
		//Download and Extract the Workflow ID
		JSONObject obj = parseJSON(get("/REST/workflows?format=json&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials()));
		String id = Integer.toString(obj.getJSONArray("items").getJSONObject(0).getJSONObject("data_fields").getInt("wrk_workflowData_id"));
		
		//Attempt to download the workflow using the workflow Id
		get("/REST/workflows/"+id, getAdminCredentials());
	}
	
	@Test 
	public void testWorkflowUserPrivileges() throws Exception, RESTException{
		
		// Upload a workflow as administrator.
		put( "/REST/workflows?wrk:workflowData/status=Queued&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/data_type=xnat:mrSessionData&wrk:workflowData/id=" + ADMIN_EXP, getAdminCredentials());
				
		try{
			// Attempt to modify the workflow as a normal user.
			put("/REST/workflows?wrk:workflowData/status=Complete&wrk:workflowData/pipeline_name=WORKFLOW_TEST&wrk:workflowData/launch_time=2013-05-20%2013:51:25.089&wrk:workflowData/id=" + ADMIN_EXP, getUserCredentials());
		}catch(RESTException e){
			if(e.getStatusCode() == 403){
			    return; // Since we were expecting it to fail, return here.
            }
		}
		
		//Throw an Exception if we made it this far.
		throw new Exception("FAILED: Should not be able to modify a workflow that you did not insert.");
	}
}
