/*
 * TestAnonymizer.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.iod.module.macro.Code;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.nrg.config.entities.Configuration;
import org.nrg.config.entities.ConfigurationData;
import org.nrg.framework.constants.Scope;
import org.nrg.xnat.systest.BaseTestCase;

import com.google.common.collect.Lists;

public class TestAnonymizer extends BaseTestCase {
	private final static String[] SUBJECTS={"JUNIT_ANON_SUBJ1", "JUNIT_ANON_SUBJ2"};
	private final static String TEST_ZIP = "localizer.zip";
	private URLConnection urlConn;


	String CURRENT_PROJECT=null;
	
	@Before
	public void setUp() throws IOException, RESTException {
		CURRENT_PROJECT= createProjectId("ANON");
			//only project[0] is used by all projects, so no reason to re create all of them each time
			put("/REST/projects/" + CURRENT_PROJECT + "?format=xml&req_format=qs",getUserCredentials());
            put("/REST/projects/" + CURRENT_PROJECT + "/prearchive_code/0",getUserCredentials());

			//create subject
			put("/REST/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "?format=xml&req_format=qs&gender=male",getUserCredentials());
			
			//create session
			put("/REST/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());

	}

	@After
	public void tearDown() throws IOException, RESTException {
		delete("/REST/projects/" + CURRENT_PROJECT + "?removeFiles=true",getUserCredentials());
		if(otherProject!=null){
			delete("/REST/projects/" + otherProject + "?removeFiles=true",getUserCredentials());
			otherProject=null;
		}
	}
	
	/**
	 * Go through the setup and teardown process without doing anything.
	 */
	@Test
	public void testNothing() {}
	
	/**
	 * Convert from an HTTP response to a string. Useful for marshalling
	 * XML/JSON responses.
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private static String convertStreamToString(InputStream is) throws IOException {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line + "\n");
	    }
	    is.close();
	    return sb.toString();
	}
	
	/**
	 * Convenience function that uploads that the file at the given path to the given project 
	 * @param project
	 * @param scriptPath
	 * @throws IOException
	 */
	public void uploadScript(String project, String scriptPath) throws IOException, RESTException {
		if (project != null) {
			put("/REST/config/edit/projects/"+ project + "/image/dicom/script?inbody=true", getUserCredentials(),scriptPath);
		}
		else {
			put("/REST/config/edit/image/dicom/script?inbody=true", getAdminCredentials(),scriptPath);
		}
	}
	
	/**
	 * Convenience function that gets the latest edit script for the given project
	 * @param project
	 * @return
	 * @throws IOException
	 * @throws JSONException 
	 */
	public String getScript(String project) throws IOException, RESTException, JSONException  {
		String url;
		if (project != null) {
			url="/REST/config/edit/projects/"+ project + "/image/dicom/script?format=json";
		}
		else {
			url="/REST/config/edit/image/dicom/script?format=json";
		}
		List<JSONObject> objects=parseJSONTable(get(url,getAdminCredentials()));
		return (String)objects.get(0).get("script");
	}
	
	/**
	 * Test uploading and downloading multiple scripts to the a project.
	 * @throws JSONException 
	 */
	@Test
	public void testUploadScriptToProject() throws IOException, RESTException, JSONException{
		// upload an anon script to project
		this.uploadScript(CURRENT_PROJECT, "anon1.das");
		assertTrue(FileUtils.readFileToString(GetSourceFile("anon1.das")).equals(this.getScript(CURRENT_PROJECT)));
		
		// upload another
		this.uploadScript(CURRENT_PROJECT, "anon2.das");
		// ensure that the most recent script is downloaded
		assertTrue(FileUtils.readFileToString(GetSourceFile("anon2.das")).equals(this.getScript(CURRENT_PROJECT)));
	}
	
	/**
	 * Test to see if a file uploaded via the Zip importer to the prearchive and then archived to a project
	 * has had both the site-wide and project specific anon scripts applied to it. 
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testPrearchiveProjectZipUpload() throws IOException, RESTException, JSONException{
		final String tag="data_proj_import";
		this.uploadScript(CURRENT_PROJECT, "anon1.das");

		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+CURRENT_PROJECT,
				getUserCredentials(),
				"images/localizer.zip");
		final String expected_dest=getExptURL(CURRENT_PROJECT, "PATIENT_NAME", "PATIENT_ID");
		get(expected_dest,getUserCredentials());
		//test misc resource
		ResponseWrapper scan1=get(expected_dest +"/scans/1/resources/DICOM/files/1.dcm",getUserCredentials());

		Configuration c = this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
		Configuration adminC = this.getScriptTable(null, getUserCredentials());

		File downloaded = scan1.file();
		Code[] codes = this.getCodes(downloaded);
		assertEquals(2,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(c.getId()).toString()));
	}

	/**
	 * Test that uploading to a project via the zip uploader applies both
	 * the site-wide and project specific scripts to the DICOM files.
	 * @throws JSONException 
	 */
	@Test
	public void testProjectZipUpload() throws IOException, RESTException, JSONException{
		final String tag="project_anon_script";
		// upload a project specific script
		this.uploadScript(CURRENT_PROJECT, "anon1.das");
		
		// create the experiment
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], "MR1");
		
		// upload DICOM to the experiment
		String projectURL = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + "MR1";
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=true&dest=" + "/archive" + projectURL,
												getUserCredentials(),
												"images/localizer.zip");
		//retrieve one of the DICOM files
		String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
		File d = this.getMostRecentDcm(tag, url);
		
		// check if the script was applied
		Configuration c = this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Code[] codes = this.getCodes(d);
		assertEquals(2,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(c.getId()).toString()));
	}
	
	/**
	 * Test that applying a project specific script adds the name 
	 * of the project to the DICOM header
	 */
	@Test
	public void testSessionLabelInHeader() throws IOException, RESTException{
		final String tag="project_anon_script";
		// upload a project specific script
		this.uploadScript(CURRENT_PROJECT, "anon1.das");
		
		// create the experiment
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], "MR1");
		
		// upload DICOM to the experiment
		String projectURL = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + "MR1";
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=true&dest=" + "/archive" + projectURL,
												getUserCredentials(),
												"images/localizer.zip");
		//retrieve one of the DICOM files
		String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
		File d = this.getMostRecentDcm(tag, url);
		
		final DicomInputStream sample_dis = new DicomInputStream(d);
		final DicomObject sample = sample_dis.readDicomObject();
		String patientId = sample.getString(Tag.PatientID);
		assertTrue(patientId.equals("MR1"));
	}
	
	
	
	/**
	 * Test that uploading a script to a project and then disabling it
	 * keeps the edit script from being applied
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testDisabledProjectScript() throws IOException, RESTException, JSONException {
		final String tag="project_disabled_anon_script";
		// upload a project specific script
		this.uploadScript(CURRENT_PROJECT, "anon1.das");

		// create the experiment
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], "MR1");

		// disable the project-specific edit script
		String uri = "/REST/config/edit/projects/"+CURRENT_PROJECT +"/image/dicom/status?activate=false";
		put(uri,getUserCredentials());

		// upload DICOM to the experiment
		String projectURL = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + "MR1";
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=true&dest=" + "/archive" + projectURL,
				getUserCredentials(),
				"images/localizer.zip");
		//retrieve one of the DICOM files
		String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
		File d = this.getMostRecentDcm(tag, url);

		// check if just the site-wide script was applied
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Code[] codes = this.getCodes(d);
		assertEquals(1,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
	}
	
	/**
	 * Test that disabling the site-wide script and only applies
	 * the project-specific script to the DICOM files.
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testDisabledSiteWideScript()throws IOException, RESTException, JSONException {
		final String tag="project_disabled_anon_script";
		// upload a project specific script
		this.uploadScript(CURRENT_PROJECT, "anon1.das");

		// create the experiment
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], "MR1");

		// disable the site-wide edit script
		String uri = "/REST/config/edit/image/dicom/status?activate=false";
		put(uri,getAdminCredentials());

		// upload DICOM to the experiment
		try {
			String projectURL = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + "MR1";
			post(getServiceURL("import")+"?triggerPipelines=false&overwrite=true&dest=" + "/archive" + projectURL,
					getUserCredentials(),
					"images/localizer.zip");
			
			//retrieve one of the DICOM files
			String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
			File d = this.getMostRecentDcm(tag, url);

			// check if just the project-specific script was applied
			Configuration c= this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
			Code[] codes = this.getCodes(d);
			assertEquals(1,codes.length);
			assertTrue(codes[0].getCodeValue().equals(new Long(c.getId()).toString()));
		}finally{
			//re-enable the script because it is persistent
			String reenable = "/REST/config/edit/image/dicom/status?activate=true";
			put(reenable, getAdminCredentials());
		}
		
	}
	

	/**
	 * Convenience function to get the Configuration object associated with the
	 * given project
	 * @param project
	 * @param userCredentials
	 * @return
	 * @throws IOException
	 * @throws JSONException 
	 */
	public Configuration getScriptTable(String project, Credentials userCredentials) throws IOException, RESTException, JSONException {
		ResponseWrapper response;
		if (project == null) {
			response= get("/REST/config/edit/image/dicom/script?format=json", 
												  userCredentials);
		}
		else {
			response=get("/REST/config/edit/projects/" + project + "/image/dicom/script?format=json", 
					                userCredentials);
		}
		JSONObject jo = response.getResponseAsJSON();
		JSONArray ja = jo.getJSONObject("ResultSet").getJSONArray("Result");
		Configuration c = new Configuration();
		c.setId(Long.parseLong((String)ja.getJSONObject(0).get("id")));
		if (StringUtils.isNotBlank(project)) {
			c.setScope(Scope.Project);
			c.setEntityId((String) ja.getJSONObject(0).get("project"));
		} else {
			c.setScope(Scope.Site);
		}
		ConfigurationData cd = new ConfigurationData();
		cd.setContents((String)ja.getJSONObject(0).get("script"));
		c.setConfigData(cd);
		return c;
	}
	
	/**
	 * Get the URL for the most recent session uploaded to the prearchive. This is 
	 * necessary because when a file is uploaded without a subject or session specified
	 * they are autogenerated by XNAT and we don't know what they are. So to get at the 
	 * session that was just uploaded to the prearchive, we get a list of the all the prearchive
	 * contents and pick the one that was \"uploaded\" most recently.
	 * @return
	 * @throws IOException
	 * @throws JSONException 
	 * @throws ParseException 
	 */
	public String getMostRecentURL() throws IOException, RESTException, JSONException, ParseException {
		ResponseWrapper response=get("/REST/prearchive/projects/?format=json", getAdminCredentials());
		JSONObject jo = response.getResponseAsJSON();
		JSONArray ja = jo.getJSONObject("ResultSet").getJSONArray("Result");
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss.SSS");
		String latest = null;
		Date mostRecent = null;
		for (int i = 0; i < ja.length(); i++) {
			String sUploaded = (String) ja.getJSONObject(i).get("uploaded");
			Date d = sd.parse(sUploaded);
			if (mostRecent == null) { 
				mostRecent = d;
				latest = (String) ja.getJSONObject(i).get("url");
			}
			else if (mostRecent.before(d)) {
				mostRecent = d;
				latest = (String) ja.getJSONObject(i).get("url");
			}
		}
		return latest;
	}
	
	/**
	 * Get a DICOM file from the most recently uploaded session to the prearchive.
	 * @param tag
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws IOException, RESTException 
	 */
	public File getMostRecentDcm (String tag, String url) throws IOException, IOException, RESTException {
		String completeURL ="/REST" + url + "/scans/1/resources/DICOM/files/1.dcm"; 
		ResponseWrapper dcmRequest = get(completeURL,getAdminCredentials());
		return dcmRequest.file();
	}
	
	/**
	 * Test that uploading via the zip uploader to the prearchive applies the 
	 * site-wide script.
	 * @throws IOException
	 * @throws ParseException 
	 * @throws JSONException 
	 * @throws IOException, RESTException 
	 */
	@Test
	public void testDefaultAnonZipUploadSiteWide() throws IOException, JSONException, ParseException, RESTException {
		try {
			final String tag="data_default_anon_upload";
			ResponseWrapper scan = post(getServiceURL("import")+"?triggerPipelines=false",
					                       getUserCredentials(),
					                       "images/localizer.zip");
			
			String latest = this.getMostRecentURL();
			File downloaded = this.getMostRecentDcm(tag, latest);
			Configuration c = this.getScriptTable(null, getAdminCredentials());
			Code[] codes = this.getCodes(downloaded);
			assertEquals(1,codes.length);
			assert(codes[0].getCodeValue().equals(new Long(c.getId()).toString()));
		}
		catch(Throwable e) {
			fail(e.getMessage());
		}
	}
	
	/**
	 * Create the following experiment.
	 * @param project
	 * @param subject
	 * @param experiment
	 * @throws IOException
	 */
	public void createExperiment(String project, String subject, String experiment) throws IOException, RESTException {
		put("/REST/projects/" + project + "/subjects/"+subject+"/experiments/"+experiment+"/scans/1?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable", 
				getUserCredentials());


		// create the scan resource
		//put("/REST/projects/" + project + "/subjects/" + subject + "/experiments/" + experiment + "/scans/1/resources/DICOM", getUserCredentials());

	}
		
	/**
	 * Upload a DICOM file to the \"resources\" section of the experiment.
	 * @param project
	 * @param subject
	 * @param experiment
	 * @throws IOException
	 */
	public void uploadDcmResource(String project, String subject, String experiment) throws IOException, RESTException {
		// create the experiment
		this.createExperiment(project, subject, experiment);
        
        // upload the dicom file
        put("/REST/projects/" + project + "/subjects/" + subject + "/experiments/" + experiment + "/scans/1/resources/DICOM/files/1.dcm?inbody=true", getUserCredentials(),"images" + File.separator + "1.dcm");

	}
	

	/**
	 * Test that uploading an image directly to the \"resource\" section of an experiment
	 * does *not* trigger the site-wide or project-specific edit script application.
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testResourceUpload() throws IOException, RESTException, JSONException {
		try {
			final String tag = "data_default_anon_upload_project_specific";
			// upload an anon script to project
			this.uploadScript(CURRENT_PROJECT, "anon1.das");
			assertTrue(FileUtils.readFileToString(GetSourceFile("anon1.das")).equals(this.getScript(CURRENT_PROJECT)));
			
			// upload another
			this.uploadScript(CURRENT_PROJECT, "anon2.das");
			// ensure that the most recent script is downloaded
			assertTrue(FileUtils.readFileToString(GetSourceFile("anon2.das")).equals(this.getScript(CURRENT_PROJECT)));
			
			// upload a zip to project
			this.uploadDcmResource(CURRENT_PROJECT, SUBJECTS[0] , "MR1");
			final String expected_dest=getExptURL(CURRENT_PROJECT, SUBJECTS[0], "MR1");
			//get the DICOM file
			String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
			
			// There shouldn't be any anonymization done because it's not going through the
			// the upload assistant or zip uploader or prearchive
			File d = this.getMostRecentDcm(tag, url);
			Code [] codes = this.getCodes(d);
			assert(codes == null);
		}
		catch (Throwable e) {
			fail(e.getMessage());
		}
	}

	/**
	 * Test to see if a file uploaded via the Gradual DICOM importer is properly anonymized in reference to the site-wide script
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testDefaultAnonGradualDicomUpload() throws IOException, RESTException, JSONException {
		// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String tag="prearc_gradual_import";
		final String proj1 = CURRENT_PROJECT;
		
		//upload the project specific script
		this.uploadScript(proj1, "anon1.das");
		
		//upload scan
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+proj1,getUserCredentials(),"images/scan1.zip");
		String uri1=post1.getResponseTxt1stLine();
		
		// commit it 
		ResponseWrapper post3=post(uri1+"?action=commit",getUserCredentials());
		assertEquals(uri1,post3.getResponseTxt1stLine());
		
		// download one of the DICOM files
		ResponseWrapper scan1=get(uri1 +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		File downloaded = scan1.file();
		Code[] codes = this.getCodes(downloaded);
		
		// Since the files are sitting in the prearchive the project specific script should not have applied
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		assertEquals(1,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
	}
	
	@Test
	public void testNoProjectScript() throws IOException, RESTException, JSONException {
		// services/import?dest=/archive/projects/{PROJECT)/subjects/SUBJECT/experiments/EXPERIMENT POST
		final String tag="project_anon_script";

		// create the experiment
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], "MR1");
		
		// upload DICOM to the experiment
		String projectURL = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + "MR1";
		post(getServiceURL("import")+"?triggerPipelines=false&overwrite=true&dest=" + "/archive" + projectURL,
												getUserCredentials(),
												"images/localizer.zip");
		//retrieve one of the DICOM files
		String url = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/MR1"; 
		File d = this.getMostRecentDcm(tag, url);
		
		// check if the script was applied
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Code[] codes = this.getCodes(d);
		assertEquals(1,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
	}
	
	/**
	 * Test that uploading via the gradual DICOM importer into archiving into a project
	 * applies the site-wide and project specific edit scripts to the DICOM files. 
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testDefaultProjectGradualDicomUpload() throws IOException, RESTException, JSONException {
		final String tag="prearc_gradual_import";
		final String proj1 = CURRENT_PROJECT;
		
		//upload the project specific script
		this.uploadScript(proj1, "anon1.das");
		
		//upload scan and set it to auto-archive
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+proj1,getUserCredentials(),"images/scan1.zip");
		String uri1=post1.getResponseTxt1stLine();
		
		// commit it 
		ResponseWrapper post3=post(uri1+"?action=commit&auto-archive=true",getUserCredentials());
		String uri2 = post3.getResponseTxt1stLine();
		
		// download one of the DICOM files
		ResponseWrapper scan1=get(uri2 +"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		File downloaded = scan1.file();
		Code[] codes = this.getCodes(downloaded);
		
		// Ensure that the site-wide and project-specific scripts have been applied.
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Configuration c = this.getScriptTable(proj1, getUserCredentials());
		assertEquals(2,codes.length);
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(c.getId()).toString()));
	}
	
	/**
	 * Test that changing the subject name of a session triggers a re-application of the
	 * project specific script.
	 * 
	 * After the change the session should have the following script application history:
	 * 1. Site-wide 
	 * 2. Project specific
	 * 3. Project specific
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testSubjectChange() throws IOException, RESTException, JSONException {
		String tag = "subject_change_gradual";
		// upload script for first project
		this.uploadScript(CURRENT_PROJECT, "anon1.das");
		String session = "MR1";
		String newSubject = "subj_mod";
		
		
		//upload zip to prearchive
		ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+CURRENT_PROJECT+"/subjects/00001/experiments/case01",
									  getUserCredentials(),
									  "images/scan1.zip");
		String uri1=post1.getResponseTxt1stLine();
				
		// change the subject field
		put(getExptURL(CURRENT_PROJECT, "00001", "case01")+"?subject_ID="+ newSubject,
				getUserCredentials());
		
		String newURI = "/projects/" + CURRENT_PROJECT + "/subjects/" + newSubject + "/experiments/" + "case01";
		
		// download one of the DICOM files from the new project
		ResponseWrapper scan1 = null;
		try {
			scan1=get("/REST" + newURI + "/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		}
		catch (Throwable e) {
			fail(e.getMessage());
		}
		
		// make sure the code values match up. 
		File downloaded = scan1.file();
		Code[] codes = this.getCodes(downloaded);
		
		assertEquals(3,codes.length);
		
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Configuration proj1C = this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(proj1C.getId()).toString()));
		assertTrue(codes[2].getCodeValue().equals(new Long(proj1C.getId()).toString()));
	}
	
	/**
	 * Test that changing the label of an experiment reapplies the edit script associated
	 * with that experiment's project.
	 * 
	 * Also test that the anon script writes the DICOM header with the name of the new session.
	 * 
	 * After the rename a DICOM file should have the following history:
	 *   - a site-wide edit script application
	 *   - a project-specific edit script application
	 *   - another project-specific script application
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testLabelChange() throws IOException, RESTException, JSONException {
		String tag = "label_change_gradual";
		// upload script for first project
		this.uploadScript(CURRENT_PROJECT, "projectsubjectsession.das");
		String session = "MR1";
		String newSession = "new_MR1";
		
		String uri = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + session;
		this.createExperiment(CURRENT_PROJECT, SUBJECTS[0], session);
		
		String oldurl=getExptURL(CURRENT_PROJECT, "00001", "case01");
		
		//upload zip to prearchive
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/" 
									      + CURRENT_PROJECT + "/subjects/00001/experiments/case01",
									  getUserCredentials(),
									  "images/scan1.zip");
		
		// change the label
		put(oldurl+"?label="+newSession,
				getUserCredentials());
		
		String newURI = "/projects/" + CURRENT_PROJECT + "/subjects/" + SUBJECTS[0] + "/experiments/" + newSession;
		
		// download one of the DICOM files from the new project
		ResponseWrapper scan1 = null;
		try {
			scan1=get("/REST" + newURI + "/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		}
		catch (Throwable e) {
			fail(e.getMessage());
		}
		
		// make sure the code values match up. 
		File downloaded = scan1.file();
		
		final DicomInputStream sample_dis = new DicomInputStream(downloaded);
		final DicomObject sample = sample_dis.readDicomObject();
		
		String patientId = sample.getString(Tag.PatientID);
		
		assertTrue(patientId.equals(newSession));
		
		Code[] codes = this.getCodes(downloaded);
		
		assertEquals(3,codes.length);
		
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Configuration proj1C = this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(proj1C.getId()).toString()));
		assertTrue(codes[2].getCodeValue().equals(new Long(proj1C.getId()).toString()));
	}
	
	String otherProject=null;
	
	/**
	 * Test that moving an old project to a new project applies the 
	 * new project's anon script. Uploaded via the Gradual DICOM Importer
	 * 
	 * The script application history on this experiment should be:
	 * 1. Site wide
	 * 2. old project script
	 * 3. new project script
	 * @throws IOException
	 * @throws JSONException 
	 */
	@Test
	public void testProjectChange() throws IOException, RESTException, JSONException {
		otherProject= createProjectId("ANON");
		
		put("/REST/projects/" + otherProject + "?format=xml&req_format=qs",getUserCredentials());

		//create subject
		put("/REST/projects/" + otherProject + "/subjects/" + SUBJECTS[1] + "?format=xml&req_format=qs&gender=male",getUserCredentials());
		
		//create session
		put("/REST/projects/" + otherProject + "/subjects/" + SUBJECTS[1] + "/experiments/MR1?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());

		
		String tag = "project_change_gradual_";
		// upload script for first project
		this.uploadScript(CURRENT_PROJECT, "anon1.das");
		// upload script for new project, the scripts are the same because we don't care about the content
		this.uploadScript(otherProject, "anon1.das");
		
		//upload zip to prearchive
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/archive/projects/"+CURRENT_PROJECT+"/subjects/00001/experiments/case01",getUserCredentials(),"images/scan1.zip");
				
		// move the subject to the new project, this is necessary to avoid a bug deleting the new project at tearDown() time
		put("/REST/projects/" + CURRENT_PROJECT + "/subjects/00001/projects/" + otherProject + "?primary=true",
									  getAdminCredentials());
		
		// move the old project to the new project 
		put( "/REST/projects/" + CURRENT_PROJECT + "/subjects/00001/experiments/case01/projects/" + otherProject+"?primary=true",
									  getAdminCredentials());
		String uri3 = "/REST/projects/"+otherProject+"/subjects/00001/experiments/case01";
		
		// download one of the DICOM files from the new project
		ResponseWrapper scan1 = null;
		try {
			scan1=get(uri3+"/scans/1/resources/DICOM/files/000001.IMA",getUserCredentials());
		}
		catch (Throwable e) {
			fail(e.getMessage());
		}
		
		// make sure the code values match up
		File downloaded = scan1.file();
		Code[] codes = this.getCodes(downloaded);
		
		assertEquals(3,codes.length);
		
		Configuration adminC = this.getScriptTable(null, getUserCredentials());
		Configuration proj1C = this.getScriptTable(CURRENT_PROJECT, getUserCredentials());
		Configuration proj2C = this.getScriptTable(otherProject, getUserCredentials());
		assertTrue(codes[0].getCodeValue().equals(new Long(adminC.getId()).toString()));
		assertTrue(codes[1].getCodeValue().equals(new Long(proj1C.getId()).toString()));
		assertTrue(codes[2].getCodeValue().equals(new Long(proj2C.getId()).toString()));
	}
	
	/**
	 * Return the deidentification elements in the given DICOM file 
	 * @param dcmFile
	 * @return
	 * @throws IOException
	 */
	private Code[] getCodes(File dcmFile) throws IOException {
		final DicomInputStream sample_dis = new DicomInputStream(dcmFile);
		final DicomObject sample = sample_dis.readDicomObject();
		Code[] codes = Code.toCodes(sample.get(Tag.DeidentificationMethodCodeSequence));
		sample_dis.close();
		return codes;
	}
	

	/**
	 * This mimics the behavior of the upload assistant but sends multiple copies of the same data concurrently.
	 * ANON IS TURNED ON in TestAnonymizer
	 * @throws IOException
	 * @throws InterruptedException 
	 * @throws JSONException 
	 * @throws RESTException 
	 */
	@Ignore
	@Test
	public void testConcurrentGradualImportWAnon() throws InterruptedException, IOException, JSONException, RESTException {
		final String tag="concurrent_import";
		
		final List<ResponseWrapper> responses=Lists.newArrayList();
		
		class ThreadCounter implements Callable<Integer> {
			public int running;
			public int threads;
			public ThreadCounter(int count){
				this.running=count;
				this.threads=count;
			}
			
			@Override
			public synchronized Integer call() throws IOException {
				return running--;
			}
		};
		
		final ThreadCounter counter=new ThreadCounter(5);
		
		class Uploader extends Thread{
			int threadNum;
			public Uploader(int num){
				threadNum=num;
			}
			
			@Override
			public void run() {
				try {
					ResponseWrapper post1=post(getServiceURL("import")+"?triggerPipelines=false&import-handler=DICOM-zip&dest=/prearchive/projects/"+CURRENT_PROJECT,getUserCredentials(),"images/scan1.zip");
					responses.add(post1);
				} catch (Throwable e) {
					//we expect most of these to fail.  Only the first one should work.
				}finally{
					try {
						counter.call();
					} catch (Exception e) {
					}
				}
			}
		};
		
		List<Thread> threads=Lists.newArrayList();
		for(int i=0;i<counter.threads;i++){
			threads.add(new Uploader(i));
		}
		
		for(Thread t: threads){
			t.start();
		}
		
		while(counter.running>0){
			Thread.currentThread();
			Thread.sleep(1000);
		}
		
		assertEquals("Expected one successful response.",1,responses.size());
		
		final ResponseWrapper response=responses.get(0);
		
		assertEquals(200,response.getStatusCode());
		
		final String uri1=response.getResponseTxt1stLine();
		
		//issue commit action with autoarchive=true
		ResponseWrapper post3=post(uri1+"?action=commit&AA=true",getUserCredentials());
		
		final String arc=post3.getResponseTxt1stLine();
		
		assertFalse("!=\r\n" + uri1 + "\r\n" + arc,uri1.equals(arc));
		
		assertTrue(arc.startsWith("/data/archive"));
		
		//test misc resource
		ResponseWrapper scan1=get(arc +"/scans/1/resources/DICOM/files?format=json",getUserCredentials());
		assertEquals("Expected 24 files",24,scan1.getResponseAsJSONList().size());
	}

}
