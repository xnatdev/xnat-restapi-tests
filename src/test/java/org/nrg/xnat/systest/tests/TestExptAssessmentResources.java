/*
 * TestExptAssessmentResources.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import com.google.common.collect.Maps;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatQcmanualassessordataBean;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestExptAssessmentResources  extends BaseTestCase{
	final String PROJECT1= createProjectId("TEA");
	final String PROJECT2= createProjectId("TEA");
	
	String session;
	
	@Before
	public void setUp() throws IOException,RESTException {

		put("/REST/projects/"+ PROJECT1,getUserCredentials());

		put("/REST/projects/"+ PROJECT2 + "?format=xml", getUserCredentials(),"test_project_v1.xml");
		
		post("/REST/projects/"+ PROJECT2 + "/subjects?format=xml", getUserCredentials(),"test_subject_v1.xml");
		
		ResponseWrapper rw=post("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments?format=xml",getUserCredentials(),"test_expt_v1.xml");
		session=rw.getResponseTxt1stLine();
		
		session=session.substring(session.lastIndexOf('/')+1);
	}

	@After
	public void tearDown() throws IOException,RESTException {
		delete("/REST/projects/"+ PROJECT1 + "?format=xml&removeFiles=true", getUserCredentials());
		delete("/REST/projects/"+ PROJECT2 + "?format=xml&removeFiles=true", getUserCredentials());
	}
	
	@Test
	public void testXMLCRUD() throws IOException,RESTException, JSONException, SAXException, ComparisonException{
		get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors?format=html", getUserCredentials());
		
		post("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors?format=xml", getUserCredentials(), "test_asst_v1.xml");

			
		ResponseWrapper response=get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1?format=xml", getUserCredentials());

		Map<Class,List<String>> ignore=Maps.newHashMap();
		ignore.put(XnatQcmanualassessordataBean.class, new ArrayList<String>());
		ignore.get(XnatQcmanualassessordataBean.class).add("project");
		
		//compare to original
		response.compareBeanXML(GetSourceFile("test_asst_v1.xml"), ignore);

		//test listing isn't broken
		get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors?format=html", getUserCredentials());

		//modify assessment
		put("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1?format=xml", getUserCredentials(), "test_asst_v2.xml");

		//get
		get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1?format=xml", getUserCredentials());

		//confirm listing still works
		get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors?format=html", getUserCredentials());

		//parse json list
		List<JSONObject> assmts=parseJSONTable(get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1/projects?format=json",getUserCredentials()));
		assertEquals("Should include only one assessment",assmts.size(),1);
		
		//delete the assessment
		delete("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1?format=xml", getUserCredentials());


		//parse json list
		assmts=parseJSONTable(get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors?format=json",getUserCredentials()));
		assertEquals("Should include no assessments",assmts.size(),0);
			
		//confirm deletion
		try{
			get("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/JUNIT1_E1/assessors/JUNIT_E1_R1?format=xml", getUserCredentials());

			fail("Item should have been deleted");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		
		response=get("/REST/experiments/"+session+"/assessors?format=json", getUserCredentials());
		assertEquals(0,response.getResponseAsJSONList().size());
		
		//check again without project in url
		put("/REST/projects/"+ PROJECT2 + "/subjects/1/experiments/"+session+"/assessors/JUNIT_E1_R1?format=xml&project="+PROJECT2, getUserCredentials(),"test_asst_v2.xml");

		response=get("/REST/experiments/"+session+"/assessors?format=json", getUserCredentials());
		List<JSONObject> assemts=response.getResponseAsJSONList();
		assertEquals(1,assemts.size());
		
		String ID=assemts.get(0).getString("ID");
		
		//check get
		get("/REST/experiments/"+session+"/assessors/"+ ID + "?format=xml", getUserCredentials());
		
		//check no sharing
		response=get("/REST/experiments/"+session+"/assessors/"+ ID + "/projects?format=json",getUserCredentials());
		assertEquals(response.getResponseAsJSONList().size(), 1);
		
		//share into other project
		put("/REST/experiments/"+session+"/assessors/"+ ID + "/projects/" + PROJECT1 + "?format=xml", getUserCredentials());
		
		//count list of shared projects 1
		response=get("/REST/experiments/"+session+"/assessors/"+ ID + "/projects?format=json",getUserCredentials());
		assertEquals(response.getResponseAsJSONList().size(), 2);
		
		//delete from shared projects
		delete("/REST/experiments/"+session+"/assessors/"+ ID + "/projects/"+ PROJECT1 +"?format=json",getUserCredentials());
		
		//count list of shared projects 0
		response=get("/REST/experiments/"+session+"/assessors/"+ ID + "/projects?format=json",getUserCredentials());
		assertEquals(response.getResponseAsJSONList().size(), 1);
		
		//confirm standard get
		get("/REST/experiments/"+session+"/assessors/"+ ID + "?format=xml", getUserCredentials());

		//confirm listing
		response=get("/REST/experiments/"+session+"/assessors?format=json", getUserCredentials());
		assertEquals(response.getResponseAsJSONList().size(), 1);
		
		//delete assessment		
		delete("/REST/experiments/"+session+"/assessors/"+ ID + "?format=xml", getUserCredentials());

		//confirm listing
		response=get("/REST/experiments/"+session+"/assessors?format=json", getUserCredentials());
		assertEquals(response.getResponseAsJSONList().size(), 0);
			
		try{
			//confirm direct access fails
			get("/REST/experiments/"+session+"/assessors/"+ ID + "?format=xml", getUserCredentials());
			
			fail("Item should have been deleted");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
	}
}
