/*
 * TestMuchoFiles.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

import static org.junit.Assert.assertTrue;

public class TestMuchoFiles extends BaseTestCase {
	protected int subject_count=3;
	protected int session_count=3;
	protected int scan_count=10;
	protected int recon_count=2;
	protected boolean allowGET=true;
	public String getProject(){
		return "JUNIT_Q";
	}

	@Before
	public void setUp() throws IOException, RESTException {
		put("/REST/projects/" + getProject() + "?format=xml&req_format=qs&alias=" + getProject() + "_ALIAS",getUserCredentials());
	}

	@After
	public void tearDown() throws IOException, RESTException {
		delete("/REST/projects/" + getProject() + "?removeFiles=true",getUserCredentials());
	}
	
//	@Test
	public void testMuchoFiles() throws IOException, RESTException
	{
		boolean _continue=true;
		
		for(int sC=1;sC<=subject_count;sC++){
			put("/REST/projects/" + getProject() + "/subjects/s" + sC + "?format=xml&req_format=qs&gender=male",getUserCredentials());
			
			for(int mC=1;mC<=session_count;mC++){
				put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());

				put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/resources/ZIP/files/"+ TestFileUpload.TEST_ZIP +"?inbody=true",getUserCredentials(),"images/" + File.separator + TestFileUpload.TEST_ZIP);
				
				for(int cC=1;cC<=scan_count;cC++){
					put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable",getUserCredentials());
					put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "/resources/DICOM",getUserCredentials());

					
					for(int fC=1;fC<=3;fC++){
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "/resources/DICOM/files/" + fC + ".dcm?inbody=true",getUserCredentials(),"images/" + fC + ".dcm");
					}
					
					if(!_continue)break;
				}
				
				for(int cC=1;cC<=recon_count;cC++){
					put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "?format=xml&req_format=qs&type=LOCALIZER",getUserCredentials());
					put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "/out/resources/DICOM",getUserCredentials());

					
					for(int fC=1;fC<=3;fC++){
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "/out/resources/DICOM/files/" + fC + ".dcm?inbody=true",getUserCredentials());
					}
					
					if(!_continue)break;
				}

				put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "?format=xml&fixScanTypes=true",getUserCredentials());

				if(!_continue)break;
			}
			
			put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/resources/ZIP/files/" + TestFileUpload.TEST_ZIP + "?inbody=true",getUserCredentials());
			
			if(!_continue)break;
		}
		
		if(allowGET){
			for(int sC=1;sC<=subject_count;sC++){
				
				for(int mC=1;mC<=session_count;mC++){
					get("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "?format=xml",getUserCredentials());

					if(!_continue)break;
					
					for(int cC=1;cC<=scan_count;cC++){
						
						for(int fC=1;fC<=3;fC++){
							get("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "/resources/DICOM/files/" + fC + ".dcm",getUserCredentials());
						}
						if(!_continue)break;
						
					}
					
					for(int cC=1;cC<=recon_count;cC++){
						
						for(int fC=1;fC<=3;fC++){
							get("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" +  sC + "_MR" + mC + "_" + cC + "/out/resources/DICOM/files/" + fC + ".dcm",getUserCredentials());
						}
						if(!_continue)break;
						
					}
				}
				if(!_continue)break;
			}
		}
	}

}
