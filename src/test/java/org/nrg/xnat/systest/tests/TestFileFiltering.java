/*
 * TestFileFiltering.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;


import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.fail;

public class TestFileFiltering extends BaseTestCase{
	private static final String PROJECT1 = createProjectId("TFF");

	/**
	 * Sets up the test.
	 *
	 * @throws IOException When a read or write error occurs.
	 * @throws RESTException When an exception with the REST service occurs.
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, RESTException {
		put(TestFileFiltering.class,getProjectURL(PROJECT1)+"?format=xml&req_format=qs",getAdminCredentials(),NONE);
	}

	/**
	 * Cleans up after the test.
	 *
	 * @throws IOException When a read or write error occurs.
	 * @throws RESTException When an exception with the REST service occurs.
	 */
	@AfterClass
	public static void tearDownAfterClass() throws IOException, RESTException {
		delete(TestFileFiltering.class,getProjectURL(PROJECT1)+"?removeFiles=true",getAdminCredentials(),NONE);
	}

	@Test
	public void testFileFiltering() throws IOException, JSONException, RESTException{
		//process
		final String subject = "1";
		final String session = "MR1";
		
		final String resource = "TEST";
		final String fileName = "images/1.dcm";
		
		createSubject(PROJECT1, subject,null, getAdminCredentials());
		createExpt(PROJECT1, subject,session,XNAT_MR_SESSION_DATA,null, getAdminCredentials());
		
		uploadExptResource(PROJECT1, subject,session, resource, fileName,fileName+"1","content=TEST1", getAdminCredentials());
		uploadExptResource(PROJECT1, subject,session, resource, fileName,fileName+"2","content=TEST1", getAdminCredentials());
		uploadExptResource(PROJECT1, subject,session, resource, fileName,fileName+"3","content=TEST2", getAdminCredentials());
		uploadExptResource(PROJECT1, subject,session, resource, fileName,fileName+"4","content=TEST2", getAdminCredentials());
		uploadExptResource(PROJECT1, subject,session, resource, fileName,fileName+"5","content=TEST3", getAdminCredentials());
		
		//query all
		List<JSONObject> o=BaseTestCase.parseJSONTable(get(getExptURL(PROJECT1, subject, session)+"/resources/"+resource + "/files?format=json",getAdminCredentials()));
		Assert.assertEquals(5,o.size());
		
		//query TEST2
		o=BaseTestCase.parseJSONTable(get(getExptURL(PROJECT1, subject, session)+"/resources/"+resource + "/files?file_content=TEST2&format=json",getAdminCredentials()));
		Assert.assertEquals(2,o.size());
		
		//query TEST3
		o=BaseTestCase.parseJSONTable(get(getExptURL(PROJECT1, subject, session)+"/resources/"+resource + "/files?file_content=TEST3&format=json",getAdminCredentials()));
		Assert.assertEquals(1,o.size());
		
		try {
			validateUpload(getExptURL(PROJECT1, subject, session)+"/resources/"+resource + "/files?file_content=TEST3&index=1", fileName, getAdminCredentials());
			fail("Should not be able to retrieve second file for TEST3");
		} catch (RESTException e) {
			if(e.getStatusCode()!=404)
				fail(e.getMessage());
		}
		
		validateUpload(getExptURL(PROJECT1, subject, session)+"/resources/"+resource + "/files?file_content=TEST2&index=1", fileName, getAdminCredentials());
	}

}
