/*
 * TestPrearchiveMgmtAPI.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

/**
 * 
 */
package org.nrg.xnat.systest.tests;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestPrearchiveMgmtAPI extends BaseTestCase{
	private static final String URI2 = "url";
	
	public static final String SUBJECT_PARAM = "subject";
	public static final String SESSION_PARAM = "name";
	public static final String PROJECT_PARAM = "param";
	private static List<HashMap<String,Object>> sessions= Arrays.asList(
			new HashMap<String,Object>(){{put(SUBJECT_PARAM,"1");put(SESSION_PARAM,"MR11");}}, 
			new HashMap<String,Object>(){{put(SUBJECT_PARAM,"1");put(SESSION_PARAM,"MR12");}}, 
			new HashMap<String,Object>(){{put(SUBJECT_PARAM,"2");put(SESSION_PARAM,"MR21");}}, 
			new HashMap<String,Object>(){{put(SUBJECT_PARAM,"3");put(SESSION_PARAM,"MR31");}}
			);
	
	
	static String PROJECT1= createProjectId("PRE");
	static String PROJECT2= createProjectId("PRE");
	static String PROJECT3= createProjectId("PRE");
	
	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws IOException, RESTException {
		//upload data
		put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT1 +"?format=xml&req_format=qs",getUserCredentials(),NONE);
        put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT1 +"/prearchive_code/0",getUserCredentials(),NONE);
	
		for(HashMap<String,Object> session:sessions){
			post(TestPrearchiveMgmtAPI.class,StringUtils.join(new String[]{getServiceURL("import"),"?triggerPipelines=false&project=" + PROJECT1 +"&subject=",(String)session.get(SUBJECT_PARAM),"&session=",(String)session.get(SESSION_PARAM),"&overwrite=append&prearchive=true"}),getUserCredentials(),"images/localizer.zip");
		}
		
		//create new projects
		put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT2 +"?format=xml&req_format=qs",getUserCredentials(),NONE);
		put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT3 +"?format=xml&req_format=qs",getUserCredentials(),NONE);

        put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT2 +"/prearchive_code/0",getUserCredentials(),NONE);
        put(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT3 +"/prearchive_code/0",getUserCredentials(),NONE);
		
	}

	/**
	 * @throws RESTException 
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws IOException, RESTException {
		try {
			delete(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT3 +"?removeFiles=true",getUserCredentials(),NONE);
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			delete(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT2 +"?removeFiles=true",getUserCredentials(),NONE);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		delete(TestPrearchiveMgmtAPI.class,"/REST/projects/" + PROJECT1 +"?removeFiles=true",getUserCredentials(),NONE);
		
		put(TestPrearchiveMgmtAPI.class,"/REST/prearchive",getAdminCredentials(),NONE);
	}
		
	//TESTS
	@Test
	public void testGradualCommitUnassignedMove() throws IOException, JSONException, RESTException, InterruptedException {
	// services/import?dest=/prearchive POST	
		clearUnassigned();
		final String tag="prearc_gradual_commit_unassigned_move";
		
		ResponseWrapper response=post(getServiceURL("import")+"?import-handler=gradual-DICOM&triggerPipelines=false&dest=/prearchive&inbody=true",getUserCredentials(),"images/localizer/2.dcm");
		
		String uri=response.getResponseTxt1stLine();
		
		//confirm it is there
		get(uri,getAdminCredentials());
		
		ResponseWrapper post3=post(uri+"?action=commit",getAdminCredentials());

		//confirm it is there
		get(uri,getAdminCredentials());
				
		//move to project 2
		post(this.getServiceURL("prearchive/move")+"?src="+uri + "&newProject=" + PROJECT3 +"&async=false",getAdminCredentials());

		Thread.currentThread();
		Thread.sleep(3000);
		
		final Map<String,Object> map=new HashMap<String,Object>(){{
			put(TestPrearchiveMgmtAPI.SUBJECT_PARAM,"PATIENT_NAME");
			put(TestPrearchiveMgmtAPI.SESSION_PARAM,"PATIENT_ID");
			put("status","READY");}};
			
		final JSONObject match2=searchForMatches("/REST/prearchive/projects/" + PROJECT3 +"?format=json",map,tag);
		assertNotNull("Move Failed from Unassigned to project.",match2);
		
		//move to project 3
		post(this.getServiceURL("prearchive/move")+"?src="+match2.get(URI2) + "&newProject=" + PROJECT2 +"&async=false",getAdminCredentials());

		Thread.currentThread();
		Thread.sleep(3000);
		
		final JSONObject match3=searchForMatches("/REST/prearchive/projects/" + PROJECT2 +"?format=json",map,tag);
		assertNotNull("Move Failed from project to project.",match3);
		
		delete("/REST"+match3.get(URI2),getUserCredentials());
		
	}
	
	@Test
	public void testPrearchiveListing() throws IOException, JSONException, RESTException{
		final List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT1 +"?format=json",getUserCredentials()));
						
		for(HashMap<String,Object> session:sessions){
			session.put("status","READY");
			assertTrue("Missing prearchive session:" + session.toString(),containsJSONObject(session,objects));
		}
		
		final JSONObject prearc_session=objects.get(1);
		final String prearc_session_URL=prearc_session.getString("url");
		
	}
	
	@Test
	public void testPrearchiveProjectListing() throws IOException, JSONException, RESTException{
		final List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT1 +"?format=json",getUserCredentials()));
						
		for(HashMap<String,Object> session:sessions){
			session.put("status","READY");
			assertTrue("Missing prearchive session:" + session.toString(),containsJSONObject(session,objects));
		}
	}
	
	@Test
	public void testPrearchiveSessionDelete() throws IOException, JSONException, RESTException{
			
		//retrieve list of prearchive sessions, should be empty
		List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT2 +"?format=json",getUserCredentials()));

		assertEquals("New Project Prearchive should be empty.",0,objects.size());
		
		//add one session
		final HashMap<String,Object> session=sessions.get(0);
		post(StringUtils.join(new String[]{getServiceURL("import")+"?triggerPipelines=false&dest=/prearchive&project=" + PROJECT2 +"&subject=",(String)session.get(SUBJECT_PARAM),"&session=",(String)session.get(SESSION_PARAM),""}),getUserCredentials(),"images/localizer.zip");
		
		//confirm add
		objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT2 +"?format=json",getUserCredentials()));
		
		assertEquals(1,objects.size());
		
		final JSONObject prearc_session=objects.get(0);
		final String prearc_session_URL=prearc_session.getString("url");
		
		//delete via Direct URI
		delete("/REST"+prearc_session_URL,getUserCredentials());
		
		//confirm delete via project listing
		objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT2 +"?format=json",getUserCredentials()));

		assertEquals("Project Prearchive post delete should be empty.",0,objects.size());
	}

	
	@Test
	public void testPrearchiveWDestination() throws IOException, JSONException, RESTException{
			
		//retrieve list of prearchive sessions, should be empty
		List<JSONObject> objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT2 +"?format=json",getUserCredentials()));

		assertEquals("New Project Prearchive should be empty.",0,objects.size());
		
		String destination ="/prearchive/projects/" + PROJECT2 +"/20090804_102241/2341242414";
		//add one session
		final HashMap<String,Object> session=sessions.get(0);
		post(StringUtils.join(new String[]{getServiceURL("import")+"?triggerPipelines=false&dest=" + destination + "&subject=",(String)session.get(SUBJECT_PARAM),"&session=",(String)session.get(SESSION_PARAM),""}),getUserCredentials(),"images/localizer.zip");
		
		//delete via Direct URI
		delete("/REST" +destination,getUserCredentials());
		
		//confirm delete via project listing-- should throw a 404 if it is missing
		objects=parseJSONTable(get("/REST/prearchive/projects/" + PROJECT2 +"?format=json",getUserCredentials()));

		assertEquals("Project Prearchive post delete should be empty.",0,objects.size());
	}
		
	//TESTS
	@Test
	public void testUnassignedMove() throws IOException, JSONException, InterruptedException, RESTException {
	// services/import?dest=/prearchive POST	
		final String tag="prearc_unassigned_move";
		post(getServiceURL("import")+"?triggerPipelines=false&dest=/prearchive&subject=subject1",getUserCredentials(),"images/localizer.zip");
		
		final Map<String,Object> map=new HashMap<String,Object>(){{
			put(TestPrearchiveMgmtAPI.SUBJECT_PARAM,"subject1");
			put(TestPrearchiveMgmtAPI.SESSION_PARAM,"PATIENT_ID");
			put("status","READY");}};
			
		//SHOULD GO INTO Unassigned
		final JSONObject match=searchForMatches("/REST/prearchive/projects/Unassigned?format=json",map,tag);
		assertNotNull("Upload Failed.",match);
		
		final String uri=match.getString(URI2);
			
		//move to project 2
		post(this.getServiceURL("prearchive/move")+"?src="+uri + "&newProject=" + PROJECT3 +"",getAdminCredentials());
		
		Thread.sleep(3000);
		final JSONObject match2=searchForMatches("/REST/prearchive/projects/" + PROJECT3 +"?format=json",map,tag);
		assertNotNull("Move Failed 2.",match2);
			
		//move to project 3
		post(this.getServiceURL("prearchive/move")+"?src="+match2.get(URI2) + "&newProject=" + PROJECT2 +"",getAdminCredentials());

		Thread.sleep(3000);
		final JSONObject match3=searchForMatches("/REST/prearchive/projects/" + PROJECT2 +"?format=json",map,tag);
		assertNotNull("Move Failed 3.",match3);
		
		delete("/REST"+match3.get(URI2),getUserCredentials());
		
	}
	
	
	
	private JSONObject searchForMatches(final String url, final Map<String,Object> map, final String tag) throws IOException, JSONException, RESTException{
		final List<JSONObject> objects=parseJSONTable(get(url,getAdminCredentials()));

		return getJSONObject(map,objects);
		
	}
}
