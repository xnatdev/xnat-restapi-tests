/*
 * TestProjectURIs.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

public class TestProjectURIs extends BaseTestCase {
	protected int subject_count=3;
	protected int session_count=3;
	protected int scan_count=10;
	protected int recon_count=2;
	protected boolean allowGET=true;


	private String PROJ_DEL_CROSS1= createProjectId("TPUR");
	private String PROJ_DEL_CROSS2= createProjectId("TPUR");
	
	private String p= createProjectId("TPUR");
	
	public String getProject(){
		return p;
	}

	@Before
	public void setUp() throws IOException, RESTException {
		put("/REST/projects/" + getProject() + "?format=xml&req_format=qs",getUserCredentials());
		createProject(PROJ_DEL_CROSS1,null,this.getUserCredentials());
		createProject(PROJ_DEL_CROSS2,null,this.getUserCredentials());
	}
	
	@After
	public void tearDown() throws IOException, RESTException {

		delete("/REST/projects/" + getProject() + "?removeFiles=true",getUserCredentials());
			
		delete("/REST/projects/" + PROJ_DEL_CROSS1 + "?removeFiles=true",getUserCredentials());
			
		delete("/REST/projects/" + PROJ_DEL_CROSS2 + "?removeFiles=true",getUserCredentials());
			
	}
	
	@Test
	public void testPrearchiveConfig() throws IOException, RESTException
	{

		put("/REST/projects/" + getProject() + "/prearchive_code/4",getUserCredentials());
		put("/REST/projects/" + getProject() + "/prearchive_code/0",getUserCredentials());
		put("/REST/projects/" + getProject() + "/prearchive_code/11",getUserCredentials());

	}

	
	@Test
	public void testProjectDeleteCrossover() throws IOException, RESTException
	{
		uploadProjResource( PROJ_DEL_CROSS1, "TESTING", SOME_FILE,SOME_FILE,null, getUserCredentials());
		uploadProjResource( PROJ_DEL_CROSS2, "TESTING2", SOME_FILE,SOME_FILE,null, getUserCredentials());
		
		validateProjResource(PROJ_DEL_CROSS1, "TESTING", SOME_FILE,SOME_FILE, getUserCredentials());
		
		delete("/REST/projects/" + PROJ_DEL_CROSS2 + "?removeFiles=true",getUserCredentials(),NONE);
		
		validateProjResource(PROJ_DEL_CROSS1, "TESTING", SOME_FILE,SOME_FILE, getUserCredentials());
	}
}
