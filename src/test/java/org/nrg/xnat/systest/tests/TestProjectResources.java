/*
 * TestProjectResources.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.tests;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatProjectdataBean;
import org.nrg.xnat.systest.BaseTestCase;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.fail;

public class TestProjectResources extends BaseTestCase {

    @Before
    public void setUp() throws IOException {
    }

    public static List<String> projects = Lists.newArrayList();

    private static String registerProject() {
        String p = createProjectId("PROJECT");
        projects.add(p);
        return p;
    }

    @AfterClass
    public static void tearDownAfterClass() throws IOException {
        for (String p : projects) {
            try {
                delete(TestImport.class, "/REST/projects/" + p + "?removeFiles=true", getUserCredentials(), NONE);
            } catch (Throwable ignore) {
                //ignore
            }
        }
    }

    @Test
    public void testList() throws IOException {
        try {
            get("/REST/projects?format=html", getUserCredentials());
        } catch (RESTException e) {
            if (e.getStatusCode() != 403) {
                fail(e.getMessage());
            }
        }

    }

    @Test
    public void testQueryCRUD() throws IOException, RESTException {
        String CURRENT_PROJECT = registerProject();

        put("/REST/projects/" + CURRENT_PROJECT + "?format=xml&req_format=qs&alias=" + CURRENT_PROJECT + "_ALIAS", getUserCredentials());

        get("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials());

        get("/REST/projects/" + CURRENT_PROJECT + "_ALIAS?format=xml", getUserCredentials());

        delete("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials());

    }

    private boolean compare(ResponseWrapper response, String source) throws IOException, ComparisonException, SAXException {
        Map<Class, List<String>> ignore = new Hashtable<>();

        ignore.put(XnatProjectdataBean.class, new ArrayList<String>());
        ignore.get(XnatProjectdataBean.class).add("ID");
        ignore.get(XnatProjectdataBean.class).add("studyProtocol");
        ignore.get(XnatProjectdataBean.class).add("secondary_ID");

        Comparison c = CompareObjectsFromFile(response.file(), this.GetSourceFile(source), ignore);
        if (c.critical.size() == 0) {
            for (String s : c.warnings) {
                System.out.println(s);
            }
        } else {
            throw new BaseTestCase.ComparisonException(Joiner.on(", ").join(c.critical));
        }

        return true;
    }

    @Test
    public void testXMLCRUD() throws IOException, SAXException, ComparisonException, RESTException {
        String CURRENT_PROJECT = registerProject();

        //create project
        put("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials(), "test_project_v1.xml");

        //get/compare project
        compare(get("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials()), "test_project_v1.xml");

        //modify project
        put("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials(), "test_project_v2.xml");

        //get/compare project
        compare(get("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials()), "test_project_v2.xml");

        //resubmit (no change)
        put("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials(), "test_project_v2.xml");

        //get/compare/project
        compare(get("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials()), "test_project_v2.xml");

        //delete project
        delete("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials());

        try {
            //confirm deleted
            get("/REST/projects/" + CURRENT_PROJECT + "?format=xml", getUserCredentials());
            fail("Item should have been deleted");
        } catch (RESTException e) {
            if (e.getStatusCode() != 404) {
                fail(e.getMessage());
            }
        }
    }

}
