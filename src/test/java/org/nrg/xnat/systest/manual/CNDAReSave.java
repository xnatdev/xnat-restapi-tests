/*
 * CNDAReSave.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.manual;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xdat.bean.XnatPetscandataBean;
import org.nrg.xdat.bean.XnatReconstructedimagedataBean;
import org.nrg.xdat.bean.XnatSubjectdataBean;
import org.nrg.xdat.bean.reader.XDATXMLReader;
import org.nrg.xdat.model.XnatProjectparticipantI;
import org.nrg.xnat.systest.BaseTestCase;
import org.nrg.xnat.systest.BaseTestCase.RESTException;
import org.nrg.xnat.systest.BaseTestCase.ResponseWrapper;
import org.xml.sax.SAXException;

public class CNDAReSave  extends BaseTestCase {
	final String SUBJECT_LOCATION="cnda_test_subjects";
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		
	}

	final List<String> checkedProjects=new ArrayList<String>();
	
	public void checkProject(String project) throws MalformedURLException,IOException, RESTException{
		if(!checkedProjects.contains(project)){
			try {
				get(String.format("/REST/projects/%s", new Object[]{project}),getAdminCredentials());
			} catch (FileNotFoundException e) {
					put(String.format("/REST/projects/%s", new Object[]{project}),getAdminCredentials());
			}
			checkedProjects.add(project);
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testReSave() throws Throwable{
		final File dir=this.GetSourceFile(SUBJECT_LOCATION);
		
		for(final File srcF:dir.listFiles()){
			if(srcF.getName().endsWith(".xml")){
				System.out.println("Processing "+srcF.getName());
				try {
					FileInputStream fis2 = new FileInputStream(srcF);
					XDATXMLReader reader2 = new XDATXMLReader();
					XnatSubjectdataBean src=(XnatSubjectdataBean) reader2.parse(fis2);
				
					try{
						this.checkProject(src.getProject());
					}catch (MalformedURLException e1) {
							fail(e1.getMessage());
					} catch (IOException e1) {
							fail(e1.getMessage());
					}
						
					for(XnatProjectparticipantI pp:src.getSharing_share()){
						this.checkProject(pp.getProject());
					}
					
					
					int iterations=2;
					for(int i=0;i<iterations;i++){
						try {
							put(String.format("/REST/projects/%s/subjects/%s?format=xml&allowDataDeletion=true&loadExisting=false", new Object[]{src.getProject(),src.getLabel()}),getAdminCredentials(),srcF);
						} catch (MalformedURLException e) {
							fail(e.getMessage());
						} catch (IOException e) {
							fail(e.getMessage());
						}
					}
					

					try {
						get(String.format("/REST/projects/%s/subjects/%s?format=xml&concealHiddenFields=true", new Object[]{src.getProject(),src.getLabel()}),getAdminCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						fail(e.getMessage());
					}
					
					Map<Class,List<String>> ignore=new Hashtable<Class,List<String>>();

					ignore.put(XnatPetscandataBean.class, new ArrayList<String>());
					ignore.get(XnatPetscandataBean.class).add("parameters/annotation");
					
					ignore.put(XnatReconstructedimagedataBean.class, new ArrayList<String>());
					ignore.get(XnatReconstructedimagedataBean.class).add("provenance");
									
					try{
						final Comparison c=CompareObjectsFromFile(srcF,this.GetResultsFile("GET_" +src.getId() + ".xml"),ignore);
						if(c.critical.size()==0){
							for(String s: c.warnings){
								System.out.println(s);
							}
							assertTrue(true);
							System.out.println("Files are equivalent.");
						}else{
							for(String s: c.critical){
								System.out.println(s);
								fail(s);
							}
						}
					} catch (Exception e) {
						fail(e.getMessage());
					}
				} catch (FileNotFoundException e) {
					fail(e.getMessage());
				} catch (IOException e) {
					fail(e.getMessage());
				} catch (SAXException e) {
					fail(e.getMessage());
				}
			}
		}
		
	}
}
