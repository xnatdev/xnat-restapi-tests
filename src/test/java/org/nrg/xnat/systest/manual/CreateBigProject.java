/*
 * CreateBigProject.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.manual;
import org.junit.After;
import org.nrg.xnat.systest.tests.TestMuchoFiles;

/**
 * @author timo
 *
 */
public class CreateBigProject extends TestMuchoFiles {
	public CreateBigProject(){
		super();
		subject_count=1600;
		session_count=1;
		scan_count=1;
		recon_count=0;
		allowGET=false;
	}
	
	public String getProject(){
		return "BIG_TEST5";
	}


}
