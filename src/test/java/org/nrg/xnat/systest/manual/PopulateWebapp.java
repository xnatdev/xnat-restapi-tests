/*
 * PopulateWebapp.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.manual;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.json.JSONArray;
import org.json.JSONObject;
import org.nrg.xnat.systest.util.Base64;



public class PopulateWebapp {

	public static final String host="http://localhost:7080/xnat";
	public static final String user1="tolsen:tolsen";
	public static final String user2="admin:admin";
	public static final String user3="tolsen:test";
	public static final String root_file_directory="C:\\DATA\\tim_dev_3\\xnat_test\\files";
	
	public void execute(){

		try {
			URLConnection urlConn;
				        
			GetMethod get = new GetMethod(host + "/REST/projects/JUNIT1?format=xml");
			
	        HttpClient client = new HttpClient();
	        get.addRequestHeader("Authorization", "Basic " +  Base64.encode(user1));
	        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        
	        int status = client.executeMethod(get);
	        
	        if(status==401){
	        	System.out.println("Failed authentication.");
	        }
	        
	        if(status!=200){
	        	System.out.println("Creating project...");
				urlConn = newConnection("/REST/projects?format=xml", user1);
				if(urlConn instanceof HttpURLConnection) {
				    HttpURLConnection httpConn = (HttpURLConnection)urlConn;
				    httpConn.setRequestMethod("POST");
				}
				urlConn.setRequestProperty("Cache-Control", "no-cache");
				urlConn.setRequestProperty("Connection", "Keep-Alive");
				
				this.writeToStream(urlConn, "test_project_v1.xml");
				this.writeToFile(urlConn, "project_POST.xml");
				
				System.out.println("project POST Successful");
				
				urlConn=null;
	        }


	        get = new GetMethod(host + "/REST/projects/JUNIT1/subjects/1?format=xml");
			
	        client = new HttpClient();
	        get.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
	        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        
	        status = client.executeMethod(get);
	        
	        if(status!=200){
	        	System.out.println("Creating subject...");
				urlConn = newConnection("/REST/projects/JUNIT1/subjects?format=xml", user1);
				if(urlConn instanceof HttpURLConnection) {
				    HttpURLConnection httpConn = (HttpURLConnection)urlConn;
				    httpConn.setRequestMethod("POST");
				}
				urlConn.setRequestProperty("Cache-Control", "no-cache");
				urlConn.setRequestProperty("Connection", "Keep-Alive");
				
				this.writeToStream(urlConn, "test_subject_v1.xml");
				this.writeToFile(urlConn, "subject_POST.txt");
				
				System.out.println("subject POST Successful");
				
				urlConn=null;
	        }


	        get = new GetMethod(host + "/REST/projects/JUNIT1/subjects/1/experiments/JUNIT1_E1?format=xml");
			
	        client = new HttpClient();
	        get.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
	        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        
	        status = client.executeMethod(get);
	        
	        if(status!=200){
	        	System.out.println("Creating experiment...");
				urlConn = newConnection("/REST/projects/JUNIT1/subjects/1/experiments?format=xml", user1);
				if(urlConn instanceof HttpURLConnection) {
				    HttpURLConnection httpConn = (HttpURLConnection)urlConn;
				    httpConn.setRequestMethod("POST");
				}
				urlConn.setRequestProperty("Cache-Control", "no-cache");
				urlConn.setRequestProperty("Connection", "Keep-Alive");
				
				this.writeToStream(urlConn, "test_expt_v1.xml");
				this.writeToFile(urlConn, "expt_POST.txt");
				
				System.out.println("expt POST Successful");
				
				urlConn=null;
	        }

	        get = new GetMethod(host + "/REST/projects/JUNIT1/subjects/1/experiments/JUNIT1_E1/resources?format=json");
			
	        client = new HttpClient();
	        get.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
	        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
	        
	        status = client.executeMethod(get);
	        JSONArray catalogs = new JSONObject(get.getResponseBodyAsString()).getJSONObject("ResultSet").getJSONArray("Result");
	        
	        if(catalogs.length()==0){
		        //create resource catalog
				PostMethod filePost = new PostMethod(host + "/REST/projects/JUNIT1/subjects/1/experiments/JUNIT1_E1/resources?format=json&req_format=qs&xnat:resourceCatalog/label=TEST1");
		                
		        client = new HttpClient();
		        filePost.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
		        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
		        
		        System.out.println("Posting resource catalog: " + client.executeMethod(filePost));
		        
		        get = new GetMethod(host + "/REST/projects/JUNIT1/subjects/1/experiments/JUNIT1_E1/resources?format=json");
				
		        client = new HttpClient();
		        get.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
		        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
		        
		        status = client.executeMethod(get);
		        catalogs = new JSONObject(get.getResponseBodyAsString()).getJSONObject("ResultSet").getJSONArray("Result");
	        }
	        
	        JSONObject catalog = catalogs.getJSONObject(0);
	        int resource_id=catalog.getInt("xnat_abstractresource_id");
	        
	        
	        for(File f:new File(root_file_directory + "\\images").listFiles()){
	        	System.out.println("Sending " + f.getAbsolutePath() + "...");
		        PostMethod filePost = new PostMethod(host + "/REST/projects/JUNIT1/subjects/1/experiments/JUNIT1_E1/resources/" + resource_id +"/files?format=json");
		        filePost.addRequestHeader("Authorization", "Basic " + Base64.encode(user1));
		        
		        Part[] parts={new FilePart("img1",f)};
		        filePost.setRequestEntity(new MultipartRequestEntity(parts,filePost.getParams()));
		        
		        client = new HttpClient();
		        client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
		        
		        status = client.executeMethod(filePost);
	        }
	        
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PopulateWebapp pw = new PopulateWebapp();
		pw.execute();
		
	}
	

	public URLConnection newConnection(String script,String userString) throws MalformedURLException, IOException{
		URLConnection url = new URL(host+ script).openConnection();
		url.addRequestProperty("Authorization", "Basic " + Base64.encode(user1));
		return url;
	}

	public void writeToStream(URLConnection urlConn,String source) throws IOException{
		urlConn.setDoOutput(true);
		File inFile=new File(root_file_directory + "\\" + source);
		
		OutputStream out=urlConn.getOutputStream();
		
		InputStream bis = null;
		BufferedOutputStream bos = null;

		try {
			bis = new FileInputStream(inFile);

			bos = new BufferedOutputStream(out);

			byte[] buff = new byte[256];
			int bytesRead;

			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
				bos.flush();
			}

			bis.close();
			bos.flush();
			bos.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}

	public void writeToFile(URLConnection url,String destination) throws IOException{
		File outFile=new File(root_file_directory + "\\results\\" + destination);
		InputStream bis = null;
		BufferedOutputStream bos = null;

		try {
			FileOutputStream out = new FileOutputStream(outFile);

			bis = url.getInputStream();
			bos = new BufferedOutputStream(out);

			byte[] buff = new byte[256];
			int bytesRead;
			int loaded = 0;

			while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
				bos.write(buff, 0, bytesRead);
				bos.flush();
				loaded = loaded + bytesRead;
			}

			bis.close();
			bos.flush();
			bos.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}

}
