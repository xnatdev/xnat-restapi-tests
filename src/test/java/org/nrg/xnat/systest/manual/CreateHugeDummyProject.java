/*
 * CreateHugeDummyProject.java
 *
 * XNAT REST API Test Suite
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD https://opensource.org/licenses/BSD-2-Clause
 */

package org.nrg.xnat.systest.manual;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.xnat.systest.BaseTestCase;

import static org.junit.Assert.fail;

public class CreateHugeDummyProject extends BaseTestCase {
	protected int subject_count=3;
	protected int session_count=3;
	protected int scan_count=10;
	protected int recon_count=2;
	protected boolean allowGET=true;
	public String getProject(){
		return "DUMMY";
	}
	
	public CreateHugeDummyProject(){
		super();
		subject_count=5;
		session_count=10;
		scan_count=7;
		recon_count=0;
		allowGET=true;
	}

	@Before
	public void setUp() throws Exception, RESTException {

		try {
			put("/REST/projects/" + getProject() + "?format=xml&req_format=qs&alias=" + getProject() + "_ALIAS",getUserCredentials());
			
		} catch (MalformedURLException e) {
			fail(e.getMessage());
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
	}


	
	@Test
	public void testMuchoFiles() throws RESTException
	{
		boolean _continue=true;
		
		for(int sC=1;sC<=subject_count;sC++){
			try {
				put("/REST/projects/" + getProject() + "/subjects/s" + sC + "?format=xml&req_format=qs&gender=male",getUserCredentials());
			} catch (MalformedURLException e) {
				fail(e.getMessage());
			} catch (IOException e) {
				fail(e.getMessage());
			}
			
			for(int mC=1;mC<=session_count;mC++){
				try {
					put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "?format=xml&req_format=qs&xsiType=xnat:MRSession&date=12/12/1999",getUserCredentials());
				} catch (MalformedURLException e) {
					fail(e.getMessage());
				} catch (IOException e) {
					fail(e.getMessage());
				}
				
				for(int cC=1;cC<=scan_count;cC++){
					try {
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "?format=xml&req_format=qs&xsiType=xnat:MRScan&series_description=LOCALIZER&quality=usable&type=dummy",getUserCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						fail(e.getMessage());
					}
					
					try {
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "/resources/DUMMY",getUserCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						e.printStackTrace();
						fail(e.getMessage());
					}
					
					int FILE_COUNT=256;
					
					for(int fC=1;fC<=FILE_COUNT;fC++){
						try {
							put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/scans/" + cC + "/resources/DUMMY/files/dummy_" + fC + ".txt?inbody=true",getUserCredentials(),"dummy.txt");
						} catch (MalformedURLException e) {
							fail(e.getMessage());
						} catch (IOException e) {
							fail(e.getMessage());
						}
					}
					
					if(!_continue)break;
				}
				
				for(int cC=1;cC<=recon_count;cC++){
					try {
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "?format=xml&req_format=qs&type=LOCALIZER",getUserCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						fail(e.getMessage());
					}
					
					try {
						put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "/out/resources/DICOM",getUserCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						e.printStackTrace();
						fail(e.getMessage());
					}
					
					for(int fC=1;fC<=3;fC++){
						try {
							put("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/reconstructions/" + sC + "_MR" + mC + "_" + cC + "/out/resources/DICOM/files/" + fC + ".dcm?inbody=true",getUserCredentials(),"images" + File.separator + fC + ".dcm");
						} catch (MalformedURLException e) {
							fail(e.getMessage());
						} catch (IOException e) {
							fail(e.getMessage());
						}
					}
					
					if(!_continue)break;
				}
				if(!_continue)break;
			}
			
			
			if(!_continue)break;
		}
		
		if(allowGET){
			for(int sC=1;sC<=subject_count;sC++){
				
				for(int mC=1;mC<=session_count;mC++){
					try {
						get("/REST/projects/" + getProject() + "/subjects/s" + sC + "/experiments/s" + sC + "_MR" + mC + "/files?format=xcat&all=true",getUserCredentials());
					} catch (MalformedURLException e) {
						fail(e.getMessage());
					} catch (IOException e) {
						fail(e.getMessage());
					}
					if(!_continue)break;
				}
				if(!_continue)break;
			}
		}
	}

}
